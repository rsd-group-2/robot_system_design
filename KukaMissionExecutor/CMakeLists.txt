# %Tag(FULLTEXT)%
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

project(rsdmissionexecutor)
cmake_minimum_required(VERSION 2.8.6)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp rospy std_msgs geometry_msgs message_generation message_filters sensor_msgs)

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  Brick.msg
)

## Declare ROS messages and services
add_service_files(	
        FILES
        Close.srv
        Home.srv
        Move.srv
        Open.srv
        Stop.srv
        goToQ.srv
        nextBrick.srv
        RequestBricks.srv
        getIsMoving.srv
        setRobotDropOff.srv
        setRobotImagePose.srv
)

generate_messages(DEPENDENCIES std_msgs geometry_msgs)

include_directories(include ${catkin_INCLUDE_DIRS})

# We use the settings that robwork studio uses
SET(CMAKE_BUILD_TYPE RelWithDebInfo)
SET(RW_ROOT "$ENV{RW_ROOT}")

#Include default settings for constructing a robwork dependent project
FIND_PACKAGE(
  RobWork
  QUIET
  HINTS
    "${RW_ROOT}")
#The following is a workaround for the old version of RobWork. Remove once no longer needed (everyone switched to a RobWork version newer than April 2012)
IF(NOT ROBWORK_FOUND)
  MESSAGE(STATUS "Could not find RobWork with the new method. Trying the old one.")
  INCLUDE(${RW_ROOT}/build/FindRobWork.cmake)
ENDIF(NOT ROBWORK_FOUND)

#find_package(Threads REQUIRED)

INCLUDE_DIRECTORIES( ${ROBWORK_INCLUDE_DIRS} ${ROBWORKSTUDIO_INCLUDE_DIRS})
LINK_DIRECTORIES(${ROBWORK_LIBRARY_DIRS} ${ROBWORKSTUDIO_LIBRARY_DIRS} ${catkin_LIBRARIES})

MESSAGE( STATUS "Robwork includedirs ${ROBWORK_INCLUDE_DIRS}" )

set(ROS_BUILD_STATIC_LIBS false)
set(ROS_BUILD_SHARED_LIBS true)


SET(SrcFiles src/KukaMissionExecutor.cpp src/KukaMissionExecutor.hpp src/main.cpp)

add_executable(${PROJECT_NAME} ${SrcFiles})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES} ${ROBWORK_LIBRARIES})
MESSAGE(${PROJECT_NAME} "Done!")


# %EndTag(FULLTEXT)%
