// Includes
#include <ros/ros.h>
#include "kuka_rsi/getConfiguration.h"
#include "kuka_rsi/setConfiguration.h"
#include "kuka_rsi/getQueueSize.h"
#include "kuka_rsi/getIsMoving.h"
#include "kuka_rsi/stopRobot.h"
#include "kuka_rsi/getSafety.h"
#include "std_msgs/String.h"
#include <iostream>
#include <queue>
#include <boost/thread.hpp>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "kukaRSIParser.h"
#include "UDP.h"
#include <rw/math.hpp>
#include <rw/math/Q.hpp>
#include <rw/trajectory/RampInterpolator.hpp>

#define DEBUG                   false
#define IP_SERVER               "192.168.100.50"
#define IP_CLIENT               "192.168.100.100"
#define PORT_SERVER             49000
#define PORT_CLIENT             53453
#define RSI_TIME                0.012   // s
#define PUBLISH_RATE            20      // Hz
#define DEGREETORAD             (M_PI/180.0)
#define RADTODEGREE             (180.0/M_PI)
#define JOINT_SPEED_MAX         80
#define JOINT_SPEED_NORMAL      20
#define JOINT_SPEED_BRAKE       10
#define JOINT_ACC_MAX           50
#define JOINT_ACC_NORMAL        20
const rw::math::Q QMAX(6, 170, 135, 60, 185, 25, 350);
const rw::math::Q QMIN(6, -170, -100, -200, -185, -210, -350);
const rw::math::Q speedBrake(6, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE, JOINT_SPEED_BRAKE);
const rw::math::Q accNormal(6, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL, JOINT_ACC_NORMAL);

// Max joint speed
//360 °/s
//300 °/s
//360 °/s
//381 °/s
//388 °/s
//615 °/s

template <typename T>
class SynchronisedQueue
{
    private:
        std::queue<T> m_queue;              // Use STL queue to store data
        boost::mutex m_mutex;               // The mutex to synchronise on
        boost::condition_variable m_cond;   // The condition to wait for

    public:
        // Add data to the queue and notify others
        void enqueue(const T& data)
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            // Add the data to the queue
            m_queue.push(data);

            // Notify others that data is ready
            m_cond.notify_one();
        }

        // Get data from the queue. Wait for data if not available
        T dequeue()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            // When there is no data, wait till someone fills it.
            // Lock is automatically released in the wait and obtained
            // again after the wait
            while(m_queue.size()==0)
                m_cond.wait(lock);

            // Retrieve the data from the queue
            T result = m_queue.front();
            m_queue.pop();

            return result;
        }

        int size()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.size();
        }

        T front()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.front();
        }

        T back()
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);
            return m_queue.back();
        }

        void clear(bool smoothBrake)
        {
            // Acquire lock on the queue
            boost::unique_lock<boost::mutex> lock(m_mutex);

            std::queue<rw::math::Q> empty;

            if(m_queue.empty() == false && smoothBrake)
            {
                rw::math::Q qStart = m_queue.front();
                m_queue.pop();

                if(m_queue.empty() == false)
                {
                    rw::math::Q qNext = m_queue.front();
                    m_queue.pop();
                    rw::math::Q qDelta = qNext-qStart;

                    for(unsigned i=0; i<qDelta.size(); i++)
                    {
                        if(qDelta[i] > 0.05)
                            qNext[i] += 1.0;
                        else if(qDelta[i] < -0.05)
                            qNext[i] -= 1.0;
                    }

                    rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qNext, speedBrake, accNormal);
                    for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
                       empty.push(rampInterpolator.x(j));
                }
            }

            std::swap(m_queue, empty);
        }
};

// Global variables
std::string _ipServer, _ipClient;
int _portServer, _portClient;
bool _debug;
boost::thread *_serverThread, *_clientThread;
SynchronisedQueue<std::string> _sendQueue;
SynchronisedQueue<rw::math::Q> _qQueue;
rw::math::Q _qCurrent(6,0,0,0,0,0,0), _qOffset(6, 0, -90, 90, 0, 90, 0);
boost::mutex _qMutex;
bool _safety;
boost::mutex _safetyMutex;

// Prototypes
bool setConfigurationCallback(kuka_rsi::setConfiguration::Request &req, kuka_rsi::setConfiguration::Response &res);
bool getConfigurationCallback(kuka_rsi::getConfiguration::Request &req, kuka_rsi::getConfiguration::Response &res);
bool getQueueSizeCallback(kuka_rsi::getQueueSize::Request &req, kuka_rsi::getQueueSize::Response &res);
bool stopRobotCallback(kuka_rsi::stopRobot::Request &req, kuka_rsi::stopRobot::Response &res);
bool isMovingCallback(kuka_rsi::getIsMoving::Request &req, kuka_rsi::getIsMoving::Response &res);

// Functions
void client()
{
    // Variables
    udp_client_server::udp_client client(_ipClient, _portClient);
    std::string str;

    while(true)
    {
        try
        {
            // Get frame from queue
            str = _sendQueue.dequeue();

            // Inform
            if(_debug || DEBUG)
                std::cout << "Sent: \n" << str << std::endl << std::endl;

            // Send frame
            client.send(str.c_str(), str.size());

            // Signal interrupt point
            boost::this_thread::interruption_point();
        }
        catch(const boost::thread_interrupted&)
        {
            ROS_INFO("- Client thread interrupted. Exiting thread.-");
            break;
        }
    }
}

void server()
{
    // Variables
    udp_client_server::udp_server server(_ipServer, _portServer);
    KukaRSIParser rsiParser;
    char *msg = new char[1000];
    std::string str;
    rw::math::Q qCurrentReal(6);
    int oldIPOC = 0;
    rw::math::Q qNew(6);
    rw::math::Q qCurrent(6);

    // Loop
    while(true)
    {
        try
        {
            // Receive packet from client
            int msgSize = server.recv(msg, 1000);

            if(msgSize != -1)
            {
                // Convert to string
                str = (std::string(msg)).substr(0,msgSize-1);

                // Parse packet
                rsiParser.parseXML(str);

                // Get IPOC
                int IPOC = rsiParser.getIPOC();

                // Get safety
                _safetyMutex.lock();
                _safety = rsiParser.getDigout();
                _safetyMutex.unlock();

                // Get current joint configuration
                KukaRSIParser::RSI_AIPos AIPos = rsiParser.getQ();
                for(int i=0; i<6; i++)
                    qCurrentReal[i] = AIPos.q[i];

                // Get current in offset coordinates
                _qMutex.lock();
                _qCurrent = qCurrentReal - _qOffset;
                qCurrent = _qCurrent;
                _qMutex.unlock();

                // Check IPOC
                if((IPOC-oldIPOC) > (RSI_TIME*1000)*2)
                {
                    std::cout << "Connection to robot (re)established!" << std::endl;
                    _qQueue.clear(false);
                    qNew = qCurrent;
                }
                oldIPOC = IPOC;

                // Inform
                if(_debug || DEBUG)
                    std::cout << "Received: \n" << str << std::endl << std::endl;

                /****************************************************************************/
                if(_qQueue.size())
                    qNew = _qQueue.dequeue();

                // Update new pos
                for(int i=0; i<6; i++)
                    AIPos.q[i] = qNew[i];
                /****************************************************************************/

                // Construct return frame
                str = rsiParser.constructFile(AIPos);

                // Send to send queue
                _sendQueue.enqueue(str);
            }
            else
            {
                ROS_ERROR("Received empty frame!");
            }

            // Signal interrupt point
            boost::this_thread::interruption_point();
        }
        catch(const boost::thread_interrupted&)
        {
            ROS_INFO("- Server thread interrupted. Exiting thread.");
            break;
        }
    }
}

/*void server()
{
    // Variables
    udp_client_server::udp_server server(_ipServer, _portServer);
    KukaRSIParser rsiParser;
    char *msg = new char[1000];
    std::string str;
    rw::math::Q qCurrentReal(6);
    int oldIPOC = 0;
    rw::math::Q qNew(6);
    rw::math::Q qCurrent(6);

    int slowDown = 0;

    // Loop
    while(true)
    {
        try
        {
            // Receive packet from client
            int msgSize = server.recv(msg, 1000);

            if(msgSize != -1)
            {
                // Convert to string
                str = (std::string(msg)).substr(0,msgSize-1);

                // Parse packet
                rsiParser.parseXML(str);

                // Get IPOC
                int IPOC = rsiParser.getIPOC();

                // Get safety
                _safetyMutex.lock();
                _safety = rsiParser.getDigout();
                _safetyMutex.unlock();

                // Get current joint configuration
                KukaRSIParser::RSI_AIPos AIPos = rsiParser.getQ();
                for(int i=0; i<6; i++)
                    qCurrentReal[i] = AIPos.q[i];

                // Get current in offset coordinates
                _qMutex.lock();
                _qCurrent = qCurrentReal - _qOffset;
                qCurrent = _qCurrent;
                _qMutex.unlock();

                // Check IPOC
                if((IPOC-oldIPOC) > (RSI_TIME*1000)*2)
                {
                    std::cout << "Connection to robot (re)established!" << std::endl;
                    _qQueue.clear(false);
                    qNew = qCurrent;
                }

                if(IPOC-oldIPOC > 12)
                    std::cerr << "FUCK IPOC" << std::endl;

                // Inform
                if(_debug || DEBUG)
                    std::cout << "Received: \n" << str << std::endl << std::endl;

                // Check delay
                int delay = rsiParser.getDelay();
                if(delay > 0)
                {
                    std::cerr << "Received (" << (IPOC-oldIPOC) <<  "): \n" << str << std::endl << std::endl;
                }

                oldIPOC = IPOC;
                /****************************************************************************
                if(_qQueue.size() && delay == 0 && slowDown == 0)
                    qNew = _qQueue.dequeue();
                else if(slowDown > 0)
                    slowDown--;

                if(delay>0 && slowDown == 0)
                    slowDown = 5;

                /*if(_qQueue.size())
                {
                    qNew = _qQueue.front();

                    if(delay == 0 && first == false)
                        _qQueue.dequeue();

                    first = false;
                }*

                // Update new pos
                for(int i=0; i<6; i++)
                    AIPos.q[i] = qNew[i];
                /****************************************************************************

                // Construct return frame
                str = rsiParser.constructFile(AIPos);

                if(delay > 0)
                {
                    //std::cerr << "Sent: \n" << str << std::endl << std::endl;

                    std::cerr << (qNew+_qOffset).norm2() << std::endl;
                }

                if(str.empty())
                    std::cerr << "FUCK" << std::endl;

                // Send to send queue
                _sendQueue.enqueue(str);
            }
            else
            {
                ROS_ERROR("Received empty frame!");
            }

            // Signal interrupt point
            boost::this_thread::interruption_point();
        }
        catch(const boost::thread_interrupted&)
        {
            ROS_INFO("- Server thread interrupted. Exiting thread.");
            break;
        }
    }
}*/

void signalCallback(int signal)
{
    // Interrupt threads
    _serverThread->interrupt();
    _clientThread->interrupt();

    // Exit program
    exit(1);
}

bool setConfigurationCallback(kuka_rsi::setConfiguration::Request &req, kuka_rsi::setConfiguration::Response &res)
{
    rw::math::Q qEnd(6);
    _qMutex.lock();
    rw::math::Q qStart = _qCurrent;
    _qMutex.unlock();

    if(_qQueue.size())
        qStart = _qQueue.back();

    // Get joint values
    for(unsigned int i=0; i<qEnd.size(); i++)
        qEnd(i) = req.q[i] * RADTODEGREE;

    // Check joint values
    for(unsigned int i=0; i<qEnd.size(); i++)
    {
        if(qEnd(i) > QMAX(i) || qEnd(i) < QMIN(i))
        {
            ROS_ERROR("Joint limit overrule!");
            return false;
        }
    }

    // Check speed values
    rw::math::Q qSpeed(6);
    for(unsigned int i=0; i<qSpeed.size(); i++)
    {
        if(req.speed[i] < 1.0 || req.speed[i] > JOINT_SPEED_MAX)
            qSpeed(i) = JOINT_SPEED_NORMAL;
        else
            qSpeed(i) = req.speed[i];
    }

    // Check acceleration values
    // TODO

    // Create interpolated trajectory
    rw::trajectory::RampInterpolator<rw::math::Q> rampInterpolator(qStart, qEnd, qSpeed, accNormal);
    for(double j = 0.0; j <= rampInterpolator.duration(); j += RSI_TIME)
        _qQueue.enqueue(rampInterpolator.x(j));

    return true;
}

bool getConfigurationCallback(kuka_rsi::getConfiguration::Request &req, kuka_rsi::getConfiguration::Response &res)
{
    _qMutex.lock();
    // Put into res array
    for(unsigned int i=0; i<_qCurrent.size(); i++)
        res.q[i] = _qCurrent[i];

    _qMutex.unlock();

    return true;
}

bool getQueueSizeCallback(kuka_rsi::getQueueSize::Request &req, kuka_rsi::getQueueSize::Response &res)
{
    // Get queue size
    res.queueSize = _qQueue.size();

    return true;
}

bool stopRobotCallback(kuka_rsi::stopRobot::Request &req, kuka_rsi::stopRobot::Response &res)
{
    _qQueue.clear(true);

    return true;
}

bool isMovingCallback(kuka_rsi::getIsMoving::Request &req, kuka_rsi::getIsMoving::Response &res)
{
    // Get queue size
    res.isMoving = (bool)_qQueue.size();
    return true;
}

bool safetyCallback(kuka_rsi::getSafety::Request &req, kuka_rsi::getSafety::Response &res)
{
    _safetyMutex.lock();
    res.safetyBreached = !_safety;
    _safetyMutex.unlock();

    return true;
}

int main()
{
    // Setup ROS Arguments
    char** argv = NULL;
    int argc = 0;

    // Init ROS Node
    ros::init(argc, argv, "KukaRSI");
    ros::NodeHandle nh;

    // Get parameters
    std::string servicePrefixName;
    nh.param<std::string>("/Kuka_RSI/KukaRSI/IPServer", _ipServer, IP_SERVER);
    nh.param<std::string>("/Kuka_RSI/KukaRSI/IPClient", _ipClient, IP_CLIENT);
    nh.param<int>("/Kuka_RSI/KukaRSI/PortServer", _portServer, PORT_SERVER);
    nh.param<int>("/Kuka_RSI/KukaRSI/PortClient", _portClient, PORT_CLIENT);
    nh.param<bool>("/Kuka_RSI/KukaRSI/Debug", _debug, false);
    nh.param<std::string>("/Kuka_RSI/KukaRSI/CmdServiceName", servicePrefixName, "/KukaNode");

    // Create service handlers
    ros::ServiceServer getConfigurationService = nh.advertiseService(servicePrefixName + "/GetConfiguration", getConfigurationCallback);
    ros::ServiceServer setConfigurationService = nh.advertiseService(servicePrefixName + "/SetConfiguration", setConfigurationCallback);
    ros::ServiceServer getQueueSizeService = nh.advertiseService(servicePrefixName + "/GetQueueSize", getQueueSizeCallback);
    ros::ServiceServer stopRobotService = nh.advertiseService(servicePrefixName + "/StopRobot", stopRobotCallback);
    ros::ServiceServer isMovingService = nh.advertiseService(servicePrefixName + "/IsMoving", isMovingCallback);
    ros::ServiceServer getSafetyService = nh.advertiseService(servicePrefixName + "/GetSafety", safetyCallback);

    // Create and run server and client threads
    _serverThread = new boost::thread(server);
    _clientThread = new boost::thread(client);

    // Handle signals
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = signalCallback;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    // Inform
    std::cout << "Server+Client is running.." << std::endl;

    ros::Rate r(50); // 10 hz

    // Spin
    while(ros::ok())
    {
        ros::spinOnce();
        r.sleep();
    }


    // Interrupt threads
    _serverThread->interrupt();
    _clientThread->interrupt();

    // Return
    return 0;
}
