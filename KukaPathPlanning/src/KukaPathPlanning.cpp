#include "KukaPathPlanning.hpp"

KukaPathPlanning::KukaPathPlanning(string wcfile ) : KukaPathPlanning( WorkCellLoader::Factory::load(wcfile))
{
}

KukaPathPlanning::KukaPathPlanning(rw::models::WorkCell::Ptr workcell )
{
    // Load the workcell
    _wc = workcell;
    _state = _wc->getDefaultState();
    _defaultState = _wc->getDefaultState();


    // Get the frames used for kinematics
    _TCPFrame = _wc->findFrame("TCP");
    _cameraFrame = _wc->findFrame("Camera");
    _legoRefFrame = _wc->findFrame("LegoBrickREF");
    _imageFrame = _wc->findFrame("ImageFrame");

    // Error checks
    if (_TCPFrame == NULL) RW_THROW("TCP frame: " << _TCPFrame->getName() << " not found!");
    if (_cameraFrame == NULL) RW_THROW("Camera frame: " << _cameraFrame->getName() << " not found!");
    if (_legoRefFrame == NULL) RW_THROW("Lego REF frame: " << _legoRefFrame->getName() << " not found!");
    if (_imageFrame == NULL) RW_THROW("Image REF frame: " << _imageFrame->getName() << " not found!");


    // Get devices (robots)
     std::cout << " Get devices from workcell" << std::endl;
    const std::vector<rw::common::Ptr<Device> >& devices = _wc->getDevices();

    // Check if any (if not = no robot: return)
    if(devices.size() == 0)
    {
        std::cout << "No devices in the workcell" << std::endl;
        return;
    }

    uint _devicesLenght = devices.size();
    int _deviceNumber = -1;

    for( unsigned int i = 0; i < _devicesLenght; i++)
    {
        if((devices[i])->getName() == "KukaKr6")
        {
            _deviceNumber = i;
        }
    }

    std::cout << "Device number: " << _deviceNumber << std::endl;
    _robot = dynamic_cast<SerialDevice*>(devices[_deviceNumber].get());
    std::cout << "The robot is loaded: " << _robot->getName() << std::endl;

    //Default state
    _robot->setQ(Q(6, 0.0 , 0.0 , 0.0 , 0.0, 0.0 , 0.0), _state);

     _gripper = _wc->findDevice("PG70");
     if (_gripper == NULL) RW_THROW("Gripper device: " << _gripper->getName() << " not found!");

     _newState = _state;
}

void  KukaPathPlanning::run()
{
    // Setup ROS
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv, "kukapathplanning");
    _nodeHandleCommands = new ros::NodeHandle;

    // Setup ROS service clients
    _nodeHandleRobot = new ros::NodeHandle;

    // Robot services
    _serviceRobotGetConfiguration = _nodeHandleRobot->serviceClient<rsdkukapathplanner::getConfiguration>("/KukaNode/GetConfiguration");
    _serviceRobotSetConfiguration = _nodeHandleRobot->serviceClient<rsdkukapathplanner::setConfiguration>("/KukaNode/SetConfiguration");
    _serviceRobotgetQueueSize = _nodeHandleRobot->serviceClient<rsdkukapathplanner::getQueueSize>("/KukaNode/GetQueueSize");
    _serviceRobotIsMoving = _nodeHandleRobot->serviceClient<rsdkukapathplanner::getIsMoving>("/KukaNode/IsMoving");
    _serviceRobotStop = _nodeHandleRobot->serviceClient<rsdkukapathplanner::stopRobot>("/KukaNode/StopRobot");

    // Gripper services
    _servicePG70Home = _nodeHandleCommands->serviceClient<rsdkukapathplanner::Home>("PG70/home");
    _servicePG70Close = _nodeHandleCommands->serviceClient<rsdkukapathplanner::Close>("PG70/close");
    _servicePG70Stop = _nodeHandleCommands->serviceClient<rsdkukapathplanner::Stop>("PG70/stop");
    _servicePG70Move = _nodeHandleCommands->serviceClient<rsdkukapathplanner::Move>("PG70/move");
    _servicePG70Open = _nodeHandleCommands->serviceClient<rsdkukapathplanner::Open>("PG70/open");
    //_servicePG70GetQ = _nodeHandleGripper->serviceClient<rsdkukapathplanner::Open>("PG70/getQ");

    // Get the current state from ROS
    // Robot
    rsdkukapathplanner::getConfiguration getConfig;
    Q q_robot(6);

    // Call service
    if(!_serviceRobotGetConfiguration.call(getConfig))
    {
         ROS_ERROR("Failed to call the 'GetConfiguration'");
    }
    else
    {
        // Fill out information and update the robot configuration
        for(int i = 0; i < 6; i++)
        {
             q_robot(i) = getConfig.response.q[i];
        }
        _robot->setQ(q_robot,_state);
    }

    // Open gripper
    rsdkukapathplanner::Open open;
    open.request.power = 10.0;

    if(!_servicePG70Open.call(open))
        ROS_ERROR("Failed to call the 'servicePG70Open'");

    // Set the gripper open
    Q q_gripper(1, 0.035);
    _gripper->setQ(q_gripper,_state);

    // init
    this->initRRTPlanner();

    // Setup ROS service server
    _setGrip = _nodeHandleCommands->advertiseService(((string)"/nextBrick"), &KukaPathPlanning::callbackSetTCPGripPose, this);
    _setRobotTCP = _nodeHandleCommands->advertiseService(((string)"/setRobotImagePose"), &KukaPathPlanning::callbackSetImageREF, this);
    _qToQPlanning = _nodeHandleCommands->advertiseService(((string) "/goToQ"), &KukaPathPlanning::callbackSetRobotQ, this);
    _setRobotDeliver = _nodeHandleCommands->advertiseService(((string) "/setRobotDropOffPose"), &KukaPathPlanning::callbackSetDropOffREF, this);

    // Set loop rate
    ros::Rate loop_rate(100);

    while(_nodeHandleCommands->ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
}

bool KukaPathPlanning::setRobotQ(Q goToQ )
{
    // Robot
    ROS_INFO_STREAM("goToQ path planning called");

    _qMutex.lock();
    Q q_new(6);
    q_new = goToQ;
    _qMutex.unlock();

    Q qCurrent(6);
    qCurrent = _robot->getQ(_state);

    Path<Q> path;

    // TODO: TRY with linear motion in joint space first
    LinearInterpolator<Q> pathCheck(qCurrent, q_new, 1.0);

    State tempState = _state;
    _robot->setQ(goToQ, tempState);

    bool linearInCollision = false;
    bool RRTInCollision = false;
    bool goalInCollision = false;

    if(!_detector->inCollision(tempState, NULL, true))
    {
        // Lineary detection
        for(double dx = 0; dx <= 1.0 ; dx+= 0.001 )
        {
            _robot->setQ(pathCheck.x(dx), tempState);
            if(_detector->inCollision(tempState, NULL, true))
            {
                // Linear motion is in collision
                linearInCollision = true;
            }
        }

        if(linearInCollision)
        {
            // RRT Connect
            unsigned int maxTime = 10;
            RRTInCollision = queryRRT(qCurrent, q_new, path , maxTime, 0.05);
        }
        else
        {
            ROS_INFO("Linear joint space planning");
            path.push_back(q_new);
        }

        // Robot
        ROS_INFO_STREAM("After path planning call");
    }
    else
    {
        goalInCollision = true;
    }


    if((!goalInCollision && !RRTInCollision) || (!goalInCollision && !linearInCollision) )
    {
        rsdkukapathplanner::setConfiguration setConfig;
        // Fill out information and update the robot configuration
        // Call service

        for(unsigned int i= 0; i < path.size() ; i++)
        {
            // Fill out information and update the robot configuration
            for(int j = 0; j < 6; j++)
            {
                  setConfig.request.speed[j] = 40;
                  setConfig.request.q[j] = path[i](j);
            }

            ROS_INFO_STREAM("Set goToQ: " << path[i]);

            if(!_serviceRobotSetConfiguration.call(setConfig))
            {
                 ROS_ERROR("Failed to call the 'SetConfiguration'");
            }
            else
            {
                _robot->setQ(path[i], _state);
            }
        }

        return true;
    }

    ROS_INFO_STREAM("goToQ path planning finished");
    return false;
}

bool KukaPathPlanning::callbackSetRobotQ(rsdkukapathplanner::goToQ::Request &req, rsdkukapathplanner::goToQ::Response &res)
{
    ROS_INFO_STREAM("callback goToQ called");
    _qMutex.lock();

    bool collision_free=false;

    // Copi the data from res
    Q _goToQ(6);
    for(unsigned int i= 0; i < 6; i++)
    {
        _goToQ(i) = req.q[i];
    }
    _qMutex.unlock();

    // No reason to plan if the configuration is the same
    if(_goToQ != _robot->getQ(_state))
    {
        collision_free = setRobotQ(_goToQ);
    }

    // Feedback to the master controller
    if(!collision_free)
    {
        // Feedback the error to the master
        ROS_INFO_STREAM("PathPlanner error no collision free path");
    }

    return true;
}

bool KukaPathPlanning::callbackSetDropOffREF(rsdkukapathplanner::setRobotDropOff::Request &req,
                                          rsdkukapathplanner::setRobotDropOff::Response &res)
{
    rw::math::Q q_drop_off_pose(6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

//    Transform3D<double> baseTimageFrame = rw::kinematics::Kinematics::frameTframe(_robot->getBase(), _imageFrame, _state);
//    Transform3D<double> tcpTcam = rw::kinematics::Kinematics::frameTframe(_cameraFrame, _TCPFrame , _state);

//    Transform3D<double> transform_picture = baseTimageFrame * tcpTcam;

//    std::cout << "Transfrom picture: Trans: " << transform_picture.P() << "RPY: " << RPY<double>(transform_picture.R()) << std::endl;

//    if(inverseKinematics(transform_picture, q_drop_off_pose))
//    {
        setRobotQ(q_drop_off_pose);
//    }
    return true;
}


bool KukaPathPlanning::callbackSetImageREF(rsdkukapathplanner::setRobotImagePose::Request &req,
                                          rsdkukapathplanner::setRobotImagePose::Response &res)
{
    rw::math::Q q_image_pose(6);

    Transform3D<double> baseTimageFrame = rw::kinematics::Kinematics::frameTframe(_robot->getBase(), _imageFrame, _state);
    Transform3D<double> tcpTcam = rw::kinematics::Kinematics::frameTframe(_cameraFrame, _TCPFrame , _state);

    Transform3D<double> transform_picture = baseTimageFrame * tcpTcam;

    std::cout << "Transfrom picture: Trans: " << transform_picture.P() << "RPY: " << RPY<double>(transform_picture.R()) << std::endl;

    if(inverseKinematics(transform_picture, q_image_pose))
    {
        setRobotQ(q_image_pose);
    }
    return true;
}

bool KukaPathPlanning::callbackSetTCPGripPose(rsdkukapathplanner::nextBrick::Request &req,
                                          rsdkukapathplanner::nextBrick::Response &res)
{
    _poseMutex.lock();

    // Gimble lock?!
    //inverseKinematics( rw::kinematics::Kinematics::frameTframe(_TCPFrame, _imageFrame, _state),_state);
    //_robot->setQ(Q(6,-0.643, 0.350, -0.287, -0.193, 0.190, -2.226),_state);

   // ImageFrame seen in base
   Transform3D<double> baseTimageFrame = rw::kinematics::Kinematics::frameTframe(_robot->getBase(), _legoRefFrame , _state);
   //Transform3D<double> camTtcp = Transform3D<double>(Vector3D<double>( camFrameTtcpFrame.P()[0] , camFrameTtcpFrame.P()[1] , -camFrameTtcpFrame.P()[2]), camFrameTtcpFrame.R() );

   Transform3D<double> poseBricktcp = baseTimageFrame * Transform3D<double>(Vector3D<double>( req.pose.x , req.pose.y, 0.0),
                                           RPY<double>(req.pose.theta, 0.0, 0.0));

    // Convert to joint space (Inverse kinematics)
    rw::math::Q gripConfiguration(6);

    if(grapsPointSelction(poseBricktcp, gripConfiguration))
    {
        // Do the grasp
        ROS_INFO_STREAM("Graspning solution was found, going to the configuration \n" << gripConfiguration);
        setRobotQ(gripConfiguration);
    }
    else
    {
        // Failure
        ROS_INFO_STREAM("Error there where no graspning solution for the brick");

        // TODO: send feedback to excuter
    }



    _poseMutex.unlock();
    return true;
}

/*
 * Transform of the Grasping point relative to the Brick pose
 *
*/
rw::math::Transform3D<double> KukaPathPlanning::maketransform3d( rw::math::Transform3D<double> poseBrick, double theataOffset=0.0)
{
    return poseBrick*rw::math::Transform3D<double>(Vector3D<double>(0.0,0.0,0.0), RPY<double>(theataOffset,0.0,0.0));
}

/*
 * Generates grasp point bassed on a 6D Pose of the Brick
 *
*/
bool KukaPathPlanning::grapsPointSelction(rw::math::Transform3D<double> poseBrick, rw::math::Q& gripConfiguration)
{
    ROS_INFO("Grasp point selction called");
    bool solutionGrip = false;

    rw::math::Q q1(6);
    rw::math::Q q2(6);

    // Create two grasp points bassed on the brick (conversion from transform2D -> transform3D)
    Transform3D<double> grip1 = maketransform3d(poseBrick, -rw::math::Pi/2.0);
    Transform3D<double> grip2 = maketransform3d(poseBrick, rw::math::Pi/2.0);

     //ROS_INFO_STREAM("grip1 T: " << grip1.P() << " R: " << RPY<double>(grip1.R()));
     //ROS_INFO_STREAM("grip2 T: " << grip2.P() << " R: " << RPY<double>(grip2.R()));

    // Get inverse kinematic for both
    bool solutionGrip1 = inverseKinematics( grip1, q1);
    bool solutionGrip2 = inverseKinematics( grip2, q2);

    std::cout << "Grip 1:" << q1 << std::endl;
    std::cout << "Grip 2:" << q2 << std::endl;

    if(solutionGrip1 && solutionGrip2)
    {        
        // Select the grasp point which gives the shortest path
        if( abs( q1.norm2() -_robot->getQ(_state).norm2()) <=
                abs( q2.norm2() - _robot->getQ(_state).norm2()))
        {
            // Grip 1 is shortest
            gripConfiguration =  q1;
        }
        else
        {
            // Grip 2 is shortest
            gripConfiguration =  q2;
        }
        solutionGrip = true;
    }
    else if(solutionGrip1 )
    {
        // Only Grasp point 1 is good
        gripConfiguration =  q1;
        solutionGrip = true;
    }
    else if(solutionGrip2)
    {
        // Only Grasp point 2 is good
        gripConfiguration =  q2;
        solutionGrip = true;
    }
    else
    {
        // No solution at all
        ROS_INFO("No solution for grasp points");
        solutionGrip = false;
    }

    //ROS_INFO("Grasp point selction endded");
    return solutionGrip;
}

/*
 * Inverse kinematic for the robot
 * T desired must be converted to Kuka.Joint6
*/
bool KukaPathPlanning::inverseKinematics(rw::math::Transform3D<double> tcpPose, Q& inverseSolution )
{
    ROS_INFO("Inverse kinematics called");

    // Local variable
    bool invKinematic = false;
    rw::kinematics::State tempState = _state;

    ROS_INFO_STREAM( "Robot end: " << _robot->getEnd()->getName());

    // Base to TCP
    //Transform3D<double> baseTend = _robot->baseTend(_state);
    //Transform3D<double> endTtcp = Transform3D<double>(Vector3D<double>( 0 , 0, 0 ), RPY<double>(0.0, 0.0 ,0.0));
    Transform3D<double> endTtcp = rw::kinematics::Kinematics::frameTframe(_robot->getEnd(), _TCPFrame , _state);
    //Transform3D<double> baseTtcp = robot->baseTframe(_wc->findFrame("TCP"), _state);
    //Transform3D<double> robotposeEnd = baseTend * endTtcp * tcpPose;
     Transform3D<double> robotposeEnd = tcpPose * endTtcp;
    //Transform3D<double> robotposeEnd = baseTtcp * tcpPose;
     ROS_INFO_STREAM("poseInBase T: " << robotposeEnd.P() << " R: " << RPY<double>(robotposeEnd.R()));

    // Inverse kinematics solver
    rw::invkin::JacobianIKSolver iksolver(_robot, tempState);
    //rw::invkin::InvKinSolver iksolver(&_robot);

    // Meta solver to keep in the joint limits
    rw::invkin::IKMetaSolver mSolver(&iksolver, _robot, _detector);

    // Generate joint configurations
    std::vector<Q> result;
    result = mSolver.solve( robotposeEnd , tempState, 1000, false);

    if(result.size() == 0)
    {
        // No solutions
        invKinematic = false;
        ROS_INFO("No inverse kinematics solution");
    }
    else
    {
        Q bestQ(6);
        double differenceQ = 0.0;
        double bestN2Q = 100000.0;

        // Find the closets solution
        for( auto it = result.begin() ; it != result.end(); ++it)
        {
            // Get the current Q
            differenceQ = abs(it->norm2() - _robot->getQ(_state).norm2());

            if(differenceQ < bestN2Q)
            {
                bestQ = *it;
                bestN2Q = differenceQ;
            }
        }

        invKinematic = true;
        inverseSolution = bestQ;
        //ROS_INFO("A inverse kinematic solution has been found");
    }

    return invKinematic;
}

/*
 * Inits the RRT Connect planner
 *
*/
void KukaPathPlanning::initRRTPlanner()
{
   _detector = new CollisionDetector(_wc, ProximityStrategyFactory::makeDefaultCollisionStrategy() );
   _qMetric = MetricFactory::makeEuclidean<Q>();
}


/*
 * Get a Q Path from the RRT planner
 *
*/
bool KukaPathPlanning::queryRRT( Q qStart, Q qGoal, Path<Q> &path, unsigned int maxTime, double epsilon)
{
    ROS_INFO_STREAM("Call queryRRT");

    PlannerConstraint _constraint = PlannerConstraint::make(_detector, _robot, _state);
    QSampler::Ptr sampler = QSampler::makeConstrained(
            QSampler::makeUniform(_robot),
            _constraint.getQConstraintPtr()
    );

    QToQPlanner::Ptr  _rrtPlanner = RRTPlanner::makeQToQPlanner(
            _constraint,
            sampler,
            _qMetric,
            epsilon,
            RRTPlanner::RRTConnect
            );

    ROS_INFO_STREAM("run queryRRT");
    bool pathFound = _rrtPlanner->query(qStart, qGoal, path, maxTime);

    std::cout << "RRT found path " << to_string(pathFound) << " size: " << path.size() << std::endl; ;

    if(path.size() > 1)
    {
        path.erase(path.begin());
    }

    if(path.size() > 4 )
    {
        ROS_INFO_STREAM("push back path");
        PathLengthOptimizer lengthOptimizer = PathLengthOptimizer(_constraint, _qMetric);
        lengthOptimizer.shortCut(path);
    }
    else if( path.size() < 1)
    {
        ROS_INFO_STREAM("push back single goal");
        path.push_back(qGoal);
    }
    ROS_INFO_STREAM("qeueryRRT end");

    return pathFound;
}

KukaPathPlanning::~KukaPathPlanning()
{

}

