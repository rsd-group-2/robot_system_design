﻿#ifndef SampleWIDGET_HPP
#define SampleWIDGET_HPP

#include <QObject>

// Includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>

// RobWork
#include <rw/math/Constants.hpp>
#include <rw/math/Math.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/kinematics/FKRange.hpp>
#include <rw/kinematics/State.hpp>
#include <rwlibs/task/Task.hpp>
#include <rw/invkin/IKMetaSolver.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>
#include <rws/RobWorkStudio.hpp>

#include "nodethread.hpp"

// QT
#include "ui_SampleWidget.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QWidget>
#include <QTimer>


#include <boost/foreach.hpp>

// Namespace
using namespace rw::math;
using namespace rw::models;
using namespace std;

// Class
class SampleWidget : public QWidget, private Ui::SampleWidgetClass
{
Q_OBJECT

public:
        SampleWidget(QWidget *parent = 0);
        ~SampleWidget();
        void initialize(rw::models::WorkCell::Ptr workcell, rws::RobWorkStudio* rws);
        void stateChangedListener(const rw::kinematics::State &state);

private slots:
        void callback();
        void eventBtn();
        void update_gui_q(rw::math::Q robot, bool gripper_open);
        void checkbox_update_gui_q();

signals:
        void setQ(rw::math::Q q);
        void gripper_open_close();

private:
	    bool openWorkCell();

        bool _gripperOpen;
        bool _update_gui_from_ros;

        std::mutex _mutex_update_gui__ros;

        ofstream outputfile;

        NodeThread* _nodethread;

        QThread* _worker_nodethread;

	    rw::models::WorkCell::Ptr _rwWorkCell;
	    rw::models::SerialDevice *_device;
        rw::models::Device::Ptr _gripper;
        rw::kinematics::State _state, _newState;
	    rws::RobWorkStudio *_rws;
};

#endif // SampleWIDGET_HPP
