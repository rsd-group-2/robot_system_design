#ifndef NODETHREAD_HPP
#define NODETHREAD_HPP

#include <QObject>

#include <mutex>

#include <ros/ros.h>
// Gripper
#include <rsdkukarwsplugin/Move.h>
#include <rsdkukarwsplugin/Stop.h>
#include <rsdkukarwsplugin/Home.h>
#include <rsdkukarwsplugin/Close.h>
#include <rsdkukarwsplugin/Open.h>

// Robot
#include <rsdkukarwsplugin/goToQ.h>
#include <rsdkukarwsplugin/getConfiguration.h>

#include <rw/math.hpp>
#include <rw/math/Math.hpp>
#include <rw/kinematics.hpp>

#include <QThread>
#include <QMetaType>
#include <QTimer>

class NodeThread : public QObject
{
    Q_OBJECT

public slots:
    void rosrun();
    void gripper_control();
    void set_q_slot(rw::math::Q q_new);
    void refresh_q_GUI();

signals:
    void update_gui_Q(rw::math::Q robot, bool gripper_open);


public:
    NodeThread();

private:

    void update_gui_image();

    //std::mutex _mutex_refresh_rate;
    //double _gui_refresh_rate;

    bool _gripperOpen;

    // Timer update
    QTimer* _gui_refresh_timer;
    QTimer* _ros_refresh_timer;

    // ROS
    ros::NodeHandle *_nodeHandle;
    ros::ServiceClient _servicePG70Home;
    ros::ServiceClient _servicePG70Close;
    ros::ServiceClient _servicePG70Stop;
    ros::ServiceClient _servicePG70Move;
    ros::ServiceClient _servicePG70Open;
    ros::ServiceClient _servicegoToQ;
    ros::ServiceClient _serviceGetQ;
};

#endif // NODETHREAD_HPP
