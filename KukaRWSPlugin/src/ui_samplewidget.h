/********************************************************************************
** Form generated from reading UI file 'SampleWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAMPLEWIDGET_H
#define UI_SAMPLEWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SampleWidgetClass
{
public:
    QVBoxLayout *verticalLayout;
    QPushButton *_moverobot;
    QPushButton *_resetrobot;
    QPushButton *_homerobot;
    QPushButton *_gripperbtn;

    void setupUi(QWidget *SampleWidgetClass)
    {
        if (SampleWidgetClass->objectName().isEmpty())
            SampleWidgetClass->setObjectName(QString::fromUtf8("SampleWidgetClass"));
        SampleWidgetClass->resize(172, 374);
        verticalLayout = new QVBoxLayout(SampleWidgetClass);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        _moverobot = new QPushButton(SampleWidgetClass);
        _moverobot->setObjectName(QString::fromUtf8("_moverobot"));

        verticalLayout->addWidget(_moverobot);

        _resetrobot = new QPushButton(SampleWidgetClass);
        _resetrobot->setObjectName(QString::fromUtf8("_resetrobot"));

        verticalLayout->addWidget(_resetrobot);

        _homerobot = new QPushButton(SampleWidgetClass);
        _homerobot->setObjectName(QString::fromUtf8("_homerobot"));

        verticalLayout->addWidget(_homerobot);

        _gripperbtn = new QPushButton(SampleWidgetClass);
        _gripperbtn->setObjectName(QString::fromUtf8("_gripperbtn"));

        verticalLayout->addWidget(_gripperbtn);


        retranslateUi(SampleWidgetClass);

        QMetaObject::connectSlotsByName(SampleWidgetClass);
    } // setupUi

    void retranslateUi(QWidget *SampleWidgetClass)
    {
        SampleWidgetClass->setWindowTitle(QApplication::translate("SampleWidgetClass", "Rob01 Milling Player", 0, QApplication::UnicodeUTF8));
        _moverobot->setText(QApplication::translate("SampleWidgetClass", "Move robot", 0, QApplication::UnicodeUTF8));
        _resetrobot->setText(QApplication::translate("SampleWidgetClass", "Reset robot in RWS", 0, QApplication::UnicodeUTF8));
        _homerobot->setText(QApplication::translate("SampleWidgetClass", "Home Robot", 0, QApplication::UnicodeUTF8));
        _gripperbtn->setText(QApplication::translate("SampleWidgetClass", "Open/Close Gripper", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SampleWidgetClass: public Ui_SampleWidgetClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAMPLEWIDGET_H
