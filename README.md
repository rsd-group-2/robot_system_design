This repository is for all the code related to the robot system design project.

Remember to 'make clean' (or 'catkin_make clean') your code before you commit.
Otherwise the repository becomes filled with automatically generated files that we don't need to share.

We can use the issue tracker on the site to write down eventual bugs and maybe also the tasks. 
The issues in the issue tracker can be assigned to specific people and thus we can see who is working on what.

Gitlab also provides wiki pages for our project which could be used to document the ROS nodes that we make.