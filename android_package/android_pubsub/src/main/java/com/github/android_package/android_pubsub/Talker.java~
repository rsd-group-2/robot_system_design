/*
 * Copyright (C) 2014 Chris Sørensen.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.rosjava.android_package.android_pubsub;

import org.ros.concurrent.CancellableLoop;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

/**
 * A simple {@link Publisher} {@link NodeMain}.
 */
public class Talker extends AbstractNodeMain {
  private String topic_name;
  private String control_mode;
  private String tipper;
  private String navigation_control;
  private String move_to_state;

  private boolean send = false; 

  public Talker() {
    topic_name = "chatter";
    control_mode = "";
    tipper = "";
    navigation_control = "";
    move_to_state = "";
  }

  public Talker(String topic)
  {
    topic_name = topic;
    control_mode = "";
    tipper = "";
    navigation_control = "";
    move_to_state = "";
  }

  public void set_Control_mode(String android_text)
  {
    control_mode = android_text;
    send = true;
  }

  public void set_tipper(String android_text)
  {
    tipper = android_text;
    send = true;
  }

  public void set_navigation(String android_text)
  {
    navigation_control = android_text;
    send = true;
  }

  public void set_state(String android_text)
  {
    move_to_state = android_text;
    send = true;
  }	

  @Override
  public GraphName getDefaultNodeName() {
    return GraphName.of("rosjava/talker");
  }

  @Override
  public void onStart(final ConnectedNode connectedNode) {
    final Publisher<std_msgs.String> publisher =
        connectedNode.newPublisher(topic_name, std_msgs.String._TYPE);
    // This CancellableLoop will be canceled automatically when the node shuts
    // down.
    connectedNode.executeCancellableLoop(new CancellableLoop() {
      private int sequenceNumber;

      @Override
      protected void setup() {
        sequenceNumber = 0;
      }

      @Override
      protected void loop() throws InterruptedException {
        std_msgs.String str = publisher.newMessage();
   
	str.setData(control_mode + "," + navigation_control + "," + tipper + "," + move_to_state);        
	if (send)
	{	
		publisher.publish(str);
		send = false;

		control_mode = "";
		navigation_control = "";
		tipper = "";
		move_to_state = "";
	}
        Thread.sleep(1);
      }
    });
  }
}
