#ifndef _BRICK_DETECTOR_H_
#define _BRICK_DETECTOR_H_

#include <opencv2/opencv.hpp>
#include <vector>
#include "ros/ros.h"
#include "brick_detector/RequestBricks.h"
#include "brick_detector/Brick.h"
#include "sensor_msgs/Image.h"
#include "cv_bridge/cv_bridge.h"
#include "image_transport/image_transport.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/CameraInfo.h"

struct BrickPos {
	double x, y, theta;
};

struct BrickInfo {
	BrickPos position;
	double length;
	int color;
	cv::RotatedRect bounding_box;
};

enum class Color { RED = 0, BLUE = 1, YELLOW = 2, INVALID = -1};

class BrickDetector {
public:
    BrickDetector(ros::NodeHandle& nh);

	bool requestBricks(brick_detector::RequestBricks::Request& req, brick_detector::RequestBricks::Response& res);

	void imageCallback(const sensor_msgs::ImageConstPtr& img);
	void cameraInfoCallback(const sensor_msgs::CameraInfo& msg);
		
	cv::Mat getMarkedImage() { return _detected_image; }

private:
	cv::Mat _current_image;
	cv::Mat _detected_image;
	cv::Mat _image, _hsv_image;
	cv::Mat _hue, _saturation, _value;

	std::vector<BrickInfo> detect(cv::Mat image);

	std::shared_ptr<ros::NodeHandle> _nh;
	std::shared_ptr<image_transport::ImageTransport> _img_trans;
	image_transport::Publisher _pub;
	image_transport::Subscriber _sub;
	ros::ServiceServer _detect_service;

	std::shared_ptr<sensor_msgs::CameraInfo> _cam_info;

	void getHSVImages();
	void removeConveyor();
	cv::MatND getHueHistogramFromContour(std::vector<cv::Point> contour);
	void splitMultiColorContour(std::vector<cv::Point> curr_contour, std::vector< std::vector<cv::Point> >& contours);
	void splitConcaveContour(std::vector<cv::Point> curr_contour, std::vector< std::vector<cv::Point> >& contours);
	int countHistogramPeaks(cv::MatND hist);
	Color getColorCodeFromHistogram(cv::MatND hist);
	BrickPos getPose(cv::RotatedRect bounding_box, cv::Mat& img);
	cv::Point shortestBBToBBDistance(cv::RotatedRect a, cv::RotatedRect b);
	double clampToRange(double n, double min, double max);
	cv::Point closestPointSegmentToSegment(cv::Point p1, cv::Point q1, cv::Point p2,  cv::Point q2);

};

#endif /* _BRICK_DETECTOR_H */
