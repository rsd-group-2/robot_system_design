#include "ros/ros.h"
#include "BrickDetector.h"
//#include "image_transport/image_transport.h"

int main(int argc, char **argv) {
	ros::init(argc, argv, "brick_detector_node");
	ros::NodeHandle n;
	BrickDetector detector(n);
	
	ros::spin();
}
