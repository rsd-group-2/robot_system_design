#include "fsm_nav.h"

//NOTE :
//-add waiting for order to enable disable the navigation;
//-add error handling 

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
//MoveBaseClient ac("move_base", true);

using namespace fsm_nav;
        
void navigation_fsm::poseReceivedCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg){
		_pos= *msg;
		std::cout <<"\tx:"<<_pos.pose.pose.position.x << "\ty:" << _pos.pose.pose.position.y << "\torientation:" << _pos.pose.pose.orientation.z << std::endl;
}

void navigation_fsm::robotStateCallback(const std_msgs::String::ConstPtr& str)
{
	// Check if the state has changed from line_follower to waypoint
	ROS_ERROR("Recieved string: %s", str->data.c_str());

	if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE == SLAMstate::LINE_FOLLOWER_ACTIVE)
	{	
		ROS_ERROR("Change from line to waypoint");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	} 
	// If the robot is in an idle state, start doing something
	else if (str->data.compare("Idle") != 0 && NAV_STATE == SLAMstate::IDLE)
	{
		ROS_ERROR("state changed to charging");
		NAV_STATE = SLAMstate::CHARGING_STATION;
	}
	// DEBUG
	else if (str->data.compare("Idle") != 0 && NAV_STATE != SLAMstate::IDLE)
	{
		ROS_ERROR("state changed to LINE_FOLLOWER_BACK");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	}
		
		robot_state=*str;
		ROS_INFO("ROBOT STATE:[%s]",str->data.c_str());
		
}
//class constructor
navigation_fsm::navigation_fsm()
{
//		ros::init(0,"pik", "FSM_MACHINE");
		ROS_ERROR("Waypoint fsm started");
		//real vel topic waypoint_slam_navigation_vel_topic
		_vel = nh.advertise<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 10);
	 	//_vel = nh.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel",10);
	 	_pose = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/amcl_pose",10,&navigation_fsm::poseReceivedCallback,this);
		pub_decider = nh.advertise<std_msgs::String>("/main_state_topic",10);
		_sub_decider = nh.subscribe<std_msgs::String>("/main_state_topic", 10, &navigation_fsm::robotStateCallback, this);
		navigation_fsm::init();
}

void navigation_fsm::init()
{
		
		//temp
		NAV_STATE = SLAMstate::IDLE;
		//NAV_STATE = CHARGING_STATION;
		//define zones in the map and assign the initial state depending on the localization

}
//wrapper of the state machine
void navigation_fsm::fsmWrapper()
{
	
	//if(strcmp(robot_state.data.c_str(),"order_received") || strcmp(robot_state.data.c_str(),"SLAM_Navigation") || strcmp(robot_state.data.c_str(),"Waypoint_Navigation"))
	//{
		STATE=ENABLED;
	//}
	//else
		//STATE=DISABLED;	
	
	if(STATE == ENABLED)
	{

		switch(NAV_STATE)
		{
			case SLAMstate::IDLE:
			// Await next order.. Callback is used to set the state.
			break;
			case(CHARGING_STATION):
			//switch to order received to slam navigation
			//navigation_fsm::switchArea();
			navigation_fsm::disengaging();
			break;
			case(DISENGAGED):
			navigation_fsm::navigateToDispenser();
			break;
			case(BRICK_DISPENSER):
			navigation_fsm::leaveBox();
			break;
			case(BOX_OUT):
			//switch to wpt navigation state
			//navigation_fsm::switchArea();	
			navigation_fsm::wptNavigation();
			break;
			case(LINE_FOLLOWER):
			main_state_str_.data = "Line_Navigation";
			pub_decider.publish(main_state_str_);
			NAV_STATE = SLAMstate::LINE_FOLLOWER_ACTIVE;
			break;
			case LINE_FOLLOWER_ACTIVE:
			// Do nothing. Go masturbate or something.
			break;
			case(LINE_FOLLOWER_BACK):
			navigation_fsm::wptnavigationBack();
			break;
			case(BOX_IN):
			//switch to slam navigation state
			//navigation_fsm::switchArea();
			navigation_fsm::approachingChargingStation();
			case(DOCKING):
			//switch to waiting for order state
			//navigation_fsm::switchArea();
			navigation_fsm::docking();
			break;
			case(ERROR):
			//define error solving function
			break;
		}
	}
}
//disengage the robot from the charging station
void navigation_fsm::disengaging(){
	
	ROS_INFO("DISENGAGING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;
	double x_des = -0.508;	
	while(_pos.pose.pose.position.x > x_des && ros::ok()){
		
		twist.header.stamp = ros::Time::now();
		twist.twist.linear.x = -0.2;
		
		//if(_pos.pose.pose.position.x == -0.707163016193){
			//setSlamState(DISENGAGED);
			//break;
		//}
		
	_vel.publish(twist);
	ros::spinOnce();
	}
	setSlamState(DISENGAGED);
}
//navigate to brick dispenser
void navigation_fsm::navigateToDispenser(){
	MoveBaseClient ac("move_base", true);
	
	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("APPROACHING THE LEGO DISPENSER");
	
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -1.230;
	goal.target_pose.pose.position.y = 0.401;
	goal.target_pose.pose.orientation.z = -0.99;
//	goal.target_pose.pose.orientation.w = 1;
	ac.sendGoal(goal);
	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("LEGO DISPENSER REACHED");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	setSlamState(BRICK_DISPENSER);
}
//change goal coordinates with real one
void navigation_fsm::leaveBox(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("MOVING TO THE BOX EXIT");
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -0.848;
	goal.target_pose.pose.position.y =  4.3;//3.873;
	goal.target_pose.pose.orientation.z = 0;//0.7679;
	goal.target_pose.pose.orientation.w = 1;
	ac.sendGoal(goal);
	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("READY TO MOVE OUTSIDE");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	setSlamState(BOX_OUT);

}

//wptnav to line MARK switch to LINE_FOLLOWER
void navigation_fsm::wptNavigation()
{
	if (wpt_nav_.moveFromBoxToLine() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		setSlamState(LINE_FOLLOWER);
	} else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
}
//wptnav from line MARK switch to BOX_IN in the end
void navigation_fsm::wptnavigationBack()
{
	if (wpt_nav_.moveFromLineToBox() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		setSlamState(BOX_IN);
	} else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
}

void navigation_fsm::approachingChargingStation(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("PREPARING FOR DOCKING");

	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -1.476;
	goal.target_pose.pose.position.y = 0.309;
	goal.target_pose.pose.orientation.z = 3.000;
	
	ac.sendGoal(goal);
	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("WAITING FOR DOCKING");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	setSlamState(DOCKING);


}

void navigation_fsm::docking(){

	ROS_INFO("APPROACHING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;
	
	while(ros::ok()){
	
		twist.header.stamp = ros::Time::now();
		twist.twist.linear.x = 0.2;
		
		// Set the NAV state to idle and same for mission executioner
		if(_pos.pose.pose.position.x == 0.0){
			setSlamState(SLAMstate::IDLE);
			main_state_str_.data = "Idle";
			pub_decider.publish(main_state_str_);
			//setSlamState(CHARGING_STATION);
			break;
		}
		
	_vel.publish(twist);
	}
}

void navigation_fsm::setSlamState(SLAMstate state){
		
		NAV_STATE_PREVIOUS=NAV_STATE;
		NAV_STATE=state;
}


void navigation_fsm::switchArea(){
		
		std::stringstream ss;
		std_msgs::String msg;

		if(strcmp(robot_state.data.c_str(),"order_received"))
			ss << "SLAM_Navigation";
		else if(strcmp(robot_state.data.c_str(),"SLAM_Navigation "))
			ss << "Waypoint_Navigation";
		else if(strcmp(robot_state.data.c_str(),"Waypoint_Navigation"))
			ss << "SLAM_Navigation";
		else if(NAV_STATE_PREVIOUS == DOCKING)
			ss << "Waiting_for_order";
		
		msg.data = ss.str();
		pub_decider.publish(msg);
}	



//topic for orders:order_topic string








