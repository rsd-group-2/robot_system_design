#include "waypoint_navigator.hpp"

using namespace WayPointNav;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

/**
 *@brief Constructor. Initializes all the variables
**/
template <class T>
WptNav<T>::WptNav() : wpt_state_(WPTStackResult::IDLE)
{

    pub_ = n_.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1);
    pub_stop_ = n_.advertise<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 1);
    map_serv_ = n_.advertiseService("waypoint_map", &WptNav::WayPointMapCallback, this);

    // Initialize the arrays
    //box_to_line_waypoints[0] = WayPoint<T>(-0.535177, 4.1404, 0.0);
    box_to_line_waypoints[0] = WayPoint<T>(0.97748, 3.64616, -0.936974);
    box_to_line_waypoints[1] = WayPoint<T>(2.31796, 1.93919, -0.964081);

    line_to_box_waypoints[0] = WayPoint<T>(1.65546, 4.14129, 3.09398);
    line_to_box_waypoints[1] = WayPoint<T>(-0.861,  3.828, -1.647);  
    //line_to_box_waypoints[2] = WayPoint<T>(-1.1098, 1.5243, -1.59727);

    line_corr_wpt_ = WayPoint<T>(2.75387, 1.7525, -0.90);
    line_fail_wpt_ = WayPoint<T>(2.062, 2.467, -1.018);

    // Initialize the line parameters
    line_.start = Point<T>(2.79034, 1.67388);
    line_.end = Point<T>(3.3961, 0.763323);
}
/**
 *@brief Destructor
**/
template <class T>
WptNav<T>::~WptNav() {}

/**
 *@brief Move to the waypoint given by the input
 *
 *@param wpt The waypoint to move to
 *
 *@return Returns the result of the waypoint movement
**/
template <class T>
WPTStackResult WptNav<T>::moveToWpt(WayPoint<T> wpt)
{
    ROS_INFO("Moving base");
    MoveBaseClient move_base_client_("move_base", true);

    // Wait for the action server to come up
    while(!move_base_client_.waitForServer(ros::Duration(MOVE_BASE_WAIT_DURATION)))
    {
        ROS_INFO("Waiting for the move_base action server to come up");
    }
    // Setup up the goal pose
    goal_.target_pose.header.frame_id = "map";
    goal_.target_pose.header.stamp = ros::Time::now();

    tf::Quaternion q = tf::createQuaternionFromRPY(0,0,wpt.theta);

    goal_.target_pose.pose.position.x = wpt.x;
    goal_.target_pose.pose.position.y = wpt.y;
    goal_.target_pose.pose.position.z = 0;
    goal_.target_pose.pose.orientation.x = q.getX();
    goal_.target_pose.pose.orientation.y = q.getY();
    goal_.target_pose.pose.orientation.z = -q.getZ();
    goal_.target_pose.pose.orientation.w = -q.getW();
    //std::cout << "Quaternions: " << q.getX() << " " << q.getY() << " " << -q.getZ() << " " << -q.getW() << std::endl;

    ROS_INFO("Moving towards the goal");
    move_base_client_.sendGoal(goal_);
    wpt_state_ = WPTStackResult::MOVING_TO_WAYPOINT;

    move_base_client_.waitForResult();

    if(move_base_client_.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        ROS_INFO("Waypoint successfully reached");
        wpt_state_ = WPTStackResult::WAYPOINT_SUCCESSFULLY_REACHED;
        return WPTStackResult::WAYPOINT_SUCCESSFULLY_REACHED;
    }
    else
    {
        ROS_INFO("The base failed to move to the desired waypoint");
        wpt_state_ = WPTStackResult::ERROR_REACHING_WAYPOINT;
        return WPTStackResult::ERROR_REACHING_WAYPOINT;
    }
}

/**
 *@brief Move to the waypoint given by the input
 *
 *@param x, y coordinates and theta angle
 *
 *@return Returns the result of the waypoint movement
**/
 template <typename T>
 WPTStackResult WptNav<T>::moveToWpt(double x, double y, double theta)
 {
    WayPoint<T> wpt;
    wpt.x = x;
    wpt.y = y;
    wpt.theta = theta;

    return(WptNav::moveToWpt(wpt));
 }

/**
 * Set the initial pose of the robot
 * using the marker locator
 *
 * @return State of the set initial pose
**/
template <class T>
WPTStackResult WptNav<T>::setInitialPose()
{
    geometry_msgs::PoseWithCovarianceStamped initial_pose;

    // Header and frame
    initial_pose.header.stamp = ros::Time::now();
    initial_pose.header.frame_id = "map";

    marker_locator::marker_locator_msg marker_locator_msg = WptNav::getMarkerData();

    WayPoint<T> marker_locator_wpt(marker_locator_msg.x, marker_locator_msg.y, marker_locator_msg.theta);
    geometry_msgs::PoseStamped map_locator_pose;
    if (WptNav::transformMarkerToMap(marker_locator_wpt, map_locator_pose) == TRANSFORM_SUCCESSFUL)
    {
        initial_pose.pose.pose.position.x = map_locator_pose.pose.position.x;       
        initial_pose.pose.pose.position.y = map_locator_pose.pose.position.y;
        initial_pose.pose.pose.orientation.x = map_locator_pose.pose.orientation.x;
        initial_pose.pose.pose.orientation.y = map_locator_pose.pose.orientation.y;
        initial_pose.pose.pose.orientation.z = map_locator_pose.pose.orientation.z;// + QUATERNION_Z_OFFSET;
        initial_pose.pose.pose.orientation.w = map_locator_pose.pose.orientation.w;// + QUATERNION_W_OFFSET;
        initial_pose.pose.covariance[0] = COV_X;
	    initial_pose.pose.covariance[7] = COV_Y;
	    initial_pose.pose.covariance[35] = COV_THETA;
	    pub_.publish(initial_pose);
        return INITIAL_POSE_SUCCESSFULLY_SET;
    } else
    {
        std::cerr << "Error setting initial pose" << std::endl;
        return ERROR_SETTING_INITIAL_POSE;
    }
}

/**
 * @brief Move the robot from the box to the start of the line
 *
 * @return Returns the result of the waypoint movement
**/
template <class T>
WPTStackResult WptNav<T>::moveFromBoxToLine()
{
    // Get the initiali pose
    WptNav::setInitialPose();

    // Move the robot
    for(WayPoint<T> way_point : box_to_line_waypoints)
    {
        if(WptNav::moveToWpt(way_point) != WAYPOINT_SUCCESSFULLY_REACHED)
        {
            // Update the current position and try reaching the waypoint again
            WptNav::setInitialPose();
            if(WptNav::moveToWpt(way_point) != WAYPOINT_SUCCESSFULLY_REACHED)
                return ERROR_REACHING_WAYPOINT;
        }
    }

    if (WptNav::correctLinePosition() != WAYPOINT_SUCCESSFULLY_REACHED)
    {
        // Move to fail error line position
        WptNav::moveToWpt(line_fail_wpt_);
        // Try reaching the line again
        WptNav::correctLinePosition();
    }

    std::cout << "Reached the destination" << std::endl;
    return WAYPOINT_SUCCESSFULLY_REACHED;
}

/**
 * @brief Move the robot from the box to the start of the line
 *
 * @return Returns the result of the waypoint movement
**/
template <class T>
WPTStackResult WptNav<T>::moveFromLineToBox()
{
    // Get the initiali pose
    WptNav::setInitialPose();

    // Move the robot
    for(WayPoint<T> way_point : line_to_box_waypoints)
    {
        if(WptNav::moveToWpt(way_point) != WAYPOINT_SUCCESSFULLY_REACHED)
        {
            // Update the current position and try reaching the waypoint again.
            // Else return error reaching waypoint.
            WptNav::setInitialPose();
            if(WptNav::moveToWpt(way_point) != WAYPOINT_SUCCESSFULLY_REACHED)
                return ERROR_REACHING_WAYPOINT;
        }
    }

    std::cout << "Reached the destination" << std::endl;
    return WAYPOINT_SUCCESSFULLY_REACHED;
}



template <class T>
WPTStackResult WptNav<T>::correctBoxInPosition()
{
    // Get the new pose of the frobit
    WPTStackResult result;
    if (WptNav::setInitialPose() != INITIAL_POSE_SUCCESSFULLY_SET)
    {
        return ERROR_REACHING_WAYPOINT;
    }

    WayPoint<T> correct_point(-0.780868, 3.73659, -1.66672);
    if (WptNav::moveToWpt(correct_point) != WAYPOINT_SUCCESSFULLY_REACHED)
        return ERROR_REACHING_WAYPOINT;

    return WAYPOINT_SUCCESSFULLY_REACHED;
}

/**
 * @brief Corrects the position of frobit relative to the line if neccesary
 *
 * @return SUCCESS or ERROR
**/
template <class T>
WPTStackResult WptNav<T>::correctLinePosition()
{
    double distance(0.0), angle(0.0);

    // Get the new pose of the frobit
    WPTStackResult result;
    if (WptNav::setInitialPose() != INITIAL_POSE_SUCCESSFULLY_SET)
    {
        return ERROR_REACHING_WAYPOINT;
    }

    // Get marker locator data and transform to map
    marker_locator::marker_locator_msg marker_locator_msg = WptNav::getMarkerData();
    geometry_msgs::PoseStamped pose;
    WptNav::transformMarkerToMap(WayPoint<T>(marker_locator_msg.x, marker_locator_msg.y, marker_locator_msg.theta), pose);
    WayPoint<T> map_wpt(pose.pose.position.x, pose.pose.position.y, getYawFromQuaternion(pose));

    // Point on the map and the line
    Point<T> p(map_wpt.x, map_wpt.y);
    Point<T> p_line = lineFeatures_.closestPointLineSegment(line_, p);

    // Find the distance to the line. Check if above or below threshold
    distance = lineFeatures_.distBetweenPoints(p, p_line);
    Vector2d frobit_vec (std::cos(map_wpt.theta), std::sin(map_wpt.theta));
    Vector2d line_vec (line_.end.x - line_.start.x, line_.end.y - line_.start.y);
    angle = lineFeatures_.angleBetweenVectors(frobit_vec, line_vec);

    ROS_ERROR("Distance and angle are: %f and %f", distance, angle);
    if(distance > LINE_DIST_THRESH || angle > LINE_THETA_THRESH)
    {
        //  Relocate the robot to the start of the line
        if (WptNav::moveToWpt(line_corr_wpt_) != WAYPOINT_SUCCESSFULLY_REACHED)
            return ERROR_REACHING_WAYPOINT;

        result = LINE_CORRECTED;

    }
    else
    {
        ROS_ERROR("LINE UNCORRECTED");
        result = LINE_UNCORRECTED;
    }

    // Check if the new point is satifised
    if (result == LINE_CORRECTED)
    {
        line_correct_cnt_++;
        if(line_correct_cnt_ >= LINE_CORRECT_TRIALS)
        {
            // Move back behind the line and try again. Ignore end result
            WptNav::moveToWpt(line_fail_wpt_);
            line_correct_cnt_ = 0;
        }
        // Try to relocate position to start of line.
        WptNav::correctLinePosition();
    }

    return WAYPOINT_SUCCESSFULLY_REACHED;
}

template <class T>
bool WptNav<T>::WayPointMapCallback(marker_locator::MarkerLocatorData::Request &req, marker_locator::MarkerLocatorData::Response &res)
{
    tf::StampedTransform transform_in_map;
    try 
    {
        listener_.waitForTransform("map", "base_link", ros::Time(0), ros::Duration(5));
        listener_.lookupTransform("map", "base_link", ros::Time(0), transform_in_map);
        double roll, pitch, yaw;
        tf::Matrix3x3 m(transform_in_map.getRotation());
        m.getRPY(roll, pitch, yaw);

        res.marker_x = transform_in_map.getOrigin().x();
        res.marker_y = transform_in_map.getOrigin().y();
        res.marker_theta = yaw;
    }
    catch (tf::TransformException &exception)
    {
        ROS_ERROR("%s", exception.what());
        return false;
    }

    return true;
}

/**
 * @brief Retrieves the newest waypoint from the service in Marker Locator Class
 *
 * @param frame The frame in which the output is given
 *
 * @return Waypoint containing x, y and theta
**/
template <class T>
WayPoint<T> WptNav<T>::getNewestWayPoint(std::string frame)
{
    // Get the marker data
    marker_locator::marker_locator_msg msg = WptNav::getMarkerData();
    WayPoint<T> wpt (msg.x, msg.y, msg.theta);
    if (std::strcmp(frame.c_str(), "map"))
    {
        geometry_msgs::PoseStamped pose;
        WptNav::transformMarkerToMap(wpt, pose);
        wpt.x = pose.pose.position.x;
        wpt.y = pose.pose.position.y;
        wpt.theta = WptNav::getYawFromQuaternion(pose);
    }
    // If frame is not map, simply return the waypoint in the marker frame
    return wpt;
}

/**
 * @brief Transforms the waypoint from the marker frame to the map
 *
 * @param marker_locator_pos Position given from the marker locator
 *
 * @return The transformed waypoint.
**/
template <class T>
WPTStackResult WptNav<T>::transformMarkerToMap(WayPoint<T> marker_locator_pos, geometry_msgs::PoseStamped& map_locator_pose)
{
    geometry_msgs::PoseStamped marker_locator_pose;

    marker_locator_pose.header.stamp = ros::Time::now();
    marker_locator_pose.header.frame_id = "marker_link";
    marker_locator_pose.pose.position.x = marker_locator_pos.x;
    marker_locator_pose.pose.position.y = marker_locator_pos.y;
    marker_locator_pose.pose.position.z = 0;
    tf::Quaternion q = tf::createQuaternionFromRPY(0,0,marker_locator_pos.theta + MARKER_THETA_OFFSET);
    marker_locator_pose.pose.orientation.x = q.getX();
    marker_locator_pose.pose.orientation.y = q.getY();
    marker_locator_pose.pose.orientation.z = q.getZ();
    marker_locator_pose.pose.orientation.w = q.getW();

    try
    {
        listener_.transformPose("map", marker_locator_pose, map_locator_pose);
        return TRANSFORM_SUCCESSFUL;

    } catch (tf::TransformException& e)
    {
        ROS_ERROR("Error in trasnform: %s", e.what());
        return TRANSFORM_UNSUCCESSFUL;
    }

}

/**
 *Â @brief Get the yaw angle from a Quaternion
 *
 * @param The current pose of the robot
 *
 * @return A waypoint containing the pose
**/
template <class T>
double WptNav<T>::getYawFromQuaternion(geometry_msgs::PoseWithCovarianceStamped pose)
{
    double roll, pitch, yaw;
    tf::Quaternion q(pose.pose.pose.orientation.x, pose.pose.pose.orientation.y, pose.pose.pose.orientation.z, pose.pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    m.getRPY(roll, pitch, yaw);
    return yaw;
}

/**
 *Â @brief Get the yaw angle from a Quaternion
 *
 * @param The current pose of the robot
 *
 * @return A waypoint containing the pose
**/
template <class T>
double WptNav<T>::getYawFromQuaternion(geometry_msgs::PoseStamped pose)
{
    double roll, pitch, yaw;
    tf::Quaternion q(pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    m.getRPY(roll, pitch, yaw);
    return yaw;
}

/**
 *@brief Get the marker locator data
**/
template <class T>
marker_locator::marker_locator_msg WptNav<T>::getMarkerData()
{
    marker_locator::marker_locator_msg msg;
    if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Can't connect to the marker locator!");
    } else
    {
        msg = marker_locator_obj_.getMarkerLocatorData(false);
    }
    return msg;
}

/**
 * @brief Stop the movement of the vehicle
 *
**/
template <class T>
void WptNav<T>::stopMovement()
{
    geometry_msgs::TwistStamped twist;
    twist.header.stamp = ros::Time::now();

    twist.twist.linear.x = 0;
    twist.twist.linear.y = 0;
    twist.twist.angular.z = 0;

    pub_stop_.publish(twist);
}

// Possible class templates
template class WptNav<double>;
template class WptNav<int>;
template class WptNav<float>;


//********************************************* LINE_EXTRACTION **********************************************/
template <class T>
LineFeatures<T>::LineFeatures()
{

}

template <class T>
LineFeatures<T>::~LineFeatures()
{

}

template <class T>
Point<T> LineFeatures<T>::closestPointLineSegment(Line<T> line, Point<T> p)
{
    Eigen::Vector2d A (line.start.x, line.start.y);
    Eigen::Vector2d B (line.end.x, line.end.y);
    Eigen::Vector2d r = (B - A) / (B -A).norm();
    Eigen::Vector2d P (p.x, p.y);

    Eigen::Vector2d closest_point;

    double d = P.dot(r) - A.dot(r);

    // Check where on the line segment the point is at
    if(d <= 0)
    {
        // Point is closest to A
        std::cout << "Closest point is A." << std::endl;
        closest_point = A;

    } else if(d > (B-A).norm())
    {
        // Point is closest to B
        std::cout << "Closest point is B." << std::endl;
        closest_point = B;
    } else
    {
        // Closest point is between A and AB
        closest_point = A + d*r;
    }

    //std::cout << "Point: " << closest_point[0] << " " << closest_point[1] << std::endl;

    return (Point<T>(static_cast<T>(closest_point[0]), static_cast<T>(closest_point[1])));
}

template <class T>
double LineFeatures<T>::distBetweenPoints(Point<T> a, Point<T> b)
{
    Eigen::Vector2d A (static_cast<double>(a.x), static_cast<double>(a.y));
    Eigen::Vector2d B (static_cast<double>(b.x), static_cast<double>(b.y));

    return ((B-A).norm());
}

template <class T>
double LineFeatures<T>::angleBetweenVectors(Vector2d v1, Vector2d v2)
{
    std::cout << "Vec 1: " << v1 << std::endl;
    std::cout << "Vec 2: " << v2 << std::endl;
    return (std::acos((v1.dot(v2)/(v1.norm()*v2.norm()))));
}


/**
 *@brief Calculates the length and orientation vector given two points in a cartesian coordinate system
 *
 *@param desired_dest_wpt The desired waypoint which the robot is to move to
 *
 *@return A length and orientation of the robot to move
**/
 /*
WptDirection WptNav::calcLengthAndOrientation(geometry_msgs::Point desired_dest_wpt)//marker_locator::marker_locator_msg start, geometry_msgs::Point end)
{
    /** DEBUG **//*
    geometry_msgs::Point end = desired_dest_wpt;
*/
    // Retrieve start point from the marker locator
   /* if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Unable to connect to Marker Locator. Not retunring any new waypoint");
        return WptDirection(0,0);
    }

    marker_locator_data_ = marker_locator_obj_.getMarkerLocatorData(false);*/
  /*  marker_locator_data_.x = 0,
    marker_locator_data_.y = 0;
    marker_locator_data_.theta = 0.7;

    std::cout << "From: (" << marker_locator_data_.x << " , " << marker_locator_data_.y << ")" << std::endl;
    std::cout << "To: (" << end.x << " , " << end.y << ")" << std::endl;

    WptDirection wpt;
    // Calculate the unit vector of the start and end position
    Eigen::Vector2d v1_norm(std::cos(marker_locator_data_.theta), std::sin(marker_locator_data_.theta));
    Eigen::Vector2d v2(end.x - marker_locator_data_.x, end.y - marker_locator_data_.y);
    Eigen::Vector2d v2_norm = v2/v2.norm();

    // Calculate the length and angle
    wpt.length = v2.norm();
    wpt.theta = std::atan2(v2_norm[1], v2_norm[0]) - std::atan2(v1_norm[1], v1_norm[0]);

    /** DEBUG **/
 /*   std::cout << "Wpt length: \t" << wpt.length << std::endl;
    std::cout << "Wpt theta: \t" << wpt.theta << std::endl;

    return wpt;
}*/

