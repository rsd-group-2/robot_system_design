#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/lib:/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/lib/x86_64-linux-gnu:/home/smyl/catkin_ws/devel/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/home/smyl/catkin_ws/devel/lib:/opt/ros/indigo/lib"
export PATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/lib/pkgconfig:/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/lib/x86_64-linux-gnu/pkgconfig:/home/smyl/catkin_ws/devel/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/home/smyl/catkin_ws/devel/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PYTHONPATH="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/smyl/catkin_ws/src/tester_obstacle_detect/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/smyl/catkin_ws/src/tester_obstacle_detect:$ROS_PACKAGE_PATH"