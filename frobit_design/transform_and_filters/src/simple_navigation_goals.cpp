#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <iostream>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv) 
{
	ros::init(argc, argv, "simple_navigation_goals");

	// Tell the action client that we want to spin a thread by default
	MoveBaseClient ac("move_base", true);

	// Wait for the action server to come up
	while(!ac.waitForServer(ros::Duration(5.0)))
	{
		ROS_INFO("Waiting for the move_base action servber to come up");
	}

	move_base_msgs::MoveBaseGoal goal;

	// Initial coordinates
	tf::TransformListener listener;
	// Get the coordinates of the robot
	tf::StampedTransform transform_in_map;
	try 
	{
		listener.waitForTransform("odom_combined", "base_link", ros::Time(0), ros::Duration(5));
		listener.lookupTransform("map", "base_link", ros::Time(0), transform_in_map);
		std::cout << "x and y is: " << transform_in_map.getOrigin().x() << " " << transform_in_map.getOrigin().y() << std::endl;
		//ROS_INFO("x and y is: %d , %i", transform_in_map.getOrigin().x(), transform_in_map.getOrigin().y());

	} catch (tf::TransformException &exception)
	{
		ROS_ERROR("%s", exception.what());
	}


	// We'll send a goal to the robot to move 1 meter forward
	while(true)
	{	
		float x, y;
		std::cout << "Enter x coordinate: -- 10 to terminate: ";
		std::cin >> x;
		if(x == 10)
			return 0;
		std::cout << "Enter y coordinate: ";
		std::cin >> y;
		float yaw;
		std::cout << "Enter yaw: ";
		std::cin >> yaw;

		goal.target_pose.header.frame_id = "map";
		goal.target_pose.header.stamp = ros::Time::now();

		goal.target_pose.pose.position.x = x;
		goal.target_pose.pose.position.y = y;
		tf::Quaternion q = tf::createQuaternionFromRPY(0,0,yaw);
		goal.target_pose.pose.orientation.z = q.getZ();
		goal.target_pose.pose.orientation.w = q.getW();


		ROS_INFO("Sending goal");
		ac.sendGoal(goal);

		ac.waitForResult();

		if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		{
			ROS_INFO("Hooray, the base move 1 meter forward");
			// Get the coordinates of the robot
			try 
			{
				listener.waitForTransform("map", "base_link", ros::Time(0), ros::Duration(5));
				listener.lookupTransform("map", "base_link", ros::Time(0), transform_in_map);
				std::cout << "x and y is: " << transform_in_map.getOrigin().x() << " " << transform_in_map.getOrigin().y() << std::endl;
				//ROS_INFO("x and y is: %d , %i", transform_in_map.getOrigin().x(), transform_in_map.getOrigin().y());

			} catch (tf::TransformException &exception)
			{
				ROS_ERROR("%s", exception.what());
			}
		}
		else
			ROS_INFO("The base failed to move forward 1 meter for some reason");
	}
	return 0;
}
