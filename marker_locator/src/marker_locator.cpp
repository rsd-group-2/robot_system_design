#include "marker_locator.hpp"

const char SERVER[] = "10.115.253.233";

/**
 * @brief Default constructor
**/
MarkerLocator::MarkerLocator()
{
    portno = SocketConnection::SERVERPORT;
    sent_message = "Get position 6";
    bzero(buffer, SocketConnection::BUFFER_SIZE);
}

/**
 * @brief Destructor
**/
MarkerLocator::~MarkerLocator()
{
    if(close(sockfd))
    {
        //ROS_ERROR("Error closing socket");
        std::cout << "ERROR closing socket" << std::endl;
    }

        std::cout << "Socket closed" << std::endl;
}

/**
 * @brief Prints the marker locator data
 *
 * @param data Marker Locator struct containing the data
**/
void MarkerLocator::print(marker_locator::marker_locator_msg data)
{
    std::cout << "----------- MARKER LOCATOR DATA ------------" << std::endl;
    std::cout << "Id: \t\t" << data.id << "\n";
    std::cout << "Time Stamp: \t" << data.time_stamp << std::fixed << "\n";
    std::cout << "x pos: \t\t" << data.x << std::setprecision(3) << "\n";
    std::cout << "y pos: \t\t" << data.y << std::setprecision(3) << "\n";
    std::cout << "Theta \t\t" << data.theta << std::setprecision(3) << "\n";
    std::cout << "--------------------------------------------" << std::endl;
}

/**
 * @brief  Creates a socket connection to the Marker Locator.
           The IP and PORT are constants defined in the .hpp file
**/
int MarkerLocator::Connect()
{
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
    {
        ROS_ERROR("Marker Locator: Error opening socket");
        return SocketConnection::CONNECT_SOCKET_FAILED;
    }

    server = gethostbyname(SERVER);
    if (server == NULL)
    {
        ROS_ERROR("Marker Locator: Host not found");
        return SocketConnection::NO_HOST;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    // Conncet to the server
    if(connect(sockfd, (struct sockaddr *)  &serv_addr, sizeof(serv_addr)) < 0)
    {
        ROS_ERROR("Marker Locator: Error connecting to server socket");
        return SocketConnection::CONNECTION_FAILED;
    }

    return SocketConnection::CONNECTION_SUCCEDED;
}

/**
 * @brief  Retrieves the marker locator data from the serv
 *
 * @return A string containing the data.
**/
std::string MarkerLocator::getMessage()
{
    // Clear buffer and read new data
    bzero(buffer, SocketConnection::BUFFER_SIZE);
    n = write(sockfd, sent_message.c_str(), strlen(sent_message.c_str()));
    if(n < 0)
    {
        ROS_ERROR("Marker Locator: Error writing to server socket");
    }
    n = read(sockfd, buffer, SocketConnection::BUFFER_SIZE - 1);
    if (n < 0)
    {
        ROS_ERROR("Marker Locator: Unable to read data from server socket");
    }

    return std::string(buffer);
}

/**
 * @brief  Extracts the data from the string received from the
 *         Marker Locator server.
 *
 * @return The Marker Locator data object with the data
**/
marker_locator::marker_locator_msg MarkerLocator::getMarkerLocatorData(const int print_data)
{

    marker_locator::marker_locator_msg marker_locator_data;
    std::string unsorted_data_string = MarkerLocator::getMessage();
    std::string::size_type sz;
    char delimeter(',');

    // Split the string into the marker loactor msg and into the correct data types
   // std::cout << unsorted_data_string << std::endl;
    int prev_index = 0;
    int index = unsorted_data_string.find(delimeter, 0);
    marker_locator_data.id = std::stoi(unsorted_data_string.substr(0, index), &sz);
    prev_index = index + 1;
    index = unsorted_data_string.find(delimeter, index + 1);
   // std::cout << prev_index << " " << index << std::endl;
    marker_locator_data.time_stamp = std::stod(unsorted_data_string.substr(prev_index, index), &sz);
    prev_index = index + 1;
    index = unsorted_data_string.find(delimeter, index + 1);
    marker_locator_data.x = std::stod(unsorted_data_string.substr(prev_index, index), &sz);
    prev_index = index + 1;
    index = unsorted_data_string.find(delimeter, index + 1);
    marker_locator_data.y = std::stod(unsorted_data_string.substr(prev_index, index), &sz);
    prev_index = index + 1;
    marker_locator_data.theta = std::stod(unsorted_data_string.substr(prev_index, unsorted_data_string.length()), &sz);

    /**
     * DEBUG
    **/
    if(print_data)
        MarkerLocator::print(marker_locator_data);

    return marker_locator_data;
}
