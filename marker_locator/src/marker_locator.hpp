#ifndef _MARKER_LOCATOR_HPP
#define _MARKER_LOCATOR_HPP

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <iostream>

#include "ros/ros.h"
#include "marker_locator/marker_locator_msg.h"
#include "geometry_msgs/Point.h"


namespace SocketConnection {
    enum SocketMsg{
        CONNECT_SOCKET_FAILED,
        CONNECTION_FAILED,
        CONNECTION_SUCCEDED,
        NO_HOST
    };

    enum ServerParams {
        SERVERPORT = 21212,
        BUFFER_SIZE = 50,
        MARKER_LOCATOR_ID = 6
    };
};

class MarkerLocator
{
public:
    // Constructor
    MarkerLocator();

    // Destructor
    ~MarkerLocator();

    // Connect to the marker locator
    int Connect();

    // Retrieve a message from the marker locater
    std::string getMessage();

    // Retrieves the data frmo the server
    marker_locator::marker_locator_msg getMarkerLocatorData(const int print_data);

    // Send a message to the marker locator
    int sendMessage(std::string message);

private:

    // Socket variables
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    std::string sent_message;
    char buffer[SocketConnection::BUFFER_SIZE];

    // Waypoint variables
    int wpt_cnt_;

    // Waypoint vectors
    std::vector<geometry_msgs::Point> robot_cell_to_dispenser_wpts_;
    std::vector<geometry_msgs::Point> dispenser_to_robot_cell_wpts_;

    // Print the markerlocator data
    void print(marker_locator::marker_locator_msg data);
};


#endif /*_MARKER_LOCATOR_HPP*/
