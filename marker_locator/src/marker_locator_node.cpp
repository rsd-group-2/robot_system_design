#include "marker_locator.hpp"
#include "marker_locator/MarkerLocatorData.h"
#include "waypoint_navigator.hpp"

typedef WayPointNav::WayPointRequest ServiceMessage;
static WptNav wptNav;

/**
 *@brief
**/

bool requestMarkerLocation(marker_locator::MarkerLocatorData::Request &req,
                           marker_locator::MarkerLocatorData::Response &res)
{
    WayPointNav::WayPoint wpt;
    marker_locator::marker_locator_msg marker_msg;

    // Find what request was send
    switch(req.request)
    {
        case ServiceMessage::GET_CURRENT_WPT:
            ROS_INFO("Get current wpt requested");
            wpt = wptNav.getCurrentWpt();
        break;

        case ServiceMessage::GET_NEXT_WPT:
            ROS_INFO("Get next wpt requested");
            wpt = wptNav.getNextWpt();
        break;

        case ServiceMessage::GET_PREV_WPT:
            ROS_INFO("Get previous wpt requested");
            wpt = wptNav.getPreviousWpt();
        break;

        case ServiceMessage::RESET_WPT_CNT:
            ROS_INFO("Reset wpt count requested");
            wptNav.resetWpts();
        break;

        case ServiceMessage::SET_ARRIVED_STATE:
            ROS_INFO("Set arrived state requested");
            if (req.state == WayPointNav::WayPointState::CELL_TO_SLAM)
            {
                wptNav.setWptState(WayPointNav::WayPointState::SLAM_TO_CELL);
                wptNav.resetWpts();
            }
            else if (req.state == WayPointNav::WayPointState::SLAM_TO_CELL)
            {
                wptNav.setWptState(WayPointNav::WayPointState::CELL_TO_SLAM);
                wptNav.resetWpts();
            }
            else
            {
                ROS_ERROR("Undefined State!");
                res.error = 1;
            }
        break;

        default:
            // Go masturbate or something
        break;
    }

    // Set the response variables and return. In case no wptObj was retrieved,
    // everything is set to null.
    res.wpt_x = wpt.x;
    res.wpt_y = wpt.y;

    // Get the latest marker locator data
    marker_msg = wptNav.getLatestMarkerData();

    ROS_INFO("Marker x: %f", marker_msg.x);
    ROS_INFO("Marker y: %f", marker_msg.y);

    res.marker_x = marker_msg.x;
    res.marker_y = marker_msg.y;
    res.marker_theta = marker_msg.theta;

    res.wpt_state = wptNav.getWptState();
    res.wpt_count = wptNav.getWptsCnt();

    ROS_INFO("Request: request=%i, state=%i", (int)req.request, (int)req.state);

    return true;
}

int main(int argc, char** argv)
{
    int print_data = 0;
    std::string input = "";
    //std::cout << "Please enter if you want to get the data printed or not:\nEnable print: 1\nDisable print: 0" << std::endl;
    //std::cin >> print_data;

    // Initialization of ROS components
    ros::init(argc, argv, "marker_locator_node");
    ros::Time::init();
    ros::NodeHandle n;

    // Ros service to communicate with Frobit Python code
    ros::ServiceServer service = n.advertiseService("waypoint_update_service", requestMarkerLocation);

    // Initiate the publisher node
    ros::Publisher pub = n.advertise<marker_locator::marker_locator_msg>("marker_locator_topic", 10);
    ros::Rate loop_rate(1);

    // Instantiate marker locator class
    MarkerLocator marker_locator_obj;

    ROS_INFO("Program Started");
    while(ros::ok())
    {
        pub.publish(wptNav.getNewestMarkerData());
       // pub.publish(wptNav.getLatestMarkerData());

        ros::spinOnce();
        loop_rate.sleep();
    }
    std::cout << "Program Terminated" << std::endl;
}
