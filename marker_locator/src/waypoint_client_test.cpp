#include "ros/ros.h"
#include "marker_locator/MarkerLocatorData.h"
#include "waypoint_navigator.hpp"
#include <cstdlib>

int main (int argc, char** argv)
{
    ros::init(argc, argv, "waypoint_client_test");

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<marker_locator::MarkerLocatorData>("waypoint_update_service");

    while(true)
    {
        marker_locator::MarkerLocatorData srv;
        std::cout << "Enter request" << std::endl;
        std::cout << "0: get next wpt, 1: get prev wpt, 2: get current wpt, 3: reset wpt cnt, 4: set arrived state" << std::endl;
        int request;
        std::cin >> request;
        if(request == 10)
            return 0;
        srv.request.request = request;
        int state;
        std::cout << "Set state\n" << "0: CELL_TO_SLAM, 1: SLAM_TO_CELL" << std::endl;
        std::cin >> state;
        srv.request.state = state;

        if(client.call(srv))
        {
            ROS_INFO("Client called");

            if(srv.response.error > 0)
            {
                ROS_ERROR("Some error occured");
            }
            else
            {
                ROS_INFO("Response from srv");
                ROS_INFO("wtp x: %i", (int) srv.response.wpt_x);
                ROS_INFO("wtp y: %i", (int) srv.response.wpt_y);
                ROS_INFO("wpt cnt: %i", (int) srv.response.wpt_count);
                ROS_INFO("wpt state: %i", (int) srv.response.wpt_state);
                ROS_INFO("marker x: %f", (double) srv.response.marker_x);
                ROS_INFO("marker y: %f,", (double) srv.response.marker_y);
                ROS_INFO("marker theta: %f", (double) srv.response.marker_theta);
            }
        }
        else
        {
            ROS_ERROR("Failed to call service");
        }
    }
}
