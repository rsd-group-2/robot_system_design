#include "waypoint_navigator.hpp"

/**
 *@brief Constructor. Initializes all the variables
**/
WptNav::WptNav() : wpt_cnt_(0), wpt_state_(WayPointState::CELL_TO_SLAM)
{
    // Initialize the waypoint vectors
    initWpts();
}

/**
 *@brief Destructor
**/
WptNav::~WptNav()
{

}

/**
 *@brief Get the next waypoint in the list
 *
 *@return The next wpt
**/
WayPoint WptNav::getNextWpt()
{
    // Update the marker locator data
    if(!updateMarkerLocatorData())
    {
        ROS_ERROR("Cant Connect to Marker Locator");
        return WayPoint(0,0,0);
    }

    WptDirection wpt;

    wpt_cnt_++;
    // Check if the cnt exceeded maximum count
    /** DEBUG **/
    if (wpt_cnt_ >= cell_to_slam_wpts_.size() && wpt_state_ == CELL_TO_SLAM)
    {
        wpt_state_ = SLAM_TO_CELL;
        std::cout << "CHANGED TO SLAM TO CELL" << std::endl;
        resetWpts();
    } else if (wpt_cnt_ >= slam_to_cell_wpts_.size() && wpt_state_ == SLAM_TO_CELL)
    {
        wpt_state_ = CELL_TO_SLAM;
        std::cout << "CHANGED TO CELL TO SLAM" << std::endl;
        resetWpts();
    }
  /*  if (wpt_cnt_ >= cell_to_slam_wpts_.size() || wpt_cnt_ >= slam_to_cell_wpts_.size())
    {
        ROS_ERROR("Waypoint Counter exceeded max value.");
        return WptDirection(0,0);
    }*/

    if (wpt_state_ == WayPointState::CELL_TO_SLAM)
    {
        return cell_to_slam_wpts_[wpt_cnt_];
    }
    else if (wpt_state_ == WayPointState::SLAM_TO_CELL)
    {
        return slam_to_cell_wpts_[wpt_cnt_];
    }
    else
    {
        ROS_ERROR("Waypoint in an undefined state.");
        return WayPoint(0,0,0);
    }
}

/**
 *@brief Get the prevous waypoint in the list
 *
 *@return The previous wpt
**/
WayPoint WptNav::getPreviousWpt()
{
    if (wpt_cnt_ <= 0)
    {
        ROS_INFO("At the start position. Returning null value");
        return WayPoint(0,0,0);
    }
    if (wpt_state_ == WayPointState::CELL_TO_SLAM)
    {
        return cell_to_slam_wpts_[wpt_cnt_ - 1];
    }
    else if (wpt_state_ == WayPointState::SLAM_TO_CELL)
    {
        return slam_to_cell_wpts_[wpt_cnt_ - 1];
    }
    else
    {
        ROS_ERROR("Waypoint in an undefined states.");
        return WayPoint(0,0,0);
    }
}

/**
 *@brief Get the current Wpt
 *
 *@return The current Wpt
**/
WayPoint WptNav::getCurrentWpt()
{
    if (wpt_state_ == WayPointState::CELL_TO_SLAM)
    {
        return cell_to_slam_wpts_[wpt_cnt_];
    }
    else if (wpt_state_ == WayPointState::SLAM_TO_CELL)
    {
        return slam_to_cell_wpts_[wpt_cnt_];
    }
    else
    {
        ROS_ERROR("Waypoint in an undefined states.");
        return WayPoint(0,0,0);
    }
}

/**
 *@brief Reset the waypoint counter
**/
void WptNav::resetWpts()
{
    wpt_cnt_ = 0;
}

/**
 *@brief Returns the current wpt counter state
**/
int WptNav::getWptsCnt()
{
    return wpt_cnt_;
}

/**
 *@brief Set the current waypoint state
 *
 *@param new_state New state of the waypoint
**/
void WptNav::setWptState(int new_state)
{
    wpt_state_ = new_state;
}

/**
 *@brief Get the latest data loaded from the marker locator
 *
 *@return Marker locater message
**/
marker_locator::marker_locator_msg WptNav::getLatestMarkerData()
{
    return marker_locator_data_;
}

/**
 *@brief Get the current data loaded from the marker locator
 *
 *@return Marker locator message
**/
marker_locator::marker_locator_msg WptNav::getNewestMarkerData()
{
    marker_locator::marker_locator_msg marker_locator_data;
   if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Unable to connect to Marker Locator. Returning latest marker point");
        return getLatestMarkerData();
    }
    return(marker_locator_obj_.getMarkerLocatorData(false));
}

/**
 * @brief Initialize the waypoints. Hardcode programmed
**/
void WptNav::initWpts()
{
    geometry_msgs::Point p;
    cell_to_slam_wpts_.push_back(setWptValues(5.095,-0.799,0));
    cell_to_slam_wpts_.push_back(setWptValues(2.578,-1.092,0));
    cell_to_slam_wpts_.push_back(setWptValues(1.779,0.067,0));
    cell_to_slam_wpts_.push_back(setWptValues(5.885,-1.219,0));

    slam_to_cell_wpts_.push_back(setWptValues(2,1,0));
    slam_to_cell_wpts_.push_back(setWptValues(4,3,0));
    slam_to_cell_wpts_.push_back(setWptValues(7,2,0));
}

/**
 *@brief Set the value fo a point.
 *
 *@param x coordinate
 *@param y coordinate
 *@param z coordinate
 *
 @return Redefined coordinate
**/
WayPoint WptNav::setWptValues(double x, double y, double theta)
{
    WayPoint wpt;
    wpt.x = x;
    wpt.y = y;
    wpt.theta = theta;
    return wpt;
}

/**
 *@brief Calculates the length and orientation vector given two points in a cartesian coordinate system
 *
 *@param desired_dest_wpt The desired waypoint which the robot is to move to
 *
 *@return A length and orientation of the robot to move
**/
WptDirection WptNav::calcLengthAndOrientation(geometry_msgs::Point desired_dest_wpt)//marker_locator::marker_locator_msg start, geometry_msgs::Point end)
{
    /** DEBUG **/
    geometry_msgs::Point end = desired_dest_wpt;

    // Retrieve start point from the marker locator
   /* if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Unable to connect to Marker Locator. Not retunring any new waypoint");
        return WptDirection(0,0);
    }

    marker_locator_data_ = marker_locator_obj_.getMarkerLocatorData(false);*/
    marker_locator_data_.x = 0,
    marker_locator_data_.y = 0;
    marker_locator_data_.theta = 0.7;

    std::cout << "From: (" << marker_locator_data_.x << " , " << marker_locator_data_.y << ")" << std::endl;
    std::cout << "To: (" << end.x << " , " << end.y << ")" << std::endl;

    WptDirection wpt;
    // Calculate the unit vector of the start and end position
    Eigen::Vector2d v1_norm(std::cos(marker_locator_data_.theta), std::sin(marker_locator_data_.theta));
    Eigen::Vector2d v2(end.x - marker_locator_data_.x, end.y - marker_locator_data_.y);
    Eigen::Vector2d v2_norm = v2/v2.norm();

    // Calculate the length and angle
    wpt.length = v2.norm();
    wpt.theta = std::atan2(v2_norm[1], v2_norm[0]) - std::atan2(v1_norm[1], v1_norm[0]);

    /** DEBUG **/
    std::cout << "Wpt length: \t" << wpt.length << std::endl;
    std::cout << "Wpt theta: \t" << wpt.theta << std::endl;

    return wpt;
}

/**
 *@brief Update the current marker locator data
 *
 *@return True if updated successfully
**/
bool WptNav::updateMarkerLocatorData()
{
    ROS_INFO("Connecting to Marker Locator");
    if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Unable to connect to Marker Locator. Not retunring any new waypoint");
        return false;
    }
    marker_locator_data_ = marker_locator_obj_.getMarkerLocatorData(false);

   /* marker_locator_data_.x = 0;
    marker_locator_data_.y = 0;
    marker_locator_data_.theta = 0;*/
    ROS_INFO("Marker Locater data updated");
    return true;
}
