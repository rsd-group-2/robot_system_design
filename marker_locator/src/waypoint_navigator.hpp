#ifndef _WAYPOINT_NAVIGATER_HPP
#define _WAYPOONT_NAVIGATOR_HPP

#include <string>
#include <iostream>

#include "marker_locator.hpp"
#include "geometry_msgs/Point.h"
#include "ros/ros.h"

#include <Eigen/Dense>

namespace WayPointNav
{
    struct WayPoint
    {
        double x;
        double y;
        double theta;

        WayPoint() : x(0), y(0), theta(0) {}

        WayPoint(double _x, double _y, double _theta) : x(_x), y(_y), theta(_theta) {}
    };

    struct WptDirection
    {
        double theta;
        double length;

        WptDirection()
        {
            theta = 0;
            length = 0;
        }

        WptDirection(double _theta, double _length)
        {
            theta = _theta;
            length = _length;
        }

        bool isNull()
        {
            if (length == 0 && theta == 0) return true;
            else false;
        }
    };

    enum WayPointRequest {
        GET_NEXT_WPT,
        GET_PREV_WPT,
        GET_CURRENT_WPT,
        RESET_WPT_CNT,
        SET_ARRIVED_STATE
    };

    enum WayPointState {
        CELL_TO_SLAM,
        SLAM_TO_CELL
    };

};

using namespace WayPointNav;

class WptNav
{
public:
    // Constructor
    WptNav();

    // Destructor
    ~WptNav();

    // Get the next Waypoint in the list
    WayPoint getNextWpt();

    // Get previous Waypoint
    WayPoint getPreviousWpt();

    // Get Current Waypoint
    WayPoint getCurrentWpt();

    // Reset Waypoint counter
    void resetWpts();

    // Get the current wpt count
    int getWptsCnt();

    // Get the number of wpts from cell to slam
    int getCellToSlamWpts() {return cell_to_slam_wpts_.size();};

    // Get the number of wpts frmo slam to cell
    int getSlamToCellWpts() {return slam_to_cell_wpts_.size();};

    // Set the current waypoint state
    void setWptState(int new_state);

    // Get the current waypoint state
    int getWptState() {return wpt_state_;};

    // Get the latest marker locator data
    marker_locator::marker_locator_msg getLatestMarkerData();

    // Get the newest marker locator data
    marker_locator::marker_locator_msg getNewestMarkerData();

private:
    MarkerLocator marker_locator_obj_;

    // Waypoints variables
    int wpt_cnt_;
    int wpt_state_;
    std::vector<WayPoint> cell_to_slam_wpts_;
    std::vector<WayPoint> slam_to_cell_wpts_;
    marker_locator::marker_locator_msg marker_locator_data_;

    // Initialie the waypoints
    void initWpts();

    // Set the values for a point
    WayPoint setWptValues(double x, double y, double theta);

    // Calculates the angle and length from two waypoitns
    WptDirection calcLengthAndOrientation(geometry_msgs::Point desired_dest_wpt);

    // Updates the current marker locator
    bool updateMarkerLocatorData();
};
#endif /* _WAYPOINT_NAVIGATOR_HPP */
