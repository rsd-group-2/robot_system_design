#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/chris/myandroid/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/chris/myandroid/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/chris/myandroid/devel/lib:/home/chris/myandroid/devel/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"
export PATH="/home/chris/myandroid/devel/bin:/home/chris/android_core/devel/bin:/home/chris/rosjava/devel/bin:/home/chris/catkin_ws/devel/bin:/opt/ros/indigo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:~/catkin_ws/src:/opt/android-sdk/tools:/opt/android-sdk/platform-tools:/opt/android-studio/bin"
export PKG_CONFIG_PATH="/home/chris/myandroid/devel/lib/pkgconfig:/home/chris/myandroid/devel/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/chris/myandroid/build"
export PYTHONPATH="/home/chris/myandroid/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/chris/myandroid/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_MAVEN_DEPLOYMENT_REPOSITORY="/home/chris/myandroid/devel/share/maven"
export ROS_MAVEN_PATH="/home/chris/myandroid/devel/share/maven:$ROS_MAVEN_PATH"
export ROS_PACKAGE_PATH="/home/chris/myandroid/src:$ROS_PACKAGE_PATH"