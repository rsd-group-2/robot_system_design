#include "marker_locator.hpp"
#include "marker_locator/MarkerLocatorData.h"

/**
 *@brief
**/

bool requestMarkerLocation(marker_locator::MarkerLocatorData::Request &req,
                           marker_locator::MarkerLocatorData::Response &res)
{
    marker_locator::marker_locator_msg msg;
    MarkerLocator marker_locator_obj;
    switch(req.request)
    {
        case MarkerRequest::GET_LATEST_DATA:
            if(marker_locator_obj.Connect() != SocketConnection::CONNECTION_SUCCEDED)
            {
                ROS_ERROR("Unable to connect to Marker Locator. Returning latest marker point");
            } else
            {
		ROS_INFO("Successfully connected to the socket");
                msg = marker_locator_obj.getMarkerLocatorData(false);
            }

        break;
    }

    res.marker_x = msg.x;
    res.marker_y = msg.y;
    res.marker_theta = msg.theta;
  

    ROS_INFO("Marker Locator Data acquired and sent");

    // Call the destructor and close the socket
    marker_locator_obj.Disconnect();
    /*WayPointNav::WayPoint wpt;
    marker_locator::marker_locator_msg marker_msg;

    // Find what request was send
    switch(req.request)
    {
        case ServiceMessage::GET_CURRENT_WPT:
            ROS_INFO("Get current wpt requested");
            wpt = wptNav.getCurrentWpt();
        break;

        case ServiceMessage::GET_NEXT_WPT:
            ROS_INFO("Get next wpt requested");
            wpt = wptNav.getNextWpt();
        break;

        case ServiceMessage::GET_PREV_WPT:
            ROS_INFO("Get previous wpt requested");
            wpt = wptNav.getPreviousWpt();
        break;

        case ServiceMessage::RESET_WPT_CNT:
            ROS_INFO("Reset wpt count requested");
            wptNav.resetWpts();
        break;

        case ServiceMessage::SET_ARRIVED_STATE:
            ROS_INFO("Set arrived state requested");
            if (req.state == WayPointNav::WayPointState::CELL_TO_SLAM)
            {
                wptNav.setWptState(WayPointNav::WayPointState::SLAM_TO_CELL);
                wptNav.resetWpts();
            }
            else if (req.state == WayPointNav::WayPointState::SLAM_TO_CELL)
            {
                wptNav.setWptState(WayPointNav::WayPointState::CELL_TO_SLAM);
                wptNav.resetWpts();
            }
            else
            {
                ROS_ERROR("Undefined State!");
                res.error = 1;
            }
        break;

        default:
            // Go masturbate or something
        break;
    }

    // Set the response variables and return. In case no wptObj was retrieved,
    // everything is set to null.
    res.wpt_x = wpt.x;
    res.wpt_y = wpt.y;

    // Get the latest marker locator data
    marker_msg = wptNav.getLatestMarkerData();

    ROS_INFO("Marker x: %f", marker_msg.x);
    ROS_INFO("Marker y: %f", marker_msg.y);

    res.marker_x = marker_msg.x;
    res.marker_y = marker_msg.y;
    res.marker_theta = marker_msg.theta;

    res.wpt_state = wptNav.getWptState();
    res.wpt_count = wptNav.getWptsCnt();

    ROS_INFO("Request: request=%i, state=%i", (int)req.request, (int)req.state);*/



    return true;
}

int main(int argc, char** argv)
{
    // Marker Locator objects
    MarkerLocator marker_locator_obj;
    marker_locator::marker_locator_msg msg;


    // Initialization of ROS components
    ros::init(argc, argv, "marker_locator_node");
    ros::Time::init();
    ros::NodeHandle n;

    // Ros service to communicate with Frobit Python code
    ros::ServiceServer service = n.advertiseService("waypoint_update_service", requestMarkerLocation);

    // Initiate the publisher node
    //ros::Publisher pub = n.advertise<marker_locator::marker_locator_msg>("marker_locator_topic", 10);
    ros::Rate loop_rate(1);



    ROS_INFO("Program Started");
    while(ros::ok())
    {
        /*if(marker_locator_obj.Connect() != SocketConnection::CONNECTION_SUCCEDED)
        {
            ROS_ERROR("Unable to connect to Marker Locator. Returning latest marker point");
        } else
        {
            pub.publish(marker_locator_obj.getMarkerLocatorData(false));
        }*/
       // pub.publish(wptNav.getLatestMarkerData());

        ros::spinOnce();
        loop_rate.sleep();
    }
    std::cout << "Program Terminated" << std::endl;
}
