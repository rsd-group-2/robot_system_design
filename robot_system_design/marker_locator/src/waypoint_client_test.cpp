#include "ros/ros.h"
#include "marker_locator/MarkerLocatorData.h"
#include "waypoint_navigator.hpp"
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <cstdlib>

int main (int argc, char** argv)
{
    ros::init(argc, argv, "waypoint_client_test");

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<marker_locator::MarkerLocatorData>("waypoint_update_service");

    // WAypoint test
    WptNav wptNav;


    tf::TransformListener listener;
    tf::StampedTransform transform_in_map;
    while(ros::ok())
    {
	/** DEBUG START **/
	WayPointNav::WayPoint wpt;
        marker_locator::MarkerLocatorData srv;
        std::cout << "Enter request" << std::endl;
        std::cout << "0: Move to dispenser, 1: Move to start of line, 2: Get marker data, 3: to end of Box and to Line" << std::endl;
        int request;
        std::cin >> request;
        if(request == 10)
            return 0;
	else if(request == 0 || request == 3)
            wpt = WayPointNav::WayPoint(-1.255, 0.499, 0);
	else if(request == 1)
	    wpt = WayPointNav::WayPoint(2.353, 2.12828, -0.455);

	srv.request.request = 0;

        if(client.call(srv))
        {
            ROS_INFO("Client called");

            if(srv.response.error > 0)
            {
                ROS_ERROR("Some error occured");
            }
            else
            {
                ROS_INFO("Response from srv");
                ROS_INFO("marker x: %f", (double) srv.response.marker_x);
                ROS_INFO("marker y: %f,", (double) srv.response.marker_y);
                ROS_INFO("marker theta: %f", (double) srv.response.marker_theta);

                ROS_INFO("Setting initial pose!");
		wptNav.setInitialPose();
		if(request == 0)
		{
			wptNav.moveToWpt(WayPoint( -0.362949, 4.31907, 1.415));
			//wptNav.setInitialPose();
			wptNav.moveToWpt(WayPoint(0.902548, 3.60405, -0.604081));
			//wptNav.setInitialPose();
			wptNav.moveToWpt(WayPoint(1.84497, 2.66133, -0.644081));
			//wptNav.setInitialPose();
			wptNav.moveToWpt(WayPoint(2.5497, 1.91607, -0.857267)); 
		}
	/*	if(request != 2)
		{
		   if(request == 3) 
		   {
		      wptNav.moveToWpt(WayPoint(-0.897, 3.606, 1.495));
		   }
		   wptNav.moveToWpt(wpt);
		}*/

                try 
                {
                    listener.waitForTransform("map", "base_link", ros::Time(0), ros::Duration(5));
                    listener.lookupTransform("map", "base_link", ros::Time(0), transform_in_map);
                    std::cout << "Robot x and y coordinates in the map is: " << transform_in_map.getOrigin().x() << " " << transform_in_map.getOrigin().y() << std::endl;
                    //ROS_INFO("x and y is: %d , %i", transform_in_map.getOrigin().x(), transform_in_map.getOrigin().y());

                } catch (tf::TransformException &exception)
                {
                    ROS_ERROR("%s", exception.what());
                }

            }
        }
        else
        {
            ROS_ERROR("Failed to call service");
        }
    }
}
