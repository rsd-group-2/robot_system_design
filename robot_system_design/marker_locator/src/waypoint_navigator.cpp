#include "waypoint_navigator.hpp"

using namespace WayPointNav;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

/**
 *@brief Constructor. Initializes all the variables
**/
WptNav::WptNav() : wpt_state_(WPTStackResult::IDLE)
{
    _pub = _n.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1);
}
/**
 *@brief Destructor
**/
WptNav::~WptNav() {}

/**
 *@brief Move to the waypoint given by the input
 *
 *@param wpt The waypoint to move to
 *
 *@return Returns the result of the waypoint movement
**/
WPTStackResult WptNav::moveToWpt(WayPoint wpt)
{
    ROS_INFO("Moving base");
    MoveBaseClient move_base_client_("move_base", true);

    // Wait for the action server to come up
    while(!move_base_client_.waitForServer(ros::Duration(MOVE_BASE_WAIT_DURATION)))
    {
        ROS_INFO("Waiting for the move_base action server to come up");
    }
    // Setup up the goal pose
    goal_.target_pose.header.frame_id = "map";
    goal_.target_pose.header.stamp = ros::Time::now();

    tf::Quaternion q = tf::createQuaternionFromRPY(0,0,wpt.theta);

    goal_.target_pose.pose.position.x = wpt.x;
    goal_.target_pose.pose.position.y = wpt.y;
    goal_.target_pose.pose.position.z = 0;
    goal_.target_pose.pose.orientation.x = q.getX();
    goal_.target_pose.pose.orientation.y = q.getY();
    goal_.target_pose.pose.orientation.z = -q.getZ();
    goal_.target_pose.pose.orientation.w = -q.getW();
    std::cout << "Quaternions: " << q.getX() << " " << q.getY() << " " << -q.getZ() << " " << -q.getW() << std::endl;

    ROS_INFO("Moving towards the goal");
    move_base_client_.sendGoal(goal_);
    wpt_state_ = WPTStackResult::MOVING_TO_WAYPOINT;

    move_base_client_.waitForResult();

    if(move_base_client_.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
    {
        ROS_INFO("Waypoint successfully reached");
        wpt_state_ = WPTStackResult::WAYPOINT_SUCCESSFULLY_REACHED;
        return WPTStackResult::WAYPOINT_SUCCESSFULLY_REACHED;
    }
    else
    {
        ROS_INFO("The base failed to move to the desired waypoint");
        wpt_state_ = WPTStackResult::ERROR_REACHING_WAYPOINT;
        return WPTStackResult::ERROR_REACHING_WAYPOINT;
    }
}

/**
 *@brief Move to the waypoint given by the input
 *
 *@param x, y coordinates and theta angle
 *
 *@return Returns the result of the waypoint movement
**/
 WPTStackResult WptNav::moveToWpt(double x, double y, double theta)
 {
    WayPoint wpt;
    wpt.x = x;
    wpt.y = y;
    wpt.theta = theta;

    return(moveToWpt(wpt));
 }

// Set the initial pose
WPTStackResult WptNav::setInitialPose()
{
    geometry_msgs::PoseWithCovarianceStamped initial_pose;

    // Header and frame
    initial_pose.header.stamp = ros::Time::now();
    initial_pose.header.frame_id = "map";

    // Get the marker locator data
    marker_locator::marker_locator_msg marker_locator_msg;
    if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Can't connect to the marker locator!");
    } else
    {
        marker_locator_msg = marker_locator_obj_.getMarkerLocatorData(false);
    }

    geometry_msgs::PoseStamped marker_locator_pose;
    geometry_msgs::PoseStamped map_locator_pose;

    marker_locator_pose.header.stamp = ros::Time::now();
    marker_locator_pose.header.frame_id = "marker_link";
    marker_locator_pose.pose.position.x = marker_locator_msg.x;
    marker_locator_pose.pose.position.y = marker_locator_msg.y;
    marker_locator_pose.pose.position.z = 0;
    tf::Quaternion q = tf::createQuaternionFromRPY(0,0,marker_locator_msg.theta + MARKER_THETA_OFFSET);
    marker_locator_pose.pose.orientation.x = q.getX();
    marker_locator_pose.pose.orientation.y = q.getY();
    marker_locator_pose.pose.orientation.z = q.getZ();
    marker_locator_pose.pose.orientation.w = q.getW();
    std::cout << "Quaternions: " << q.getX() << " " << q.getY() << " " << q.getZ() << " " << q.getW() << std::endl;

    try
    {
        listener_.transformPose("map", marker_locator_pose, map_locator_pose);
	// Map the quaternion to a yaw angle
	double roll, pitch, yaw;
	tf::Quaternion q(map_locator_pose.pose.orientation.x, map_locator_pose.pose.orientation.y, map_locator_pose.pose.orientation.z, map_locator_pose.pose.orientation.w);
	tf::Matrix3x3 m(q);
	m.getRPY(roll, pitch, yaw);
        std::cout << "Map pose is: \n";
        std::cout << "x: \t " << map_locator_pose.pose.position.x << std::endl;
        std::cout << "y: \t " << map_locator_pose.pose.position.y << std::endl;
        std::cout << "theta: \t " << yaw << std::endl;

        // Set the initial pose
        initial_pose.pose.pose.position.x = map_locator_pose.pose.position.x; 		// Negative for some reason
        initial_pose.pose.pose.position.y = map_locator_pose.pose.position.y;
        initial_pose.pose.pose.orientation.x = map_locator_pose.pose.orientation.x;	
        initial_pose.pose.pose.orientation.y = map_locator_pose.pose.orientation.y;
        initial_pose.pose.pose.orientation.z = map_locator_pose.pose.orientation.z;// + QUATERNION_Z_OFFSET;
        initial_pose.pose.pose.orientation.w = map_locator_pose.pose.orientation.w;// + QUATERNION_W_OFFSET;
        _pub.publish(initial_pose);

 	/** DEBUG START **/
	/*std::cout << "Move to a goal: 0: Dispenser, 1: Line" << std::endl;
	int choice;
	std::cin >> choice;
	if(choice == 0) 
		moveToWpt(-0.756367, 3.72767, -0.739585);
	else if(choice == 1)
		moveToWpt(3.56954, 1.98764, -0.377832);*/
	/** DEBUG END **/
    } catch (tf::TransformException& e)
    {
        ROS_ERROR("Error in trasnform: %s", e.what());
    }

}


/**
 *@brief Calculates the length and orientation vector given two points in a cartesian coordinate system
 *
 *@param desired_dest_wpt The desired waypoint which the robot is to move to
 *
 *@return A length and orientation of the robot to move
**/
 /*
WptDirection WptNav::calcLengthAndOrientation(geometry_msgs::Point desired_dest_wpt)//marker_locator::marker_locator_msg start, geometry_msgs::Point end)
{
    /** DEBUG **//*
    geometry_msgs::Point end = desired_dest_wpt;
*/
    // Retrieve start point from the marker locator
   /* if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Unable to connect to Marker Locator. Not retunring any new waypoint");
        return WptDirection(0,0);
    }

    marker_locator_data_ = marker_locator_obj_.getMarkerLocatorData(false);*/
  /*  marker_locator_data_.x = 0,
    marker_locator_data_.y = 0;
    marker_locator_data_.theta = 0.7;

    std::cout << "From: (" << marker_locator_data_.x << " , " << marker_locator_data_.y << ")" << std::endl;
    std::cout << "To: (" << end.x << " , " << end.y << ")" << std::endl;

    WptDirection wpt;
    // Calculate the unit vector of the start and end position
    Eigen::Vector2d v1_norm(std::cos(marker_locator_data_.theta), std::sin(marker_locator_data_.theta));
    Eigen::Vector2d v2(end.x - marker_locator_data_.x, end.y - marker_locator_data_.y);
    Eigen::Vector2d v2_norm = v2/v2.norm();

    // Calculate the length and angle
    wpt.length = v2.norm();
    wpt.theta = std::atan2(v2_norm[1], v2_norm[0]) - std::atan2(v1_norm[1], v1_norm[0]);

    /** DEBUG **/
 /*   std::cout << "Wpt length: \t" << wpt.length << std::endl;
    std::cout << "Wpt theta: \t" << wpt.theta << std::endl;

    return wpt;
}*/

