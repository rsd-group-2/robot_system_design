#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Char.h"
#include "ros_android_interaction/MarkerLocatorData.h"
#include <sstream>
#include <string>
#include <geometry_msgs/TwistStamped.h>

// Order variables
std::string frobit = "frobit:_1";
std::string workcell = "workcell:_2";
std::string bricks = "bricks:_06";
std::string red = "red:_02";
std::string green = "green:_02";
std::string blue = "blue:_02";

// Variable strings from android
std::string control_mode = "";
std::string navigation = "";
std::string tipper = "";
std::string next_state = "";
std::string safety = "";

// Variable strings to android
std::string status = "";
std::string tipper_status = "";
std::string current_state = "";
std::string position = "";

// Variable strings to receive states
std::string main_state = "";
std::string line_navigation_state = "";
std::string slam_navigation_state = "";
std::string waypoint_navigation_state = "Waypoint_Navigation";

// Varibal strings to mission executioner node 
std_msgs::String msg_state;
geometry_msgs::TwistStamped msg_vel;

// Varibal string to tipper node
std_msgs::String msg_tipper;

// Tipper states
const std::string tipper_up = "1";
const std::string tipper_down = "2";
const std::string tipper_stop = "3";

// Velocity variables
const float vel_lin_max = 0.5;
const float vel_ang_max = 0.3;
const float vel_lin_step = 0.1;
const float vel_ang_step = 0.1;

// state booleans
bool manual = false;
bool safety_stop = false;
   
   // Subscribe from android
   void android_to_pc_callback(const std_msgs::String::ConstPtr& msg)
   {
	// Read incoming message and split it up in different strings by a "," seperator
	// and save them in an array
	std::istringstream ss(msg->data.c_str());
	std_msgs::String token;	
	std_msgs::String array[5];
	int cnt = 0;

	while(std::getline(ss, token.data, ',')) {
	    array[cnt].data = token.data.c_str();
	    cnt++;
	}
	
	// Only for debug
	//ROS_INFO("input: [%s]", msg->data.c_str());

	control_mode = array[0].data.c_str();
	navigation = array[1].data.c_str();
	tipper = array[2].data.c_str();
	next_state = array[3].data.c_str();
	safety = array[4].data.c_str();
   }

   // Subscribe from motor control
   void status_callback(const geometry_msgs::TwistStamped::ConstPtr& msg)
   {
	if (msg->twist.linear.x > 0 || msg->twist.angular.z > 0)
	{
		status = "moving";
	}
	else
	{
		status = "stationary";
	}	
   }

   // Subscribe from tipper control
   void tipper_callback(const std_msgs::String::ConstPtr& msg)
   {
	if ( tipper_up.compare(msg->data.c_str()) == 0)
	{
		tipper_status = "up";
	}
	else if ( tipper_down.compare(msg->data.c_str()) == 0)
	{
		tipper_status = "down";
	}
	else if ( tipper_stop.compare(msg->data.c_str()) == 0)
	{
		tipper_status = "stopped";
	}	
   }

   // Subscribe from line navigation state
   void line_navigation_state_callback(const std_msgs::String::ConstPtr& msg)
   {
	line_navigation_state = msg->data.c_str();
   }

   // Subscribe from waypoint/slam state
   void slam_state_callback(const std_msgs::String::ConstPtr& msg)
   {
	slam_navigation_state = msg->data.c_str();	
   }

   // Subscribe from main state
   void main_state_callback(const std_msgs::String::ConstPtr& msg)
   {
	main_state = msg->data.c_str();	
   }

   /**
    * This tutorial demonstrates simple sending of messages over the ROS system.
    */
   int main(int argc, char **argv)
   {
     ros::init(argc, argv, "talker");
   
     ros::NodeHandle n;

     // Create service to get position from waypoint navigation    
     ros::ServiceClient position_ser = n.serviceClient<ros_android_interaction::MarkerLocatorData>("waypoint_map");

     // Create subscriber for communication with android
     ros::Subscriber android_to_pc_sub = n.subscribe("android_to_pc", 1, android_to_pc_callback);

     // Create subscriber for mobile robot status
     ros::Subscriber status_sub = n.subscribe("/fmCommand/cmd_vel", 1, status_callback);

     // Create subscriber for tipper status
     ros::Subscriber tipper_sub = n.subscribe("/tip_transmit", 1, tipper_callback);   

     // Create subscriber for line navigation state
     ros::Subscriber line_navigation_state_sub = n.subscribe("/qr_scanner_topic", 1, line_navigation_state_callback);

     // Create subscriber for slam/waypoint navigation state
     ros::Subscriber slam_state_sub = n.subscribe("/slam_state_topic", 1, slam_state_callback);

     // Create subscriber for main state
     ros::Subscriber main_state_sub = n.subscribe("/main_state_topic", 1, main_state_callback);


     // Create publisher for communication with android
     ros::Publisher pc_to_android_pub = n.advertise<std_msgs::String>("pc_to_android", 100);
   
     // Create publisher for tipper state
     ros::Publisher tipper_pub = n.advertise<std_msgs::String>("/tip_receive", 100);

     // Create publisher for sending order to line_follower and slam/waypoint navigation
     ros::Publisher order_pub = n.advertise<std_msgs::String>("/order_topic", 100);

     // Create publisher for control mode
     ros::Publisher manual_mode_pub = n.advertise<std_msgs::String>("/manual_mode_topic", 100);

     // Create publisher for sending velocity
     ros::Publisher hmi_velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/manual_mode_vel_topic", 100);

  

     /*
     Need subscriber to MESH server 
     Need subscriber to position of mobile robot
     Need to split slam/waypoint in 2

     and need to implement how to send order to mission executioner    	
     */

     ros::Rate loop_rate(10);
   
     while (ros::ok())
     {
	if (safety.compare("stop") == 0 && safety_stop != true)
        {
		safety_stop = true;      		
	
		msg_state.data = "Manual_Mode_Start";
		manual_mode_pub.publish(msg_state);
		manual = true;		
		control_mode = "";		

		msg_vel.twist.linear.x = 0;
 		msg_vel.twist.angular.z = 0;		
		hmi_velocity_pub.publish(msg_vel);

		msg_tipper.data = tipper_stop;
		tipper_pub.publish(msg_tipper);		

		safety = "";		
	}
	else if (safety.compare("start") == 0 && safety_stop != false)
	{
		safety_stop = false;

		msg_tipper.data = tipper_down;
		tipper_pub.publish(msg_tipper);

		safety = "";
	}

      	if (control_mode.compare("manual") == 0 && manual != true)
        {
		manual = true;
		msg_state.data = "Manual_Mode_Start";
		manual_mode_pub.publish(msg_state);
		
		msg_vel.twist.linear.x = 0;
 		msg_vel.twist.angular.z = 0;		
		hmi_velocity_pub.publish(msg_vel);

		msg_tipper.data = tipper_down;
		tipper_pub.publish(msg_tipper);

		control_mode = "";
	}
	else if (control_mode.compare("auto") == 0 && manual != false)
	{
		manual = false;
		msg_state.data = "Manual_Mode_Stop";
		manual_mode_pub.publish(msg_state);
		
		control_mode = "";
	}

	if (manual && !safety_stop)
      	{
      		if (navigation.compare("up") == 0)
		{
			msg_vel.twist.linear.x += vel_lin_step;
			if (msg_vel.twist.linear.x > vel_lin_max)
			{
				msg_vel.twist.linear.x = vel_lin_max;	
			}
			hmi_velocity_pub.publish(msg_vel);

			navigation = "";	
		}
		else if (navigation.compare("down") == 0)
		{
			msg_vel.twist.linear.x -= vel_lin_step;
			if (msg_vel.twist.linear.x < -vel_lin_max)
			{
				msg_vel.twist.linear.x = -vel_lin_max;	
			}
 			hmi_velocity_pub.publish(msg_vel);

			navigation = "";	
		}
		else if (navigation.compare("left") == 0)
		{
			msg_vel.twist.angular.z += vel_ang_step;
 			if (msg_vel.twist.angular.z > vel_ang_max)
 			{
				msg_vel.twist.angular.z = vel_ang_max;	
			}
			hmi_velocity_pub.publish(msg_vel);

			navigation = "";	
		}
		else if (navigation.compare("right") == 0)
		{
			msg_vel.twist.angular.z -= vel_ang_step;
 			if (msg_vel.twist.angular.z < -vel_ang_max)
			{
				msg_vel.twist.angular.z = -vel_ang_max;	
 			}
			hmi_velocity_pub.publish(msg_vel);

			navigation = "";
		}
		
		if (tipper.compare("up") == 0)
		{
			msg_tipper.data = tipper_up;
			tipper_pub.publish(msg_tipper);

			tipper = "";
		}
		else if (tipper.compare("down") == 0)
		{
			msg_tipper.data = tipper_down;
			tipper_pub.publish(msg_tipper);

			tipper = "";
		}
      	}

	if ( main_state.compare("Line_Navigation") == 0)
	{
		current_state = line_navigation_state;
		position = "Line";
	}
	else if ( main_state.compare("Waypoint_Navigation") == 0)
	{
		ros_android_interaction::MarkerLocatorData srv;
		srv.request.request = 0;
		if (position_ser.call(srv))
		{
			std::ostringstream strs;
			strs << srv.response.marker_x << "," << srv.response.marker_y;
			position = strs.str();
		}

		current_state = waypoint_navigation_state;
	}
	else if ( main_state.compare("SLAM_Navigation") == 0)
	{
		ros_android_interaction::MarkerLocatorData srv;
		srv.request.request = 0;
		if (position_ser.call(srv))
		{
			std::ostringstream strs;
			strs << srv.response.marker_x << "," << srv.response.marker_y;
			position = strs.str();
		}

		current_state = slam_navigation_state;
	}
	else if ( main_state.compare("Manual_Mode") == 0)
	{
		current_state = "Manual mode";
		position = " ";
	}
	else if ( main_state.compare("Idle") == 0)
	{
		current_state = "Idle";
	}
	else if ( main_state.compare("Waiting_for_order") == 0)
	{
		current_state = "Waiting for order";
		position = " ";
	}

      std_msgs::String android_msg;
      std_msgs::String order_msg;
      std::stringstream android_ss;
      std::stringstream order_ss;

      // Publish to android
      android_ss << current_state + "," + tipper_status + "," + status + "," + position;
      android_msg.data = android_ss.str();   
      pc_to_android_pub.publish(android_msg);

      // Publish order
      order_ss << frobit << ",_" << workcell << ",_" << bricks << ",_" << red << ",_" << green << ",_" << blue;
      order_msg.data = order_ss.str();
      order_pub.publish(order_msg);
	
      ros::spinOnce();
  
      loop_rate.sleep();
    }
  
  
    return 0;
  }
