#ifndef _LINE_CLASS_H
#define _LINE_CLASS_H

#include <cmath>
#include <iostream>

#include <eigen3/Eigen/Dense>



#define PI 3.141593
#define RAD2DEG 180/PI

typedef Eigen::Vector2f Point2D;

/**
 * Point class to represent points in a Cartesian coordinate system
 */
template <class _T>
class Point
{
private:
    
public:
    _T x;
    _T y;
    Point() {}
    Point(_T x_coord, _T y_coord) : x(x_coord), y(y_coord) {}

    // Overload = operator
    inline Point operator=(Point a) {
        x = a.x;
        y = a.y;
        return a;
    }

    friend inline std::ostream& operator <<(std::ostream& os, const Point& a) {
        os << "(" << a.x << "," << a.y << ")";
        return os;
    }
};

/**
 * Line Class to represent lines in a Cartesian coordinate system
 */
template <class _T>
class Line
{
private:

public:
	Point<double> start, end;
    _T length;
    _T theta;
    _T x_angle,y_angle;
    bool fixed;
    
    // Default Constructor
    Line() {}
    
    // Paramterized Constructor
    Line(Point<double> _start, Point<double> _end) {
        start = _start;
        end = _end;
    }
};

#endif /* _LINE_CLASS_H */
