#include "LineExtract.hpp"
#include <math.h>
#include <iostream>
#include <assert.h>

/**
 * @brief: Split and Merge algorithm to detect lines given a set of
 * 		   points in a Cartesian coordinate system
 *
 * @params: set: Set of points to match lines to.
 */
std::vector<Line<double> > LineExtract::split_and_merge(std::vector<Point<double> >& set)
{
	//	Initialize S to contain all points non -1.
	std::vector<Point<double> > S;
	std::vector<Line<double> > lines;
	for(int i = 0; i < set.size(); i++)
	{
		if((set[i].x != -1) && (set[i].y != -1))
		{
			S.push_back(set[i]);
		}
	}

	//	Split:
	split(S, 0, S.size() - 1, lines);

	//	Verification of lines
	int n = 0;
	for(int i = 0; i < lines.size(); i++)
	{
		for(n = 0; n < round(lines[i].length); n++){}
		if(fabs(lines[i].end.x - (lines[i].start.x + n*lines[i].x_angle)) > 2)
		{
			lines[i].x_angle *= -1;
			lines[i].fixed = true;
		}
		else if(fabs(lines[i].end.y - (lines[i].start.y + n*lines[i].y_angle)) > 2)
		{
			lines[i].y_angle *= -1;
			lines[i].fixed = true;
		}
		else
		{
			lines[i].fixed = false;
		}
		n = 0;
	}

	//	Merge:
	//	If two consecutive segments are close/collinear enough, obtain the common line and find the most distant point.
	//	If distance <= threshold, merge both segments.
	for(int i = 0; i < lines.size(); i++)
	{
		for(int j = lines.size() - 1; j >= 0; j--)
		{
			if(i != j && i < j)
			{
				//	Check if the vectors have the same slope
				if(fabs(fabs(lines[i].x_angle) - fabs(lines[j].x_angle)) < 0.01 || fabs(fabs(lines[i].y_angle) - fabs(lines[j].y_angle)) < 0.01)
				{
					bool test1, test2, test3, test4;
					test1 = fabs(lines[i].start.x - lines[j].end.x) < MERGE_THRES && fabs(lines[i].start.y - lines[j].end.y) < MERGE_THRES;
					test2 = fabs(lines[i].start.x - lines[j].start.x) < MERGE_THRES && fabs(lines[i].start.y - lines[j].start.y) < MERGE_THRES;
					test3 = fabs(lines[i].end.x - lines[j].end.x) < MERGE_THRES && fabs(lines[i].end.y - lines[j].end.y) < MERGE_THRES;
					test4 = fabs(lines[i].end.x - lines[j].start.x) < MERGE_THRES && fabs(lines[i].end.y - lines[j].start.y) < MERGE_THRES;
					if(test1)
					{
						//	Combine lengths and find new start point.
						lines[i].start = lines[j].start;
						lines[i].length += lines[j].length;
						lines.erase(lines.begin() + j);
						j = lines.size();
					}
					else if(test2)
					{
						lines[i].start = lines[j].end;
						lines[i].length += lines[j].length;
						lines.erase(lines.begin() + j);
						j = lines.size();
					}
					else if(test3)
					{
						lines[i].end = lines[j].start;
						lines[i].length += lines[j].length;
						lines.erase(lines.begin() + j);
						j = lines.size();
					}
					else if(test4)
					{
						lines[i].end = lines[j].end;
						lines[i].length += lines[j].length;
						lines.erase(lines.begin() + j);
						j = lines.size();
					}
				}
			}
		}
	}

    return lines;
}

/**
 * @brief: Split the lines if the longest distance from a point to
 * 	       the prevous lines are above a threshold, maxDist
 *
 * @params:S: Point set
 *         left: Start index of the points to split
 *         right: End index of the points to split
 *         prevLines: Set of currently detected lines
 */
void LineExtract::split(std::vector<Point<double> > S, int left, int right, std::vector<Line<double> >& prevLines)
{
	// Check if there is enough points to fit a line
	if((right - left) > MINIMUM_POINTS) {
		//	Fit a line to the data
		Line<double> fitLine = fit(S, left, right);
		double dist, maxDist(0), maxIndex(0);

		//	Check furthest distance from line
		for(int i = left; i <= right; i++)
		{
			dist = shortestDist2(S[i], fitLine);
			if(dist > maxDist)
			{
				maxDist = dist;
				maxIndex = i;
			}
		}

		//std::cout << "Start: " << fitLine.start << " end: " << fitLine.end << std::endl;
		//std::cout << "Max distance from point to line: " << maxDist << std::endl;

		//	If distance is larger than threshold, split list at maxIndex. Run recursively
		if(maxDist > SPLIT_THRES)
		{
			split(S, left, maxIndex - 1, prevLines);
			split(S, maxIndex, right, prevLines);
		}
		else
		{
			prevLines.push_back(fitLine);
		}
	}
	return;
}


double df(double S_xx, double S_xy, double S_yy, double theta)
{
	return 2*S_xy*cos(2*theta) + (S_yy - S_xx)*sin(2*theta);
}

double f(double S_xx, double S_xy, double S_yy, double theta)
{
	return pow(cos(theta), 2)*S_xx + pow(sin(theta), 2)*S_yy + 2*cos(theta)*sin(theta)*S_xy;
}

/**
 * @brief: Fits a line to a dataset
 *
 * @params: data: Points to fit a line to
 * 			left: Left index of the points to fit
 * 			right: Right index of the points to fit
 */
Line<double> LineExtract::fit(std::vector<Point<double> > data, int left, int right)
{
	double avg_x(0), avg_y(0), S_xx(0), S_yy(0), S_xy(0), a, b, theta, theta1, theta2, res1, res2;
	//	Calculate average values.
	for(int i = left; i <= right; i++)
	{
		avg_x += data[i].x;
		avg_y += data[i].y;
	}
	avg_x /= (right - left + 1);
	avg_y /= (right - left + 1);

	//	Calculate variances and the covariance
	for(int i = left; i <= right; i++)
	{
		S_xx += pow(data[i].x - avg_x, 2);
		S_yy += pow(data[i].y - avg_y, 2);
		S_xy += (data[i].x - avg_x)*(data[i].y - avg_y);
	}

	//	Calculate a and b to estimate theta
	a = 2*S_xy;
	b = S_yy - S_xx;

	theta1 = 0.5*atan2(a, -b);
	theta2 = 0.5*atan2(-a, b);

	//	Check if theta1 and theta2 are valid solutions.
	assert(fabs(df(S_xx, S_xy, S_yy, theta1)) < 1e-8);
	assert(fabs(df(S_xx, S_xy, S_yy, theta2)) < 1e-8);

	res1 = f(S_xx, S_xy, S_yy, theta1);
	res2 = f(S_xx, S_xy, S_yy, theta2);

	if(res1 <= res2)
	{
		//	Use theta1
		theta = theta1;
	}
	else
	{
		//	Use theta2
		theta = theta2;
	}
	Line<double> estimate;
	estimate.x_angle = sin(theta);
	estimate.y_angle = -cos(theta);
	estimate.start = data[left];
	estimate.end = data[right];
	estimate.length = sqrt(pow(data[right].x - data[left].x, 2) + pow(data[right].y - data[left].y, 2));
	estimate.theta = theta;

	return estimate;
}

/**
 * @brief: Calculates the shortest distance from a point to a line
 *
 * @params: p: Point
 *  		l: Line
 */
double LineExtract::shortestDist(Point<double> p, Line<double> l)
{
	double x, y, dot;
    dot = (l.start.x - p.x)*l.x_angle +  (l.start.x - p.x)*l.y_angle;
	x = (l.start.x - p.x) - dot*l.x_angle;
	y = (l.start.y - p.y) - dot*l.y_angle;
    
	return sqrt(pow(x, 2) + pow(y, 2));
}

/**
 * @brief: Calculates the shortest distance from a point to a line.
 * 		   This function also supports vertical and horizontal lines.
 *
 */
double LineExtract::shortestDist2(Point<double> p, Line<double> l)
{

    return(std::abs((l.end.x - l.start.x)*(l.start.y - p.y) - (l.start.x - p.x)*(l.end.y - l.start.y))
           /(std::sqrt(std::pow(l.end.x  - l.start.x, 2) + std::pow(l.end.y - l.start.y, 2))));
}

double LineExtract::shortestAngle(Point<double> center_point, Point<double> mid_point, Line<double> line, cv::Point2d *line_point)
{
	Eigen::Vector2d A(line.end.x, line.end.y);
	Eigen::Vector2d B(line.start.x, line.start.y);
	Eigen::Vector2d C(center_point.x, center_point.y);
	Eigen::Vector2d v(B - A);

	// Normalize the line vector
	v.normalize();
	Eigen::Vector2d P = A + ((C - A).dot(v))*v;			// P is nearest point on the line
	line_point->x = P(0);
	line_point->y = P(1);

	// Calculate angle between point and line.
	Eigen::Vector2d CP(P - C);
	Eigen::Vector2d M(mid_point.x, mid_point.y);
	Eigen::Vector2d CM(M - C);					// M is the point at 0 degree angle (direction for robot)
	Eigen::Matrix2d CPM(2,2);
	CPM << CP, CM;
	double angle = atan2(CPM.determinant(), CP.dot(CM));
	return angle;
}

/**
 * @brief: Converts a set of polar coordinates to Cartesian
 *
 * @params: theta: Angle of the vector pointing from origo to the point
 * 			r:	   The magnitude of the vector
 */
// Convert a polar coordinate to a cartesian point
Point<double> LineExtract::convertPolarToCartesian(float theta, float r) {
	Point<double> p;
	p.x = r*std::cos(theta);
	p.y = r*std::sin(theta);
	return p;
}

/**
 * @brief: Calculates the distance between two points
 */
double LineExtract::Magnitude(Point<double> p1, Point<double> p2)
{
	return std::sqrt(std::pow(p2.x - p1.x,2) + std::pow(p2.y -p1.y,2));
}

/**
 * @brief: Remove outliers in a dataset using a statistical method.
 *
 * TODO: FInish it up!
 */
void LineExtract::removeStatistcalOutliers(std::vector<Point<double> >& set)
{
	// Remove statiscal outliers
	cv::Mat points(set.size(), 2, CV_32F, cv::Scalar(0));
	cv::Mat mean, covar, invcovar;

	cv::Mat p1(1, set.size(), CV_32F, cv::Scalar(0));
	cv::Mat p2(1, set.size(), CV_32F, cv::Scalar(0));

	for(size_t i = 0; i < set.size(); ++i) {
		points.at<float>(i,0) = set[i].x;
		points.at<float>(i,1) = set[i].y;
	}
	std::cout << "points: \n" << points;

	cv::calcCovarMatrix(points, covar, mean, CV_COVAR_NORMAL | CV_COVAR_ROWS);
	covar = covar/(points.rows - 1);


	std::cout << "Covar matrix \n" << covar;
	std::cout << "Mean matrix \n" << mean;


	int points_removed = 0;

	// Calculate the mean of the data set
	std::pair<double, double> means, std_devations;
 	for(size_t i = 0; i < set.size(); ++i) {
		means.first += set[i].x;
		means.second += set[i].y;
	}
 	means.first /= set.size();
 	means.second /= set.size();

 	// Calculate the standard deviations
 	for(size_t i = 0; i < set.size(); ++i) {
 		std_devations.first += std::pow(set[i].x - means.first, 2);
 		std_devations.second += std::pow(set[i].y - means.second, 2);
 	}
 	std_devations.first /= set.size();
 	std_devations.second /= set.size();

 	std::cout << "Mean values: " << means.first << " , " << means.second << std::endl;
 	std::cout << "Std deviation values: " << std_devations.first << " , "<< std_devations.second << std::endl;

 	// Remove outliers
 	/*for(size_t i = 0; i < set.size(); ++i) {
 		if(set[i].x > means.first + 2*std_devations.first) ||
 	}*/
}
