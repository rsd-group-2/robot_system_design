#ifndef _LINE_EXTRACT_H
#define _LINE_EXTRACT_H

// ---------------------------------------------------------------------------
// This software is in the public domain, furnished "as is", without technical
// support, and with no warranty, express or implied, as to its usefulness for
// any purpose.
//
// <LineExtract>.HPP
// Class containing a variety of line extraction algorithms.
// N.B. So far only merge and split has been written.
//
// Author: <Gudmundur Geir Gunnarsson>
// ---------------------------------------------------------------------------


#include "LineClass.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <eigen3/Eigen/Dense>

#include <vector>

#define FIELD_OF_VIEW	360
#define SPLIT_THRES		15//15		   // Centimeter
#define MERGE_THRES		50//1        // Centimeter
#define MINIMUM_POINTS 	2//30


class LineExtract
{
private:

	double shortestDist(Point<double> p, Line<double> l);
	Line<double> fit(std::vector<Point<double>> data, int left, int right);
	void split(std::vector<Point<double>> S, int left, int right, std::vector<Line<double> >& prevLines);

public:
	double x,y;						//	Position
	LineExtract() {}
    LineExtract(double _x, double _y) : x(_x), y(_y) {}

    Point<double> convertPolarToCartesian(float theta, float r);
    double Magnitude(Point<double> p1, Point<double> p2);
    void removeStatistcalOutliers(std::vector<Point<double>>& set);
    double shortestDist2(Point<double> p, Line<double> l);
    double shortestAngle(Point<double> p, Point<double> mid_point, Line<double> l, cv::Point2d *line_point);

    std::vector<Line<double> > split_and_merge(std::vector<Point<double> >& set);
};

#endif /* _LINE_EXTRACT_H */
