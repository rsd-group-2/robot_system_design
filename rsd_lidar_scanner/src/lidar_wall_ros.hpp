/*
 * lidar_wall_ros.hpp
 *
 *  Created on: Sep 25, 2015
 *      Author: student
 */

#ifndef LIDAR_WALL_ROS_HPP_
#define LIDAR_WALL_ROS_HPP_

#include <ros/ros.h>
#include <geometry_msgs/TwistStamped.h>
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "LineExtract.hpp"

#include <iostream>
#include <random>
#include <string>

#define SCALE_M_TO_CM 100
#define SCALE_M_TO_MM 1000

#define IMAGE_SIZE_PX 800
#define IMAGE_OFFSET_PX IMAGE_SIZE_PX/2

const double MIN_LINE_LENGTH_CM = 1;
const double MIN_DISTANCE_TO_OBSTACLE = 0.1;
const double MAX_DISTANCE_TO_OBSTACLE = 3.0;

const std::string WINDOW_HEADER = "Scan Points";

class LidarWallRos
{
public:
	LidarWallRos() {};

	LidarWallRos(std::string sub, std::string topic_);

	/**
	 * Callback function to handle incoming messages from the subscribed topic
	 */
	void lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan);

	/**
	 * Draw a point on an opencv image
	 */
	void drawPoint(cv::Mat *img, Point<double> p);

	/**
	 * Draw a line on an opencv image
	 */
	void drawLine(cv::Mat *img, Line<double> line);



private:
	ros::NodeHandle n_;
	ros::Publisher pub_;
	ros::Subscriber sub_;

	unsigned long last_time_;
	double err_sum_, last_err_;
	bool initialized_;

	// PID controller to correct the angle.
	// Currently only PD is enabled
	double PIDController(double current_angle, double distance);
};


#endif /* LIDAR_WALL_ROS_HPP_ */
