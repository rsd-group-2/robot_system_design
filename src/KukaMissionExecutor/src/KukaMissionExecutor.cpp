#include "KukaMissionExecutor.hpp"

KukaMissionExecutor::KukaMissionExecutor()
{
    _z = 0.375;
    _current_state = KukaState::RETURN_TO_START;
    _new_order = true;
}

void KukaMissionExecutor::run()
{
    // Setup ROS
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv, "KukaMissionExecutor");
    _node_handle_commands = new ros::NodeHandle;

    _servicePG70Home = _node_handle_commands->serviceClient<rsdmissionexecutor::Home>("/PG70/PG70/home");
    _servicePG70Close = _node_handle_commands->serviceClient<rsdmissionexecutor::Close>("/PG70/PG70/close");
    _servicePG70Stop = _node_handle_commands->serviceClient<rsdmissionexecutor::Stop>("/PG70/PG70/stop");
    _servicePG70Move= _node_handle_commands->serviceClient<rsdmissionexecutor::Move>("/PG70/PG70/move");
    _servicePG70Open= _node_handle_commands->serviceClient<rsdmissionexecutor::Open>("/PG70/PG70/open");

	// Gives the transformation to the pathplanner and then it goes to the position
    _pick_up_brick = _node_handle_commands->serviceClient<rsdmissionexecutor::nextBrick>("/nextBrick");
	
    // Get all Birck poses in the field of view (in the camera)
    _get_brick_poses = _node_handle_commands->serviceClient<rsdmissionexecutor::RequestBricks>("/brick_detector/detect_bricks");

    // Set the robot in configuration
    _go_to_q = _node_handle_commands->serviceClient<rsdmissionexecutor::goToQ>("/goToQ");

    // Robot is moveing
    _robot_is_moving = _node_handle_commands->serviceClient<rsdmissionexecutor::getIsMoving>("/KukaNode/IsMoving");

    _robot_set_deliver_pose = _node_handle_commands->serviceClient<rsdmissionexecutor::setRobotDropOff>("/setRobotDropOffPose");
    _robot_set_image_pose = _node_handle_commands->serviceClient<rsdmissionexecutor::setRobotImagePose>("/setRobotImagePose");

    // Set loop rate
    ros::Rate loop_rate(20);

    while(ros::ok()){

        switch(_current_state) {
        case KukaState::WAIT:{
            ros::spinOnce();
            if(_new_order) {
                _current_state = KukaState::FIND_BRICKS;
            }
        }
        break;
        case KukaState::FIND_BRICKS:{
            // Start brick detection
            _bricks.clear();
            rsdmissionexecutor::RequestBricks find_brick_srv;
            find_brick_srv.request.z = _z;
            if(!_get_brick_poses.call(find_brick_srv)) {
                ROS_ERROR_STREAM("Kuka Mission Executor: Failed to call brick detection service");
            } else {
                _bricks = find_brick_srv.response.bricks;
                if(!_bricks.empty()) {
                    // Find the first brick that fits the first brick in the order
                    _current_state = KukaState::PICK_UP_BRICK;
                    std::sort(_bricks.begin(), _bricks.end(), [](rsdmissionexecutor::Brick a, rsdmissionexecutor::Brick b) {
                        return b.clearance < a.clearance;
                    });
                } else {
                    // Start conveyor
                    _current_state = KukaState::FIND_BRICKS;
                }
            }
        }
        break;
        case KukaState::PICK_UP_BRICK:{
            rsdmissionexecutor::nextBrick next_brick_srv;
            next_brick_srv.request.pose = _bricks[0].pose;
            if(!_pick_up_brick.call(next_brick_srv)) { // This line returns when robot is in position
                ROS_ERROR_STREAM("Kuka Mission Executor: Failed to call nextBrick service");
            } else {

                // Wait with return until the robot is at the desired configuration
                rsdmissionexecutor::getIsMoving is_moving;
                bool robot_moving = true;
                while(robot_moving) {
                    if(!_robot_is_moving.call(is_moving)){
                    ROS_ERROR_STREAM("Path planner is moving call faild");
                    }

                    // Update movement status
                    boost::this_thread::sleep(boost::posix_time::milliseconds(300));
                    robot_moving = is_moving.response.isMoving;
                }

                // Grab brick
                rsdmissionexecutor::Move move;
                move.request.pos = 0.005;
                if(!_servicePG70Move.call(move))
                    ROS_ERROR("Failed to call the 'servicePG70Open' gui");

                _current_state = KukaState::DELIVER_BRICK;
            }
        }
        break;
        case KukaState::DELIVER_BRICK:{
            // Go to delivery position
            // Go to start position
            rsdmissionexecutor::setRobotDropOff setDropOffPose_srv;

            if(!_robot_set_deliver_pose.call(setDropOffPose_srv))
            {
                ROS_ERROR_STREAM("setImagePose_srv call faild");
            }

            // Wait with return until the robot is at the desired configuration
            rsdmissionexecutor::getIsMoving is_moving;
            bool robot_moving = true;
            while(robot_moving) {
                if(!_robot_is_moving.call(is_moving)){
                ROS_ERROR_STREAM("Path planner is moving call faild");
                }

                // Update movement status
                boost::this_thread::sleep(boost::posix_time::milliseconds(600));
                robot_moving = is_moving.response.isMoving;
            }

            // When in delivery position, drop brick
            // Open gripper
            rsdmissionexecutor::Move move;
            move.request.pos = 0.010;
            if(!_servicePG70Move.call(move))
                ROS_ERROR("Failed to call the 'servicePG70Open' gui");

            // Remove first brick from order
            _current_order.erase(_current_order.begin());

            _current_state = KukaState::RETURN_TO_START;
        }
        break;
        case KukaState::RETURN_TO_START:

            // Go to start position
            rsdmissionexecutor::setRobotImagePose setImagePose_srv;

            if(!_robot_set_image_pose.call(setImagePose_srv))
            {
                ROS_ERROR_STREAM("setImagePose_srv call faild");
            }


            // Wait with return until the robot is at the desired configuration
            rsdmissionexecutor::getIsMoving is_moving;
            bool robot_moving = true;
            while(robot_moving) {
                if(!_robot_is_moving.call(is_moving)){
                ROS_ERROR_STREAM("Path planner is moving call faild");
                }

                // Update movement status
                boost::this_thread::sleep(boost::posix_time::milliseconds(300));
                robot_moving = is_moving.response.isMoving;
            }


            // When in position:
            if(_current_order.empty()) {
                _current_state = KukaState::WAIT;
            } else {
                _current_state = KukaState::FIND_BRICKS;
            }
        break;
        }

        // Sleep some time
        loop_rate.sleep();
    }
}


KukaMissionExecutor::~KukaMissionExecutor()
{

}

