#ifndef KUKAMISSIONEXEXCUTOR_H
#define KUKAMISSIONEXEXCUTOR_H

// STL
#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <vector>
#include <algorithm>

// ROS
#include "ros/ros.h"
#include <ros/callback_queue.h>

// Gripper
#include "rsdmissionexecutor/Close.h"
#include "rsdmissionexecutor/Open.h"
#include "rsdmissionexecutor/Move.h"
#include "rsdmissionexecutor/Stop.h"
#include "rsdmissionexecutor/Home.h"

// Robot services
#include "rsdmissionexecutor/nextBrick.h"
#include "rsdmissionexecutor/RequestBricks.h"
#include "rsdmissionexecutor/goToQ.h"
#include "rsdmissionexecutor/getIsMoving.h"
#include "rsdmissionexecutor/setRobotImagePose.h"
#include "rsdmissionexecutor/setRobotDropOff.h"

// ROS Messages
#include "rsdmissionexecutor/Brick.h"

// RobWork
#include <iostream>
#include <rw/rw.hpp>

// Namespace
using namespace std;

using namespace rw::common;
using namespace rw::kinematics;
using namespace rw::models;

class KukaMissionExecutor
{

	enum class KukaState {WAIT, FIND_BRICKS, PICK_UP_BRICK, DELIVER_BRICK, RETURN_TO_START};
	
public:
    KukaMissionExecutor();
    ~KukaMissionExecutor();
    void run();

private:

    // ROS
    ros::NodeHandle *_node_handle_commands;

    // Gripper control
    ros::ServiceClient _servicePG70Home;
    ros::ServiceClient _servicePG70Close;
    ros::ServiceClient _servicePG70Stop;
    ros::ServiceClient _servicePG70Move;
    ros::ServiceClient _servicePG70Open;

    // Robot
    ros::ServiceClient _robot_is_moving;
    ros::ServiceClient _robot_set_image_pose;
    ros::ServiceClient _robot_set_deliver_pose;
    ros::ServiceClient _go_to_q;
	ros::ServiceClient _get_brick_poses;
    ros::ServiceClient _pick_up_brick;
	
    // ROS service server
    ros::ServiceServer _brick_sorting;

	KukaState _current_state;

	bool _new_order;
	double _z;
	std::vector<rsdmissionexecutor::Brick> _bricks;
	std::vector<rsdmissionexecutor::Brick> _current_order;
};

#endif // KUKAMISSIONEXEXCUTOR_H
