#include "KukaMissionExecutor.hpp"
#include "ros/ros.h"

int main(int argc, char* argv[])
{
    // Make a instance of the pathplanner
    KukaMissionExecutor kuka_mission_executor;

    // Run the planner and rosnode
    kuka_mission_executor.run();

    return 0;
}
