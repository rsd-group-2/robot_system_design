#ifndef KUKAPATHPLANNIG_H
#define KUKAPATHPLANNIG_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>

// ROS
#include "ros/ros.h"
#include <ros/callback_queue.h>

// Robot services
#include "rsdkukapathplanner/getConfiguration.h"
#include "rsdkukapathplanner/setConfiguration.h"
#include "rsdkukapathplanner/getQueueSize.h"
#include "rsdkukapathplanner/getIsMoving.h"
#include "rsdkukapathplanner/stopRobot.h"
#include "rsdkukapathplanner/setRobotImagePose.h"
#include "rsdkukapathplanner/setRobotDropOff.h"
#include "rsdkukapathplanner/goToQ.h"
#include "rsdkukapathplanner/nextBrick.h"

// Gripper services
#include <rsdkukapathplanner/Move.h>
#include <rsdkukapathplanner/Stop.h>
#include <rsdkukapathplanner/Home.h>
#include <rsdkukapathplanner/Close.h>
#include <rsdkukapathplanner/Open.h>
//#include <rsdkukapathplanner/GetQ.h>

// RobWork
#include <iostream>
#include <rw/rw.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathplanners/rrt/RRTQToQPlanner.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rwlibs/pathplanners/rrt/RRTPlanner.hpp>
#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>


#include <boost/foreach.hpp>



// Namespace
using namespace std;

using namespace rw::common;
using namespace rw::math;
using namespace rw::kinematics;
using namespace rw::loaders;
using namespace rw::models;
using namespace rw::pathplanning;
using namespace rw::proximity;
using namespace rw::trajectory;
using namespace rwlibs::pathplanners;
using namespace rwlibs::pathoptimization;
using namespace rwlibs::proximitystrategies;


class KukaPathPlanning
{

public:
    KukaPathPlanning(string wcfile );
    KukaPathPlanning( rw::models::WorkCell::Ptr workcell);
    ~KukaPathPlanning();
    void run();

private:

    bool grapsPointSelction(rw::math::Transform3D<double> poseBrick, rw::math::Q& gripConfiguration);
    bool inverseKinematics(rw::math::Transform3D<double> tcpPose, rw::math::Q& inverseSolution);
    rw::math::Transform3D<double> maketransform3d( rw::math::Transform3D<double> poseBrick, double theataOffset);

    // RRT Planner calls
    void initRRTPlanner();
    bool queryRRT( rw::math::Q qStart, rw::math::Q qGoal, Path<rw::math::Q> &path, unsigned int maxTime, double epsilon);

    // Method for planningn the new Q and set it
    bool setRobotQ(rw::math::Q goToQ);

    bool callbackSetTCPGripPose(rsdkukapathplanner::nextBrick::Request &req,
                                              rsdkukapathplanner::nextBrick::Response &res);

    bool callbackSetDropOffREF(rsdkukapathplanner::setRobotDropOff::Request &req,
                                              rsdkukapathplanner::setRobotDropOff::Response &res);

    bool callbackSetImageREF(rsdkukapathplanner::setRobotImagePose::Request &req,
                            rsdkukapathplanner::setRobotImagePose::Response &res);

    bool callbackSetRobotQ(rsdkukapathplanner::goToQ::Request &req,
                                             rsdkukapathplanner::goToQ::Response &res);

    // Robwork workcell, states and frames
    rw::models::WorkCell::Ptr _wc;
    rw::kinematics::State _state, _defaultState, _newState;
    rw::kinematics::Frame* _TCPFrame;
    rw::kinematics::Frame* _cameraFrame;
    rw::kinematics::Frame* _legoRefFrame;
    rw::kinematics::Frame* _imageFrame;

    //Shared variables between threads


    // Devices used in the cell
    rw::models::SerialDevice *_robot;
    rw::models::Device::Ptr _gripper;

    // ROS
    ros::NodeHandle *_nodeHandleRobot;
    ros::NodeHandle *_nodeHandleGripper;
    ros::NodeHandle *_nodeHandleCommands;

    //ros::CallbackQueue callbackQueueRobot;
    //ros::CallbackQueue callbackQueueGripper;
    ros::CallbackQueue callbackQueueCommands;

    // Robot services
    ros::ServiceClient _serviceRobotGetConfiguration;
    ros::ServiceClient _serviceRobotSetConfiguration;
    ros::ServiceClient _serviceRobotgetQueueSize;
    ros::ServiceClient _serviceRobotIsMoving;
    ros::ServiceClient _serviceRobotStop;

    // Grippers services
    ros::ServiceClient _servicePG70Home;
    ros::ServiceClient _servicePG70Close;
    ros::ServiceClient _servicePG70Stop;
    ros::ServiceClient _servicePG70Move;
    ros::ServiceClient _servicePG70Open;
//    ros::ServiceClient _servicePG70GetQ;

    // ROS service server
    ros::ServiceServer _qToQPlanning;
    ros::ServiceServer _setRobotTCP;
    ros::ServiceServer _setRobotDeliver;
    ros::ServiceServer _setGrip;

    // Path Planning
    CollisionDetector* _detector;
    QMetric::Ptr _qMetric;

    // Mutexes
    std::mutex _qMutex, _poseMutex;
};

#endif // KUKAPATHPLANNIG_H
