 #include "ros/ros.h"
#include "KukaPathPlanning.hpp"

int main(int argc, char* argv[])
{
    // Make a instance of the pathplanner
    KukaPathPlanning pathplanner("/home/student/RSD_catkin_ws/src/Workcell/Scene.wc.xml");

    // Run the planner and rosnode
    pathplanner.run();

    return 0;
}
