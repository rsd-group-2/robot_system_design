// Includes
#include "SampleWidget.hpp"

// Functions
SampleWidget::SampleWidget(QWidget *parent): QWidget(parent)
{
	setupUi(this);
    connect(_moverobot, SIGNAL(released()), this, SLOT(eventBtn()));
    connect(_resetrobot, SIGNAL(released()), this, SLOT(eventBtn()));
    connect(_gripperbtn, SIGNAL(released()), this, SLOT(eventBtn()));
    connect(_homerobot, SIGNAL(released()), this, SLOT(eventBtn()));
    connect(_checkBox_update_q_gui, SIGNAL(released()), this, SLOT(checkbox_update_gui_q()));
}

SampleWidget::~SampleWidget()
{
	//
}

void SampleWidget::initialize(WorkCell::Ptr workcell, rws::RobWorkStudio* rws) 
{

	// Load in objects
	_rwWorkCell = workcell;
	_rws = rws;
	_state = _rws->getState();
    _newState = _state;

	// Get devices (robots)
	const std::vector<rw::common::Ptr<Device> >& devices = _rwWorkCell->getDevices();

	// Check if any (if not = no robot: return)
	if(devices.size() == 0)
		return;

    std::cout << "Load the robot" << std::endl;
    // Get device name

    uint _devicesLenght = devices.size();
    int _deviceNumber = -1;

    for(unsigned int i = 0; i < _devicesLenght; i++)
    {
        if((devices[i])->getName() == "KukaKr6")
        {
            _deviceNumber = i;
        }
    }

    _gripper = _rwWorkCell->findDevice("PG70");

    std::cout << "Device number: " + _deviceNumber << std::endl;
    _device = dynamic_cast<SerialDevice*>(devices[_deviceNumber].get());
    std::cout << "Loaded device " << _device->getName() << std::endl;


    std::cout << "The robot is loaded" << std::endl;
    _gripperOpen = true;

    //
    std::cout << "Setting default state" << std::endl;
    _device->setQ(Q(6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),_state);
    _device->setQ(Q(6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),_newState);

    _mutex_update_gui__ros.lock();
    _update_gui_from_ros = true;
    _mutex_update_gui__ros.unlock();


    // Thread with the ros communication
    std::cout << "Setting up ROS class interface" << std::endl;
    _worker_nodethread = new QThread();
    _nodethread = new NodeThread();
    _nodethread->moveToThread(_worker_nodethread);

    qRegisterMetaType< rw::math::Q >("rw::math::Q");

    connect(this, SIGNAL(setQ(rw::math::Q)),_nodethread, SLOT(set_q_slot(rw::math::Q)),Qt::QueuedConnection);
    connect(this, SIGNAL(gripper_open_close()),_nodethread, SLOT(gripper_control()),Qt::QueuedConnection);
    connect(_nodethread, SIGNAL(update_gui_Q(rw::math::Q, bool)),
            this, SLOT(update_gui_q(rw::math::Q, bool)),Qt::QueuedConnection);
    _worker_nodethread->start();

    std::cout << "Done!" << std::endl;

}

void SampleWidget::stateChangedListener(const rw::kinematics::State &state)
{
	// Check if any device is loaded
    if(_device == NULL)
        return;

	// Get new q
    _newState = state;
}

void SampleWidget::eventBtn()
{
    QObject *obj = sender();

    if(obj == _moverobot)
	{
		QMessageBox msgBox;
        msgBox.setText("The robot will move to the position.");
		msgBox.setInformativeText("Do you want to continue?");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Cancel);
		int ret = msgBox.exec();
	
		switch (ret) 
		{
			case QMessageBox::Ok:
				{

                    // The new configuration
                    Q q_current(6);
                    q_current = _device->getQ(_newState);
                    _rws->setState(_state);
                    _checkBox_update_q_gui->setChecked(true);
                    _mutex_update_gui__ros.lock();
                    _update_gui_from_ros = _checkBox_update_q_gui->isChecked();
                    _mutex_update_gui__ros.unlock();
                    emit setQ(q_current);

				}
				break;

			case QMessageBox::Cancel:
				// Cancel was clicked
				break;

			default:
				// should never be reached
				break;
		}
	}
    else if (obj == _resetrobot)
    {
        // Reset last state in RWS
        _checkBox_update_q_gui->setChecked(false);
        _rws->setState(_state);
    }
    else if (obj == _homerobot)
    {
        _checkBox_update_q_gui->setChecked(false);
        _mutex_update_gui__ros.lock();
        _update_gui_from_ros = _checkBox_update_q_gui->isChecked();
        _mutex_update_gui__ros.unlock();

        _device->setQ(Q(6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),_newState);
        _rws->setState(_newState);
    }
    else if(obj == _gripperbtn)
	{
		QMessageBox msgBox;
        msgBox.setText("the Gripper will Open/Close.");
		msgBox.setInformativeText("Do you want to continue?");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Cancel);
		
		int ret = msgBox.exec();	
		switch (ret) 
		{
			case QMessageBox::Ok:
				{
                    // Open or close the gripper
                    emit gripper_open_close();
				}
				break;

			case QMessageBox::Cancel:
		       		// Cancel was clicked
			 	break;

		      	default:
		       		// Should never be reached
				break;
		}   	
	}
}

void SampleWidget::update_gui_q(rw::math::Q q_robot, bool gripper_open)
{
    _mutex_update_gui__ros.lock();

    if(_update_gui_from_ros)
    {
        _device->setQ(q_robot, _state);
        _newState = _state;

        if(gripper_open)
        {
            _gripper->setQ(Q(1,0.02), _state);
        }
        else
        {
            _gripper->setQ(Q(1,0.0), _state);
        }

        _rws->setState(_state);
    }
    _mutex_update_gui__ros.unlock();
}

void SampleWidget::checkbox_update_gui_q()
{
    _mutex_update_gui__ros.lock();
    _update_gui_from_ros = _checkBox_update_q_gui->isChecked();
    _mutex_update_gui__ros.unlock();
}

bool SampleWidget::openWorkCell() 
{
	QMessageBox::information(this, "Open", "Open WorkCell");
	return true;
}

void SampleWidget::callback() 
{
	QCoreApplication::processEvents();
}
