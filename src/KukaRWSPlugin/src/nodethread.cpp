#include "nodethread.hpp"

NodeThread::NodeThread()
{
    _gripperOpen = true;

    // Timer setup for configuration update
    _gui_refresh_timer = new QTimer(this);
    _ros_refresh_timer = new QTimer(this);

    connect(_gui_refresh_timer, SIGNAL(timeout()), this, SLOT(refresh_q_GUI()));
    connect(_ros_refresh_timer, SIGNAL(timeout()), this, SLOT(rosrun()));

    int argc = 1;
    char *argv[] = {const_cast<char *>("rwsplugin"), nullptr};
    ros::init(argc, argv, "rwsplugin_node");
    _nodeHandle = new ros::NodeHandle;

    _servicePG70Home = _nodeHandle->serviceClient<rsdkukarwsplugin::Home>("PG70/home");
    _servicePG70Close = _nodeHandle->serviceClient<rsdkukarwsplugin::Close>("PG70/close");
    _servicePG70Stop = _nodeHandle->serviceClient<rsdkukarwsplugin::Stop>("PG70/stop");
    _servicePG70Move= _nodeHandle->serviceClient<rsdkukarwsplugin::Move>("PG70/move");
    _servicePG70Open= _nodeHandle->serviceClient<rsdkukarwsplugin::Open>("PG70/open");

    _servicegoToQ = _nodeHandle->serviceClient<rsdkukarwsplugin::goToQ>("/goToQ");
    _serviceGetQ = _nodeHandle->serviceClient<rsdkukarwsplugin::getConfiguration>("KukaNode/GetConfiguration");

    _gui_refresh_timer->start(200); //time specified in ms
    _ros_refresh_timer->start(200);
}

void NodeThread::rosrun()
{
    ros::spinOnce();
}

/*
 * SLOT Takes the rw::math::Q that the gui want to set and puts into ROS
 *
*/
void NodeThread::set_q_slot(rw::math::Q q_new)
{
    std::cout << "Before planner call" << std::endl;
    rsdkukarwsplugin::goToQ setConfig;
    // Fill out information and update the robot configuration
    for(int i = 0; i < 6; i++)
    {
          setConfig.request.q[i] = q_new(i);
    } 

    if(!_servicegoToQ.call(setConfig))
    {
         ROS_ERROR("Failed to call the 'SetConfiguration' from gui");
    }
    std::cout << "After planner call" << std::endl;
}

/*
 * SLOT Manuel control of the gripper from the GUI to open/close the gripper
 *
*/
void NodeThread::gripper_control()
{
    if(!_gripperOpen)
    {
        // Open gripper
        _gripperOpen = true;
        rsdkukarwsplugin::Open open;
        open.request.power = 10.0;
        if(!_servicePG70Open.call(open))
            ROS_ERROR("Failed to call the 'servicePG70Open' gui");
    }
    else {
        _gripperOpen = false;

        // Close gripper
        rsdkukarwsplugin::Close close;
        close.request.power = 10.0;
        if(!_servicePG70Close.call(close))
            ROS_ERROR("Failed to call the 'servicePG70Close' gui ");
    }
}

/*
 * Updates the gui with a periodic interval
 *
*/
void NodeThread::refresh_q_GUI()
{
    // Get robot state
    rw::math::Q currentQ(6);

    rsdkukarwsplugin::getConfiguration getConfig;
    if(!_serviceGetQ.call(getConfig))
    {
         ROS_ERROR("Failed to call the 'getConfiguration' to update gui");
    }
    else
    {
       for( unsigned int i = 0; i < 6; i++)
        {
            currentQ(i) =  getConfig.response.q[i];
        }
        emit update_gui_Q(currentQ, _gripperOpen);
    }
}

/*
 * Update the images taken fro brick dection
 *
*/
void NodeThread::update_gui_image()
{
    // Some fancy update for the image
}
