#include <ros.h>
#include <std_msgs/String.h>


ros::NodeHandle nh;

std_msgs::String pushed_msg;
ros::Publisher pub_button("system_state", &pushed_msg);

const int button_pin = 7;
const int idle_led = 13;
const int navigation_led = 8;

int last_reading;
char navigation[16]="navigation_mode";
char idle[5]="idle";


void setup()
{
  nh.initNode();
  nh.advertise(pub_button);
   
  
  //input pin for our push button
  pinMode(button_pin, INPUT);
  
  //output pin for LEDs
  pinMode(idle_led, OUTPUT);
  pinMode(navigation_led, OUTPUT);
  
  //Enable the pullup resistor on the button 
  digitalWrite(button_pin, HIGH);
  
  //The button is a normally button
  last_reading = ! digitalRead(button_pin);
 
}

void loop()
{
  
  int reading = digitalRead(button_pin);
  //is there any change in the state?
  if (last_reading!= reading){
      
      
      if(reading == HIGH){
      
      pushed_msg.data = idle;
      digitalWrite(idle_led,true);
      digitalWrite(navigation_led,false); 
      //reading= LOW
      }else{
      digitalWrite(navigation_led,true); 
      digitalWrite(idle_led,false);    
      pushed_msg.data = navigation;
        }
  }
    
    last_reading = reading;
    pub_button.publish(&pushed_msg);
    

  
  nh.spinOnce();
}