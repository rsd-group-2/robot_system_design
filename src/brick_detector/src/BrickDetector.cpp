#include "BrickDetector.h"
#include <algorithm>

BrickDetector::BrickDetector(ros::NodeHandle& nh) {
	_nh = std::make_shared<ros::NodeHandle>(nh);
	_img_trans = std::make_shared<image_transport::ImageTransport>(image_transport::ImageTransport(nh));
	
	_pub = _img_trans->advertise("/brick_detector/detected_image", 1);
	_sub = _img_trans->subscribe("/usb_cam/image_raw", 1, &BrickDetector::imageCallback, this);
	_detect_service = _nh->advertiseService("/brick_detector/detect_bricks", &BrickDetector::requestBricks, this);

	ros::Subscriber cam_inf_sub = _nh->subscribe("/usb_cam/camera_info", 1, &BrickDetector::cameraInfoCallback, this);
	while(_cam_info == nullptr) {
		ros::spinOnce();
	}
	cam_inf_sub.shutdown();
}

void BrickDetector::cameraInfoCallback(const sensor_msgs::CameraInfo& msg) {
	ROS_INFO_STREAM("Getting calibration");
	_cam_info = std::make_shared<sensor_msgs::CameraInfo>(msg);
}

bool BrickDetector::requestBricks(brick_detector::RequestBricks::Request& req, brick_detector::RequestBricks::Response& res) {
	// Retrieve input variables
	double z = req.z;
	double focal_length_x = _cam_info->K[0];
    double focal_length_y = _cam_info->K[4];
	double center_x = 640;//_cam_info->K[2];
	double center_y = 360;//_cam_info->K[5];
	ROS_INFO_STREAM("f_x = " << focal_length_x << " f_y = " << focal_length_y);
	ROS_INFO_STREAM("c_x = " << center_x << " c_y = " << center_y);
	
	// Run detection code
	ROS_INFO_STREAM("Starting brick detection");
	std::vector<BrickInfo> bricks = detect(_current_image);

	ROS_INFO_STREAM("Bricks found: " << bricks.size());
	// Fill response
	std::vector<brick_detector::Brick> bricks_ros;
	for(auto it = bricks.begin(); it != bricks.end(); ++it) {
		// Transform coordinates
		brick_detector::Brick brick_tmp;
		brick_tmp.pose.x = (it->position.x - center_x) * z / focal_length_x;
		brick_tmp.pose.y = (it->position.y - center_y) * z / focal_length_y;
		brick_tmp.pose.theta = it->position.theta;
		brick_tmp.brick_length = sqrt(pow(it->length*cos(it->position.theta) * z / focal_length_x, 2)
									  + pow(it->length*sin(it->position.theta) * z / focal_length_y, 2));
		brick_tmp.color = it->color;

		// Find clearence
		double min_clearance = 1000;
		for(auto cl_it = bricks.begin(); cl_it != bricks.end(); ++cl_it) {
			if(cl_it != it) {
				auto length_vec = shortestBBToBBDistance(it->bounding_box, cl_it->bounding_box);
				double length_x = length_vec.x * z / focal_length_x;
				double length_y = length_vec.y * z / focal_length_y;
				double length = std::sqrt(std::pow(length_x, 2) + std::pow(length_y, 2));
				if(length < min_clearance) {
					min_clearance = length;
				}
			}
		}
		brick_tmp.clearance = min_clearance; 
		bricks_ros.push_back(brick_tmp);

		// Add text to detected image
		std::stringstream s_stream;
		std::string text;
		s_stream << "X pos: " << round(it->position.x - center_x);
		text = s_stream.str();
		s_stream.str("");
		
		cv::Point text_origin(it->position.x, it->position.y);

		cv::Size textSize = cv::getTextSize(text, cv::FONT_HERSHEY_PLAIN,
											0.75, 1, nullptr);

		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.75, cv::Scalar(0, 255, 0));

		s_stream << "Y pos: " << round(it->position.y - center_y);
		text = s_stream.str();
		s_stream.str(""); 
		text_origin.y += textSize.height;
		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.75, cv::Scalar(0, 255, 0));

		s_stream << "Theta: " << it->position.theta;
		text = s_stream.str();
		s_stream.str("");
		text_origin.y += textSize.height;
		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.75, cv::Scalar(0, 255, 0));

		s_stream << "Length: " << round(brick_tmp.brick_length*1000) / 10.0 << "cm";
		text = s_stream.str();
		s_stream.str("");
	    text_origin.y += textSize.height;
		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.75, cv::Scalar(0, 255, 0));
		
		s_stream << "Clearance: " << round(brick_tmp.clearance*1000) / 10.0 << "cm";
		text = s_stream.str();
		s_stream.str("");
		text_origin.y += textSize.height;
		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.75, cv::Scalar(0, 255, 0));
				
		std::string color;
		switch(it->color) {
		case 0:
			color = "Color: Red";
			break;
		case 1:
			color = "Color: Blue";
			break;
		case 2:
			color = "Color: Yellow";
			break;
		default:
			color = "Color: Invalid";
		}
		s_stream << color;
		text = s_stream.str();
		s_stream.str("");
		text_origin.y += textSize.height;
		cv::putText(_detected_image, text, text_origin, cv::FONT_HERSHEY_PLAIN, 0.5, cv::Scalar(0, 255, 0));
		
	}
	res.bricks = bricks_ros;
	
	_pub.publish(cv_bridge::CvImage(std_msgs::Header(), "bgr8", _detected_image).toImageMsg());

	return true;
}

void BrickDetector::imageCallback(const sensor_msgs::ImageConstPtr& img) {
	cv_bridge::CvImagePtr img_ptr;
	try {
		//ROS_INFO_STREAM("Converting image");
		//ROS_INFO_STREAM("Image type " << img->encoding);
		cv::Mat image_dist = cv_bridge::toCvCopy(img, "bgr8")->image;
		
		// Undistort
		if(_cam_info != nullptr) {
			cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << _cam_info->K[0], _cam_info->K[1], _cam_info->K[2],
									 _cam_info->K[3], _cam_info->K[4], _cam_info->K[5],
									 _cam_info->K[6], _cam_info->K[7], _cam_info->K[8]);

			cv::undistort(image_dist, _current_image, camera_matrix, _cam_info->D);
		} else {
			image_dist.copyTo(_current_image);
		}
	} catch(cv_bridge::Exception& e) {
		ROS_ERROR_STREAM("Could not convert sensor_msgs/Image to cv::Mat");
	}
}

std::vector<BrickInfo> BrickDetector::detect(cv::Mat image) {
	std::vector<BrickInfo> brick_info;
	
    // Region of interest mask
	cv::Mat roi_mask = cv::Mat::zeros(image.rows, image.cols, image.type());
	cv::rectangle(roi_mask, cv::Point(0, 165), cv::Point(1280, 495), cv::Scalar(255, 255, 255), -1);

	// Convert image to HSV space
	image.mul(roi_mask);
	//image.copyTo(_image);
	_image = image;
	getHSVImages();

	ROS_INFO_STREAM("Removing conveyor belt");
	removeConveyor();
	
	// Create gray_scale image
	cv::Mat gray_scale, gray_scale_src;
	cv::cvtColor(image, gray_scale, CV_BGR2GRAY);
	gray_scale.copyTo(gray_scale_src);
	
	// Find contours
	std::vector< std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	double polygon_approx_aggressiveness = 17;
	ROS_INFO_STREAM("Finding contours");
	cv::bilateralFilter(gray_scale_src, gray_scale, 9, 150, 150);
	cv::findContours(gray_scale, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0) );

	int count = 0;
	
	// Filter contours by area
	for(unsigned int i = 0; i < contours.size(); ++i) {
		double area = cv::contourArea(contours[i]);
		if(area < 1e3 || area > 1e7) {
			continue;
		}

		count++;
		// Kíkja inn í hvern contour og tjékka hvort það séu fleiri en einn litur.
		auto contour_hist = getHueHistogramFromContour(contours[i]);

		// Count number of peaks in histogram
		int n_peaks = countHistogramPeaks(contour_hist);
		//std::cout << std::endl << "Number of peaks: " << n_peaks << std::endl;
			
		std::vector<cv::Point> contour_approx;
		cv::approxPolyDP(contours[i], contour_approx, polygon_approx_aggressiveness, true);
		
		if(n_peaks > 1) {
			// Ef það eru fleiri en einn litur: Eyða contournum og búa til nýja eftir litunum
			//std::cout << "More than one color" << std::endl;
			splitMultiColorContour(contours[i], contours);
		} else if(cv::isContourConvex(contour_approx) == false) {
			// Annars: Tékka ef contourinn er convex: Ef hann er ekki convex, reyna að deila contournum upp í convex hluta
			//std::cout << "One color, concave" << std::endl;
			
			splitConcaveContour(contours[i], contours);
		} else {
			// Find bounding box
			auto min_area_rect = cv::minAreaRect(contours[i]);

			// Perform PCA
			BrickPos position = getPose(min_area_rect, image);
			
			// Drawing min area rect
			cv::Point2f rect_points[4];
			min_area_rect.points(rect_points);
			double long_side_length = 0;
			for( int j = 0; j < 4; j++ ) {
				cv::line(image, rect_points[j], rect_points[(j+1)%4], cv::Scalar(255, 255, 255), 1, 8 );
				double length = sqrt(pow(rect_points[j].x - rect_points[(j+1)%4].x, 2) + pow(rect_points[j].y - rect_points[(j+1)%4].y, 2));
				if(length > long_side_length) {
					long_side_length = length;
				}
			}
			
			// Fill brick info
			BrickInfo info;
			info.position = position;
			info.color = static_cast<int>(getColorCodeFromHistogram(contour_hist));
			info.length = long_side_length;
			info.bounding_box = min_area_rect;
			brick_info.push_back(info);
		}
	}

	ROS_INFO_STREAM("Done, contours looked at: " << count);
	_detected_image = image;
	return brick_info;
}

void BrickDetector::splitMultiColorContour(std::vector<cv::Point> curr_contour, std::vector< std::vector<cv::Point> >& contours) {
	// Get image from ROI inside of contour
	// Look for contours on hue inside of object
	cv::Rect roi = cv::boundingRect(curr_contour);
	cv::Mat mask = cv::Mat::zeros(_hue.size(), CV_8UC1);
	std::vector< std::vector<cv::Point> > contour;
	contour.clear();
	contour.push_back(curr_contour);
	cv::drawContours(mask, contour, 0, cv::Scalar(255), CV_FILLED); 
			
	// Extract region using mask for region
	cv::Mat contour_region;
	cv::Mat image_ROI;
	_hue.copyTo(image_ROI, mask);
			
	for(int k = 0; k < mask.cols; k++) {
		for(int j = 0; j < mask.rows; j++) {
			if(mask.at<uchar>(j, k) != 0 && image_ROI.at<uchar>(j, k) < 20) {
				image_ROI.at<uchar>(j, k) = 179;
			}
		}
	}
	cv::Canny(image_ROI, image_ROI, 30, 60, 3);
	cv::Mat image_ROI_src;
	image_ROI.copyTo(image_ROI_src);
	cv::bilateralFilter(image_ROI_src, image_ROI, 5, 150, 150);
	std::vector< std::vector<cv::Point> > contours_tmp;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(image_ROI, contours_tmp, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
	double area = cv::contourArea(curr_contour);
	for(unsigned int i = 0; i < contours_tmp.size(); ++i) {
		double current_contour_area = cv::contourArea(contours_tmp[i]);
		if(area - current_contour_area > area*0.2 && current_contour_area > 1000) {
			contours.push_back(contours_tmp[i]);
		}
	}
}

void BrickDetector::splitConcaveContour(std::vector<cv::Point> curr_contour, std::vector< std::vector<cv::Point> >& contours) {
	cv::Rect roi = cv::boundingRect(curr_contour);
	cv::Mat mask = cv::Mat::zeros(_hue.size(), CV_8UC1);
	std::vector< std::vector<cv::Point> > contour;
	contour.clear();
	contour.push_back(curr_contour);
	cv::drawContours(mask, contour, 0, cv::Scalar(255), CV_FILLED);

	// Open mask
	cv::Mat struct_elem = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 9));
	cv::dilate(mask, mask, struct_elem);
	cv::erode(mask, mask, struct_elem);
	contour.clear();
	std::vector<cv::Vec4i> hierarchy_tmp;
	cv::findContours(mask, contour, hierarchy_tmp, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
	std::vector<cv::Point> contour_approx = contour[0];

    // Find convexity defects
	std::vector<int> hull_idxs;
	std::vector<cv::Point> hull_points;
	std::vector<cv::Vec4i> defects;
	cv::convexHull(contour_approx, hull_idxs, false, true);
	cv::convexHull(contour_approx, hull_points, false, false);
	cv::convexityDefects(contour_approx, hull_idxs, defects);
			
	std::sort(defects.begin(), defects.end(),
			  [](const cv::Vec4i& a, const cv::Vec4i& b) -> bool {
				  return (a.val[3]/256.0 > b.val[3]/256.0);
			  });
			
	// Convex decomposition
	cv::Point concavity1 = contour_approx[defects[0][2]];
	cv::Point concavity2 = contour_approx[defects[1][2]];
	cv::line(mask, concavity1, concavity2, cv::Scalar(0), 3, 8);
	hierarchy_tmp.clear();
	cv::findContours(mask, contour, hierarchy_tmp, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
	for(auto c : contour) {
		contours.push_back(c);
	}
}

void BrickDetector::getHSVImages() {
	cv::cvtColor(_image, _hsv_image, CV_BGR2HSV, 0);

	std::vector<cv::Mat> channels;
	cv::split(_hsv_image, channels);
	_hue = channels[0];
	_saturation = channels[1];
	_value = channels[2];
}

void BrickDetector::removeConveyor() {
	// Create histogram from hue
	int chls[] = {0};
	cv::MatND hist;
	int hbins = 10;
	float max_hue = 180;
	int bin_size[] = {hbins};
	float hranges[] = {0, max_hue};
	const float* ranges[] = {hranges};
	cv::calcHist(&_hsv_image, 1, chls, cv::Mat(), hist, 1, bin_size, ranges, true, false );
	double max_val = 0;
	cv::Point max_loc;
	cv::minMaxLoc(hist, 0, &max_val, 0, &max_loc);

	int lower_histogram_threshold = (max_loc.y - 2)*(max_hue/hbins) - 1;
	int upper_histogram_threshold = (max_loc.y + 2)*(max_hue/hbins) - 1;
	int saturation_threshold = 100;
	int value_threshold = 60;
	for(int i = 0; i < _hue.cols; i++) {
		for(int j = 0; j < _hue.rows; j++) {
			if((_hue.at<uchar>(j,i) < upper_histogram_threshold) && (_hue.at<uchar>(j, i) > lower_histogram_threshold) ||
			   (_saturation.at<uchar>(j,i) < saturation_threshold) || _value.at<uchar>(j,i) < value_threshold) {
				_image.at<cv::Vec3b>(j,i)[0] = 0;
				_image.at<cv::Vec3b>(j,i)[1] = 0;
				_image.at<cv::Vec3b>(j,i)[2] = 0;
			}
		}
	}
}

cv::MatND BrickDetector::getHueHistogramFromContour(std::vector<cv::Point> contour) {
	// Create cv::Mat from region inside contour
	cv::Rect roi = cv::boundingRect(contour);
	cv::Mat mask = cv::Mat::zeros(_hue.size(), CV_8UC1);
	std::vector< std::vector<cv::Point> > contours;
	contours.push_back(contour);
	cv::drawContours(mask, contours, 0, cv::Scalar(255), CV_FILLED); 

	// Extract region using mask for region
	cv::Mat contour_region;
	cv::Mat image_ROI;
	_hue.copyTo(image_ROI, mask); // 'image' is the image you used to compute the contours.
	
	for(int i = 0; i < mask.cols; i++) {
		for(int j = 0; j < mask.rows; j++) {
			if(mask.at<uchar>(j, i) == 0) {
				image_ROI.at<uchar>(j, i) = 255;
			}
		}
	}
	contour_region = image_ROI(roi);
	for(int i = 0; i < contour_region.cols; i++) {
		for(int j = 0; j < contour_region.rows; j++) {
			if(contour_region.at<uchar>(j, i) < 20) {
				contour_region.at<uchar>(j, i) = 179;
			}
		}
	}
		
	// Create histogram
	int chls[] = {0};
	cv::MatND hist;
	int hbins = 12;
	float max_hue = 180;
	int bin_size[] = {hbins};
	float hranges[] = {0, max_hue};
	const float* ranges[] = {hranges};
	cv::calcHist(&contour_region, 1, chls, cv::Mat(), hist, 1, bin_size, ranges, true, false );
	cv::normalize(hist, hist);
	return hist;
}

int BrickDetector::countHistogramPeaks(cv::MatND hist) {
	bool last_col_peak = false;
	int n_peaks = 0;

	for(unsigned int i = 0; i < hist.rows; ++i) {
		if(hist.at<float>(i, 0) > 0.25) {
			if(!last_col_peak) {
				n_peaks++;
				last_col_peak = true;
			}
		} else {
			last_col_peak = false;
		}
	}

	return n_peaks;
}

Color BrickDetector::getColorCodeFromHistogram(cv::MatND hist) {
	double max_val = 0;
	cv::Point max_loc;
	cv::minMaxLoc(hist, 0, &max_val, 0, &max_loc);
	unsigned int n_bins = hist.rows;
	double color_value = max_loc.y * (180.0/n_bins);
	Color color;
	if(color_value <= 60 && color_value >= 15) {
		color = Color::YELLOW;
	} else if(color_value < 150 && color_value >= 90) {
		color = Color::BLUE;
	} else if(color_value >= 150) {
		color = Color::RED;
	} else {
		color = Color::INVALID;
	}
	return color;
}

void drawAxis(cv::Mat& img, cv::Point p, cv::Point q, cv::Scalar colour, const float scale = 0.2)
{
    double angle;
    double hypotenuse;
    angle = atan2( (double) p.y - q.y, (double) p.x - q.x ); // angle in radians
    hypotenuse = sqrt( (double) (p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
	// Scale arrow
	q.x = (int) (p.x - scale * hypotenuse * cos(angle));
    q.y = (int) (p.y - scale * hypotenuse * sin(angle));
	cv::line(img, p, q, colour, 1, CV_AA);
    // Arrow hooks
    p.x = (int) (q.x + 9 * cos(angle + CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle + CV_PI / 4));
	cv::line(img, p, q, colour, 1, CV_AA);
    p.x = (int) (q.x + 9 * cos(angle - CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle - CV_PI / 4));
	cv::line(img, p, q, colour, 1, CV_AA);
}

BrickPos BrickDetector::getPose(cv::RotatedRect bounding_box, cv::Mat& img) {
    //Construct a buffer used by the pca analysis
	cv::Point2f pts_arr[4];
	bounding_box.points(pts_arr);
	std::vector<cv::Point> pts;
	for(unsigned int i = 0; i < 4; i++) {
		pts.push_back(pts_arr[i]);
	}
    int sz = static_cast<int>(pts.size());
	cv::Mat data_pts = cv::Mat(sz, 2, CV_64FC1);
    for (int i = 0; i < data_pts.rows; ++i) {
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }
    //Perform PCA analysis
	cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
    //Store the center of the object
	cv::Point center = cv::Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
								 static_cast<int>(pca_analysis.mean.at<double>(0, 1)));
    //Store the eigenvalues and eigenvectors
	std::vector<cv::Point2d> eigen_vecs(2);
	std::vector<double> eigen_val(2);
    for (int i = 0; i < 2; ++i){
        eigen_vecs[i] = cv::Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
									pca_analysis.eigenvectors.at<double>(i, 1));
        eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
    }
    // Draw the principal components
	cv::circle(img, center, 3, cv::Scalar(255, 0, 255), 2);
	cv::Point p1 = center + 0.02 * cv::Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
	cv::Point p2 = center - 0.02 * cv::Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
	drawAxis(img, center, p1, cv::Scalar(0, 255, 0), 1);
	BrickPos position;
	position.x = center.x;
	position.y = center.y;
	position.theta = atan2(eigen_vecs[0].y, eigen_vecs[0].x);
	return position;
}

cv::Point BrickDetector::shortestBBToBBDistance(cv::RotatedRect a, cv::RotatedRect b) {
	double min = 1000;
	cv::Point min_point;
	cv::Point2f vertices_a[4], vertices_b[4];
	a.points(vertices_a);
	b.points(vertices_b);
	for(unsigned int i = 0; i < 4; i++) {
		for(unsigned int j = 0; j < 4; j++) {
			cv::Point curr_point = closestPointSegmentToSegment(vertices_a[i], vertices_a[(i + 1)%4], vertices_b[j], vertices_b[(j + 1)%4]);
			double length = sqrt(curr_point.dot(curr_point));
			if(length <= min) {
				min_point = curr_point;
			}
		}
	}
	return min_point;
}

double BrickDetector::clampToRange(double n, double min, double max) {
	if(n < min) {
		return min;
	}
	if(n > max) {
		return max;
	}
	return n;
}

cv::Point BrickDetector::closestPointSegmentToSegment(cv::Point p1, cv::Point q1, cv::Point p2,  cv::Point q2) {
	cv::Point d1 = q1 - p1; // Direction vector of first line
	cv::Point d2 = q2 - p2; // Direction vector of second line
	cv::Point r = p1 - p2;

	double a = d1.dot(d1); // Squared length of first segment
	double e = d2.dot(d2); // Squared length of second segmen
	double f = d2.dot(r);

	// Check if either segment is has degenerated into a point
	double epsilon = 0.0001;
	double s, t;
	cv::Point c1, c2;
	if(a <= epsilon && e <= epsilon) {
		// Both segments are points
		return p1 - p2;
	}
	if(a <= epsilon) {
		// First segment degenerates into a point
		s = 0.0;
		t = f / e;
		t = clampToRange(t, 0.0, 1.0);
	} else {
		double c = d1.dot(r);
		if(e <= epsilon) {
			//Second segment degenerates into a point
			t = 0.0;
			s = clampToRange(-c / a, 0.0, 1.0);
		} else {
			// General non-degenerate case
			double b = d1.dot(d2);
			double denom = a*e - b*b;

			// If segments not parallel, compute closest point on L1 to L2 and clamp to segment S1. Else pick arbitrary S
			if(denom != 0.0) {
				s = clampToRange((b*f - c*e) / denom, 0.0, 1.0);
			} else {
				s = 0.0;
			}

			// Compute point on L2 closest to S1(s)
			t = (b*s + f)/e;

			// If t in [0,1] done. Else clamp t, recompute s for the new value of t and clamp s to [0,1]
			if(t < 0.0) {
				t = 0.0;
				s = clampToRange(-c/a, 0.0, 1.0);
			} else if(t > 1.0) {
				t = 1.0;
				s = clampToRange((b - c)/a, 0.0, 1.0);
			}
		}
	}
	c1 = p1 + d1*s;
	c2 = p2 + d2*t;
	return c1 - c2;
}
