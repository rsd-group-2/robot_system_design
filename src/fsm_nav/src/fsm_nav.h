#ifndef _FSM_NAV_H_
#define _FSM_NAV_H_

#include <ros/ros.h>
#include <geometry_msgs/Twist.h> 
#include <geometry_msgs/TwistStamped.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/Pose.h>
#include <actionlib/client/simple_action_client.h>
#include <stdlib.h> 
#include <std_msgs/String.h>

#include </home/rsdgroup2/roswork/src/marker_locator/src/waypoint_navigator.hpp>

namespace fsm_nav 
{
 
	enum FSMstate
	{	ENABLED,
			DISABLED,
			ROBOT_ERROR
		     };	
	
	enum SLAMstate
	{
			IDLE,
			CHARGING_STATION,
			DISENGAGED,
			BRICK_DISPENSER,
			BOX_OUT,
			LINE_FOLLOWER,
			LINE_FOLLOWER_ACTIVE,
			LINE_FOLLOWER_BACK,
			BOX_IN,
			DOCKING,
			ERROR 		
		};
};

using namespace fsm_nav;

class navigation_fsm
{

private:
   	ros::NodeHandle nh;
  	ros::Publisher _vel;
	ros::Publisher pub_decider;
    ros::Subscriber _pose;
    ros::Subscriber _sub_decider;
    move_base_msgs::MoveBaseGoal goal;

	// Waypoint Navigator
	WptNav<double> wpt_nav_;
	
	std_msgs::String main_state_str_;

	void poseReceivedCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
	
	void robotStateCallback(const std_msgs::String::ConstPtr& str);	
		

public:
	//constructor
	 navigation_fsm();
	//distructor
	~navigation_fsm(){}

	FSMstate  STATE;
	SLAMstate NAV_STATE;
	SLAMstate NAV_STATE_PREVIOUS;
	std_msgs::String robot_state;
	geometry_msgs::PoseWithCovarianceStamped _pos;


	void init();
	
	void fsmWrapper();
	//disengage the robot from the charging station
	void disengaging();

	void navigateToDispenser();
	
	void leaveBox();

	void wptNavigation();

	void wptnavigationBack();
	
	void approachingChargingStation();

	void docking();
	
	void setSlamState(SLAMstate state);

	void switchArea();
	


};

#endif


