#include <cv.h>

#include <sstream>

//Include headers for OpenCV Image processing
#include <opencv2/imgproc/imgproc.hpp>
//Include headers for OpenCV GUI handling
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>


#include <math.h>
#include <unistd.h>
#include <time.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/TwistStamped.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <std_msgs/String.h>
#include <string>


//Paths and addresses:
const std::string address = "http://192.168.43.85:8080/?action=stream/frame.mjpg";
const std::string disc_path = "/home/smyl/Desktop/test/";

// Constants declaration:
const double KP = 0.0018;
const double KI = 0.0;
const double KD = 0.0;
const double K_ANTIOVERSHOOT = 1/5;
const double ANGLE_TO_POSITION_RATIO = 8;
const double SPEED_CONSTANT = 0.20;
const int PUBLISHING_FREQUENCY = 30;
const double LINEAR_TURN_CONSTANT = 0.1;//0.08;
const double ANGULAR_TURN_CONSTANT = -0.25;//-0.2;

cv::Mat received_image;
std::string qr_message;
bool new_image;
bool qr_code_received;

//from https://rosstitchernode.wordpress.com/2014/06/05/ros-and-opencv-with-usb_cam/
void imageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
    //Convert from the ROS image message to a CvImage suitable for working with OpenCV for processing
    cv_bridge::CvImagePtr cv_ptr;

    try
    {
        //Always copy, returning a mutable CvImage
        //OpenCV expects color images to use BGR channel order.
        cv_ptr = cv_bridge::toCvCopy(original_image, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        //if there is an error during conversion, display it
        ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
        return;
    }

    received_image = cv_ptr->image;
    new_image = true;
}

void qrCallback(const std_msgs::String::ConstPtr& msg)
{
    qr_message = msg->data.c_str();
    qr_code_received = true;
}

struct point
{
    double X,Y;
};

double PID_controller( double error, double prev_error, double &integral )
{
    double derivative;
    integral=K_ANTIOVERSHOOT*integral+error;
    derivative=prev_error-error;
    return KP*error+KI*integral+KD*derivative;
}

double distance( double x1, double y1, double x2, double y2 )
{
    return sqrt( ( x2 - x1 ) * ( x2 - x1 ) + (y2 - y1 ) * (y2 - y1 ) );
}


int main(int argc, char **argv)
{

    //Node initialisation
    ros::init(argc, argv, "line_follower_node");
    ros::NodeHandle n;

    ROS_INFO("Line follower started");

    image_transport::ImageTransport it(n);

    //Publishers
    ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/line_follower_subtopic", 10);

    //Subscriptions
    //ros::Subscriber image_sub = n.subscribe<sensor_msgs::Image>("/usb_cam/image_raw", 1, imageCallback);
    image_transport::Subscriber image_sub = it.subscribe("usb_cam/image_raw", 1, imageCallback);
    ros::Subscriber qr_sub = n.subscribe<std_msgs::String>("/qr_scanner_topic", 1, qrCallback);

    ROS_INFO("Subscribers, publishers created");


    double error, prev_error = 0, integral = 0;
    double angle, position, position_error;
    geometry_msgs::TwistStamped vel_msg;
    double pid_output, angular;
    bool crossing_detected = false;
    bool crossing_detected2 = false;
    int no_crossing_counter = 0;
    int time_turn=0;
    int crossing_counter = 0;
    new_image = false;
    qr_code_received = false;

    int time;

    cv::SimpleBlobDetector::Params params;						//Parameters for SimpleBlobDetecto
    params.blobColor=255;
    params.filterByColor=true;
    params.minArea=800;
    params.maxArea=10000000;
    params.filterByArea=true;
    //params.minCircularity=0;
    //params.maxCircularity=0.5;
    params.filterByCircularity=false;
    params.filterByInertia=false;
    //params.minInertiaRatio=0;
    //params.maxInertiaRatio=0.08;
    params.filterByConvexity=false;
    //params.minConvexity=0.9;
    //params.maxConvexity=1;
    params.minDistBetweenBlobs=20;

    /* OpenCV 3.0.0 */
    //cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    /* OpenCV 3.0.0 end */

    /* OpenCV 2.X.X */
    cv::SimpleBlobDetector blob_detector(params);
    blob_detector.create("SimpleBlobDet");
    /* OpenCV 2.X.X end */



    //The main loop frequency
    int frequency;
    ros::Rate loop_rate(PUBLISHING_FREQUENCY);

    /*cv::namedWindow("out", CV_WINDOW_AUTOSIZE);
    cv::namedWindow("out1", CV_WINDOW_AUTOSIZE);
    cv::namedWindow("out2", CV_WINDOW_AUTOSIZE);
    cv::namedWindow("Found features", CV_WINDOW_AUTOSIZE);*/
    cv::namedWindow("Found features", CV_WINDOW_AUTOSIZE);



    while (ros::ok())
    {


        time = clock();
        if( qr_code_received )
        {
            qr_code_received = false;
            if( qr_message == "GEH8H5" )
            {
                no_crossing_counter = -20;
                crossing_counter = 0;
            }
        }

        if( crossing_detected )
        {
            time_turn = clock();
            crossing_detected = false;
            //crossing_detected2 = true;
            crossing_counter++;
        }

        if( crossing_counter == 3 && (clock() - time_turn)*1000/CLOCKS_PER_SEC <600 )
        {
            vel_msg.twist.linear.x = LINEAR_TURN_CONSTANT;
            vel_msg.twist.angular.z = ANGULAR_TURN_CONSTANT;
  
            vel_msg.header.stamp = ros::Time::now();
            velocity_pub.publish(vel_msg);
            ros::spinOnce();
            loop_rate.sleep();
            continue;
            
        }

        if( crossing_counter == 4 )
        {
            vel_msg.twist.linear.x = 0;
            vel_msg.twist.angular.z = 0;
  
            vel_msg.header.stamp = ros::Time::now();
            velocity_pub.publish(vel_msg);
            ros::spinOnce();
            loop_rate.sleep();
            continue;
            
        }
        
        //assures that it's entered only when there's a new image to see
        if( !new_image )
        {
            ros::spinOnce();
            loop_rate.sleep();
            continue;
        }
        new_image = false;




        cv::Mat input_image = received_image;

        cv::resize(input_image, input_image, cv::Size(), 0.5, 0.5);


        /*cv::Mat imgs(input_image.cols, input_image.rows, input_image.type());
        transpose(input_image, imgs);  
        flip(imgs, imgs,0);
        input_image = imgs;*/

        //cv::Mat temp_image = input_image.clone();

        cv::Mat temp_image = input_image(cv::Range(input_image.rows/2,input_image.rows), cv::Range(input_image.cols/4, input_image.cols*3/4));

        cv::cvtColor(temp_image, temp_image, CV_BGR2GRAY);					//Convert to grayscale

        input_image = temp_image;

        cv::Mat out;

        cv::threshold(temp_image, out, 130, 255, cv::THRESH_BINARY_INV);			//Threshold to binary
        cv::Mat out_top = out(cv::Range(0,out.rows*2/3), cv::Range::all());
        cv::Mat out_bottom = out(cv::Range(out.rows/3,out.rows), cv::Range::all());

        //cv::imshow("out", out);
        //cv::imshow("out1", out_top);
        //cv::imshow("out2", out_bottom);
        //cv::waitKey(3);


        if( cv::countNonZero(out) < 0.5*out.rows*out.cols )
        {
             no_crossing_counter++;
        }
        else if( no_crossing_counter>30 )
        {
             no_crossing_counter = 0;
             crossing_detected = true;
             ros::spinOnce();
             loop_rate.sleep();
             continue;
        }
        



        std::vector<cv::KeyPoint> keyPoints, keyPoints_top, keyPoints_bottom;
        blob_detector.detect( out, keyPoints );
        blob_detector.detect( out_top, keyPoints_top );
        blob_detector.detect( out_bottom, keyPoints_bottom );

        std::vector<cv::KeyPoint> keyPoints_checked;
        float size_max;
        std::vector<cv::KeyPoint>::iterator largest_blob_iterator;

        //if (keyPoints_bottom.size() > 1) cout<<"ups";
        if( keyPoints_bottom.size() > 0 )
        {
            size_max = 0;
            for(std::vector<cv::KeyPoint>::iterator blob_iterator = keyPoints_bottom.begin(); blob_iterator != keyPoints_bottom.end(); blob_iterator++)
            {
                if( blob_iterator->size > size_max )
                {
                    size_max = blob_iterator->size;
                    largest_blob_iterator = blob_iterator;
                }
            }
            largest_blob_iterator->pt.y += out.rows/3;
            keyPoints_checked.push_back( *largest_blob_iterator );
        }
        if( keyPoints.size() > 0 )
        {
            size_max = 0;
            for(std::vector<cv::KeyPoint>::iterator blob_iterator = keyPoints.begin(); blob_iterator != keyPoints.end(); blob_iterator++)
            {
                if( blob_iterator->size > size_max )
                {
                    size_max = blob_iterator->size;
                    largest_blob_iterator = blob_iterator;
                }
            }
            keyPoints_checked.push_back( *largest_blob_iterator );
            /*if( size_max > 0.3*out.cols*out.rows )
            {
                crossing_detected = true;
                ros::spinOnce();
                loop_rate.sleep();
                continue;
            }*/
        }
        if( keyPoints_top.size() > 0 )
        {
            size_max = 0;
            for(std::vector<cv::KeyPoint>::iterator blob_iterator = keyPoints_top.begin(); blob_iterator != keyPoints_top.end(); blob_iterator++)
            {
                if( blob_iterator->size > size_max )
                {
                    size_max = blob_iterator->size;
                    largest_blob_iterator = blob_iterator;
                }
            }
            keyPoints_checked.push_back( *largest_blob_iterator );
        }

        //cout<<"found in all images:"<<keyPoints_checked.size()<<endl;

        double a, b, x1, x2;

        if( keyPoints_checked.size() == 3 )
        {
            //line(input_image, Point(keyPoints_checked[1].pt.x, keyPoints_checked[1].pt.y) , Point(keyPoints_checked[0].pt.x, keyPoints_checked[0].pt.y), Scalar(0, 255, 0), 3 );
            //line(input_image, Point(keyPoints_checked[2].pt.x, keyPoints_checked[2].pt.y) , Point(keyPoints_checked[1].pt.x, keyPoints_checked[1].pt.y), Scalar(0, 255, 0), 3 );

            // Parameter "a" of y = ax + b line
            a = - ( keyPoints_checked[2].pt.y - keyPoints_checked[0].pt.y ) / ( keyPoints_checked[2].pt.x - keyPoints_checked[0].pt.x );
            b = input_image.rows - keyPoints_checked[0].pt.y - a * keyPoints_checked[0].pt.x;
            x1 = ( -b ) / a;
            x2 = (input_image.rows - b) / a;
            position = (input_image.rows - b) / a;
            cv::line(input_image, cv::Point(x1, input_image.rows) , cv::Point(x2, 0), cv::Scalar(0, 255, 0), 3 );
        }
        else if( keyPoints_checked.size() == 2 )
        {
            //line(input_image, Point(keyPoints_checked[1].pt.x, keyPoints_checked[1].pt.y) , Point(keyPoints_checked[0].pt.x, keyPoints_checked[0].pt.y), Scalar(0, 255, 0), 3 );
            a = - ( keyPoints_checked[1].pt.y - keyPoints_checked[0].pt.y ) / ( keyPoints_checked[1].pt.x - keyPoints_checked[0].pt.x );
            b = input_image.rows - keyPoints_checked[0].pt.y - a * keyPoints_checked[0].pt.x;
            x1 = ( -b ) / a;
            x2 = (input_image.rows - b) / a;
            position = (input_image.rows - b) / a;
            cv::line(input_image, cv::Point(x1, input_image.rows) , cv::Point(x2, 0), cv::Scalar(0, 255, 0), 3 );
        }
        else
        {
            ROS_INFO("Line not found");
            //cv::imshow("out", out);
            //cv::imshow("out1", out_top);
            //cv::imshow("out2", out_bottom);
            cv::imshow("Found features", input_image);
            cv::waitKey(4);
            time=(clock()-time)*1000/CLOCKS_PER_SEC;;
            ROS_INFO("Elapsed time: %d ms", time);
            ros::spinOnce();
            loop_rate.sleep();
            continue;
        }

        /*for(int i=0; i<keyPoints_checked.size(); i++)
        {
        	circle(input_image, Point(keyPoints_checked[i].pt.x, keyPoints_checked[i].pt.y), 30, Scalar(0, 0, 255), 4);
        	circle(input_image, Point(keyPoints_checked[i].pt.x, keyPoints_checked[i].pt.y), 5, Scalar(0, 0, 255), 10);
        }*/
        cv::circle(input_image, cv::Point(x1, input_image.rows), 30, cv::Scalar(0, 0, 255), 4);
        cv::circle(input_image, cv::Point(x1, input_image.rows), 5, cv::Scalar(0, 0, 255), 10);
        cv::circle(input_image, cv::Point(x2, 0), 30, cv::Scalar(0, 0, 255), 4);
        cv::circle(input_image, cv::Point(x2, 0), 5, cv::Scalar(0, 0, 255), 10);
        cv::circle(input_image, cv::Point(position, input_image.rows/2), 30, cv::Scalar(0, 0, 255), 4);
        cv::circle(input_image, cv::Point(position, input_image.rows/2), 5, cv::Scalar(0, 0, 255), 10);


        //Angle is calculated ( theta = arctan a )
        angle = atan( a );

        //Here the angle between vertical line and longer beam is calculated:
        angle = angle < 0 ? -M_PI/2 - angle : M_PI/2 - angle;

        //From radians to degrees:
        angle *= 180 / M_PI;

        std::stringstream ss2;
        ss2 << (int)(angle);
        std::string angle_string = ss2.str() + " deg";
        cv::putText(input_image, angle_string, cv::Point(out.cols/10, out.rows/1.1),  cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 255, 0), 2);

        cv::imshow("out", out);
        //cv::imshow("out1", out_top);
        //cv::imshow("out2", temp_image);
        cv::imshow("Found features", input_image);
        cv::waitKey(4);

        position_error = input_image.cols/2 - position;
        //If data makes sense
        /*if( angle > -90 && angle < 90 )//&& position_error > -256 && position_error < 256 )
        {
            //Error calculation:
            error = - angle;// - position_error;

            //Steer value:
            pid_output = PID_controller(error, prev_error, integral);

            //Error is remembered for next loop
            prev_error = error;

            //pid_output<-1 ? angular = -1 : pid_output>1 ? angular = 1 : angular = pid_output;

            if( pid_output < -1 )
                angular = -1;
            else if( pid_output > 1 )
                angular = 1;
            else
                angular = pid_output;

            //if( angle > -1 && angle <1 )
                //angular = 0; 

            ROS_INFO("Calculated angular velocity: %f", angular);
            ROS_INFO("PID output: %f", pid_output);

            vel_msg.twist.linear.x = SPEED_CONSTANT;
            vel_msg.twist.angular.z = angular;
  
            vel_msg.header.stamp = ros::Time::now();
            velocity_pub.publish(vel_msg);
        }*/
        if( position_error > -400 && position_error < 400 )
        {
            //Error calculation:
            error = - position_error;

            //Steer value:
            pid_output = - PID_controller(error, prev_error, integral);

            //Error is remembered for next loop
            prev_error = error;

            //pid_output<-1 ? angular = -1 : pid_output>1 ? angular = 1 : angular = pid_output;

            if( pid_output < -1 )
                angular = -1;
            else if( pid_output > 1 )
                angular = 1;
            else
                angular = pid_output;

            //if( angle > -1 && angle <1 )
                //angular = 0; 

            ROS_INFO("Calculated angular velocity: %f", angular);
            ROS_INFO("PID output: %f", pid_output);

            vel_msg.twist.linear.x = SPEED_CONSTANT;
            vel_msg.twist.angular.z = angular;
  
            vel_msg.header.stamp = ros::Time::now();
            velocity_pub.publish(vel_msg);
        }

        time=(clock()-time)*1000/CLOCKS_PER_SEC;;
        ROS_INFO("Elapsed time: %d ms", time);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}
