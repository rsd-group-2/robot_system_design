#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/String.h>
#include <string>
#include <time.h>

//Variables in which the incoming data is saved
bool qr_code_received;
bool turn_to_cage;
int velocity_divisor;
std::string qr_message;
geometry_msgs::TwistStamped vel_from_line_follower;

//Constants
const int PUBLISHING_FREQUENCY = 1000;
const std::string CAGE_ONE_CODE = "AAAAA";
const std::string CAGE_TWO_CODE = "W1LS2M";
const std::string CAGE_THREE_CODE = "CCCCC";

//Functions that are called when new message arrives
void qrCallback(const std_msgs::String::ConstPtr& msg)
{
  qr_message = msg->data.c_str();
  qr_code_received = true;
}

void lineFollowerCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
  vel_from_line_follower = *msg;
}


int main(int argc, char **argv)
{

  //Node initialisation
  ros::init(argc, argv, "line_follower_executor_node");
  ros::NodeHandle n;

  ROS_INFO("Line follower executor started");

  //Publishers
  ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/line_follower_topic", 10);

  //Subscriptions
  ros::Subscriber qr_sub = n.subscribe<std_msgs::String>("/qr_scanner_topic", 1, qrCallback);
  ros::Subscriber line_follower_sub = n.subscribe<geometry_msgs::TwistStamped>("/line_follower_subtopic", 5, lineFollowerCallback);

  //The main loop frequency
  int frequency;
  n.param("frequency", frequency, PUBLISHING_FREQUENCY);
  ros::Rate loop_rate(frequency);

  geometry_msgs::TwistStamped vel_msg;

  //Just to be sure that there are no random numbers initialised there
  vel_from_line_follower.twist.linear.x = 0;
  vel_from_line_follower.twist.angular.z = 0;

  //To get info from subscribers
  ros::spinOnce();

  qr_code_received = false;
  turn_to_cage = false;
  int time = 0;
  

  while (ros::ok())
  {

    if( qr_code_received )
    {
      qr_code_received = false;
      if( qr_message == CAGE_TWO_CODE )
      {
        time = clock();
        turn_to_cage = true;   
      }
    }

    if( turn_to_cage )
    {
      //moveToCage();
      vel_msg.twist.linear.x = 0;
      vel_msg.twist.angular.z = -0.8;
      //Velocity is published to the robot with current time stamp
      vel_msg.header.stamp = ros::Time::now();
      // if(vel_msg.twist.linear.x != vel_msg_prev.twist.linear.x || vel_msg.twist.angular.z != vel_msg_prev.twist.angular.z)
      velocity_pub.publish(vel_msg);
      //vel_msg_prev = vel_msg;
      if( (clock()-time)*1000/CLOCKS_PER_SEC > 2000 )
        turn_to_cage = false;
    }
          
    ROS_INFO("Active state: Line_Follower");
    vel_msg.twist.linear.x = vel_from_line_follower.twist.linear.x;
    vel_msg.twist.angular.z = vel_from_line_follower.twist.angular.z;
  
    //Velocity is published to the robot with current time stamp
    vel_msg.header.stamp = ros::Time::now();
   // if(vel_msg.twist.linear.x != vel_msg_prev.twist.linear.x || vel_msg.twist.angular.z != vel_msg_prev.twist.angular.z)
    velocity_pub.publish(vel_msg);
    //vel_msg_prev = vel_msg;

    //ROS_INFO("Data sent");



    ros::spinOnce();

    loop_rate.sleep();

  }


  return 0;
}
