#include <cv.h>

#include <sstream>

//Include headers for OpenCV Image processing
#include <opencv2/imgproc/imgproc.hpp>
//Include headers for OpenCV GUI handling
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>


#include <math.h>
#include <unistd.h>
#include <time.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/TwistStamped.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <std_msgs/String.h>
#include <string>

#include <zbar.h>  


// Constants declaration:
const double KP = 0.0025;
const double KI = 0.0003;
const double KD = 0.001;
const double K_ANTIOVERSHOOT = 1/5;
const double ANGLE_TO_POSITION_RATIO = 4;
const double SPEED_CONSTANT = 0.4;
const int PUBLISHING_FREQUENCY = 10;

cv::Mat received_image;
bool new_image;

//from https://rosstitchernode.wordpress.com/2014/06/05/ros-and-opencv-with-usb_cam/
void imageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
    //Convert from the ROS image message to a CvImage suitable for working with OpenCV for processing
    cv_bridge::CvImagePtr cv_ptr;

    try
    {
        //Always copy, returning a mutable CvImage
        //OpenCV expects color images to use BGR channel order.
        cv_ptr = cv_bridge::toCvCopy(original_image, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        //if there is an error during conversion, display it
        ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
        return;
    }

    received_image = cv_ptr->image;
    new_image = true;
}

int main(int argc, char **argv)
{

    //Node initialisation
    ros::init(argc, argv, "qr_scanner_node");
    ros::NodeHandle n;

    ROS_INFO("QR scanner started");

    image_transport::ImageTransport it(n);

    //Publishers
    ros::Publisher pub = n.advertise<std_msgs::String>("/qr_scanner_topic", 1);

    //Subscriptions
    //ros::Subscriber image_sub = n.subscribe<sensor_msgs::Image>("/usb_cam/image_raw", 1, imageCallback);
    image_transport::Subscriber image_sub = it.subscribe("/usb_cam/image_raw", 1, imageCallback);

    ROS_INFO("Subscribers, publishers created");

    new_image = false;

    int time;

    //The main loop frequency
    int frequency;
    ros::Rate loop_rate(PUBLISHING_FREQUENCY);
    std_msgs::String msg;


    cv::namedWindow("BarCode",CV_WINDOW_AUTOSIZE);
    while( !new_image )
    {
        ros::spinOnce();
        loop_rate.sleep();
        continue;
    }

    new_image = false;
    cv::Mat imgs = received_image.clone();
    cv::resize(imgs, imgs, cv::Size(), 0.4, 0.4);
    
/* !!! copied from http://blog.ayoungprogrammer.com/2014/04/real-time-qr-code-bar-code-detection.html and modified !!! */
    zbar::ImageScanner scanner;   
    //scanner.set_config(zbar::ZBAR_NONE, zbar::ZBAR_CFG_ENABLE, 1);   
    scanner.set_config(zbar::ZBAR_PARTIAL, zbar::ZBAR_CFG_ENABLE, 1);   
    int width = imgs.cols; //get the width of frames of the video  
    int height = imgs.rows; //get the height of frames of the video  
    cv::Mat grey;  
/* */


    while (ros::ok())
    {
        time = clock();
        
        //assures that it's entered only when there's a new image to see
        if( !new_image )
        {
            ros::spinOnce();
            loop_rate.sleep();
            continue;
        }
        new_image = false;

        //ROS_INFO("In the loop");

        imgs = received_image;
        cv::resize(imgs, imgs, cv::Size(), 0.4, 0.4);

/* !!! copied from http://blog.ayoungprogrammer.com/2014/04/real-time-qr-code-bar-code-detection.html and modified !!! */

     
        cv::cvtColor(imgs,grey,CV_BGR2GRAY);  

        uchar *raw = (uchar *)grey.data;   
        // wrap image data   
        zbar::Image image(width, height, "Y800", raw, width * height);   
        // scan the image for barcodes   
        int n = scanner.scan(image);   
        // extract results   
        for(zbar::Image::SymbolIterator symbol = image.symbol_begin();   
            symbol != image.symbol_end();   
            ++symbol) 
        {   
            std::vector<cv::Point> vp;   


            ROS_INFO("%s", symbol->get_data().c_str()); 
            msg.data = symbol->get_data();
            pub.publish(msg);

            int n = symbol->get_location_size();   
            for(int i=0;i<n;i++)
            {   
                vp.push_back(cv::Point(symbol->get_location_x(i),symbol->get_location_y(i)));   
            }   
            cv::RotatedRect r = cv::minAreaRect(vp);   
            cv::Point2f pts[4];   
            r.points(pts);   
            for(int i=0;i<4;i++)
            {   
                cv::line(imgs,pts[i],pts[(i+1)%4],cv::Scalar(255,0,0),3);   
            }   

            //Just one code needed
            break;

        }   
        cv::imshow("BarCode", imgs); //show the frame in "BarCode" window  
        cv::waitKey(4);

        time=(clock()-time)*1000/CLOCKS_PER_SEC;
        ROS_INFO("Elapsed time: %d ms", time);
        

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}
