#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/lib:/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/lib/x86_64-linux-gnu:/home/frobitgroup2/roswork/devel/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/home/frobitgroup2/roswork/devel/lib:/opt/ros/indigo/lib"
export PATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/lib/pkgconfig:/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/lib/x86_64-linux-gnu/pkgconfig:/home/frobitgroup2/roswork/devel/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/home/frobitgroup2/roswork/devel/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PYTHONPATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/frobitgroup2/roswork/src/RSD_group2/emergency_stop:$ROS_PACKAGE_PATH"