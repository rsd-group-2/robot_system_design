#ifndef _WAYPOINT_NAVIGATER_HPP
#define _WAYPOONT_NAVIGATOR_HPP

#include <string>
#include <iostream>
#include <array>

#include "marker_locator.hpp"
#include "marker_locator/MarkerLocatorData.h"
//#include "geometry_msgs/Point.h"
#include "ros/ros.h"

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include <Eigen/Dense>
#include <Eigen/Core>


namespace WayPointNav
{
    static const double MOVE_BASE_WAIT_DURATION = 5.0;
    static const double MARKER_THETA_OFFSET = -3.850;

    static const int NUM_OF_WAYPOINTS_BOX_TO_LINE = 2;
    static const int NUM_OF_WAYPOINTS_LINE_TO_BOX = 3;

    static const double LINE_DIST_THRESH = 0.15;
    static const double LINE_THETA_THRESH = 0.3;

    typedef Eigen::Vector2d Vector2d;

    template<typename T>
    struct Point
    {
        T x;
        T y;

        Point() : x(0), y(0) {}
        Point(T _x, T _y) : x(_x), y(_y) {}
    };

    template<typename T>
    struct Line
    {
        Point<T> start;
        Point<T> end;
    };

    template<typename T>
    struct WayPoint
    {
        T x;
        T y;
        T theta;

        WayPoint() : x(0), y(0), theta(0) {}

        WayPoint(T _x, T _y, T _theta) : x(_x), y(_y), theta(_theta) {}
    };

    enum WPTStackResult {
        IDLE,
        INITIAL_POSE_SUCCESSFULLY_SET,
        ERROR_SETTING_INITIAL_POSE,
        WAYPOINT_SUCCESSFULLY_REACHED,
        MOVING_TO_WAYPOINT,
        ERROR_REACHING_WAYPOINT,
        LINE_CORRECTED,
        LINE_UNCORRECTED,
        TRANSFORM_UNSUCCESSFUL,
        TRANSFORM_SUCCESSFUL
    };

};

using namespace WayPointNav;

template <class T>
class LineFeatures
{
public:
    LineFeatures();

    ~LineFeatures();

    Point<T> closestPointLineSegment(Line<T> line, Point<T> p);

    double distBetweenPoints(Point<T> a, Point<T> b);

    double angleBetweenVectors(Vector2d v1, Vector2d v2);

private:

};

template<class T>
class WptNav
{
public:
    // Constructor
    WptNav();

    // Destructor
    ~WptNav();

    // Move to a WayPoint
    WPTStackResult moveToWpt(WayPoint<T> wpt);

    // Move to a WayPoint
    WPTStackResult moveToWpt(double x, double y, double theta);

    // Set the initial pose
    WPTStackResult setInitialPose();

    // Get the current waypoint state
    WPTStackResult getWptState() {return wpt_state_;};

    // Move from the Box to the Line
    WPTStackResult moveFromBoxToLine();

    // Move from the Line to the Box
    WPTStackResult moveFromLineToBox();

    // Stop the movement of frobit
    void stopMovement();

private:
    // Evaluate the position relative to the start of the box
    WPTStackResult correctBoxInPosition();

    // Evaluate the position relative to the line and correct if neccesary.
    WPTStackResult correctLinePosition();

    // Callback function to get the map pose
    void AmclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& pose);

    // Get the thetavalue from a pose
    double getYawFromQuaternion(geometry_msgs::PoseWithCovarianceStamped pose);
    double getYawFromQuaternion(geometry_msgs::PoseStamped pose);

    // Get the newest waypoint in a specified frame.
    WayPoint<T> getNewestWayPoint(std::string frame);

    // Transform the marker locator to the map frame
    WPTStackResult transformMarkerToMap(WayPoint<T> marker_locator_pos, geometry_msgs::PoseStamped& map_locator_pose);

    // Get marker locator data
    marker_locator::marker_locator_msg getMarkerData();

    // Publisher
    ros::NodeHandle n_;
    ros::Publisher pub_;
    ros::Publisher pub_stop_;

    // Line which is being reached when moving from box to line
    Line<T> line_;
    LineFeatures<T> lineFeatures_;
    WayPoint<T> line_corr_wpt_;
    geometry_msgs::PoseWithCovarianceStamped pose_map_frame_;

    // Waypoints variables
    WPTStackResult wpt_state_;

    // Waypoints
    std::array<WayPoint<T>, NUM_OF_WAYPOINTS_BOX_TO_LINE> box_to_line_waypoints;
    std::array<WayPoint<T>, NUM_OF_WAYPOINTS_LINE_TO_BOX> line_to_box_waypoints;

    // Move base client and transform variables
    tf::TransformListener listener_;
    geometry_msgs::TransformStamped transform_in_map_;
    move_base_msgs::MoveBaseGoal goal_;

    // Marker locator object
    MarkerLocator marker_locator_obj_;

};
#endif /* _WAYPOINT_NAVIGATOR_HPP */


