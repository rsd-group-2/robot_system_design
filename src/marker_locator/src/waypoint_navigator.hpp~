#ifndef _WAYPOINT_NAVIGATER_HPP
#define _WAYPOONT_NAVIGATOR_HPP

#include <string>
#include <iostream>

#include "marker_locator.hpp"
//#include "geometry_msgs/Point.h"
#include "ros/ros.h"

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

//#include <Eigen/Dense>

namespace WayPointNav
{
    const double MOVE_BASE_WAIT_DURATION = 5.0;
    const double MARKER_THETA_OFFSET = -3.850;
    struct WayPoint
    {
        double x;
        double y;
        double theta;

        WayPoint() : x(0), y(0), theta(0) {}

        WayPoint(double _x, double _y, double _theta) : x(_x), y(_y), theta(_theta) {}
    };

    enum WPTStackResult {
        IDLE,
        INITIAL_POSE_SUCCESSFULLY_SET,
        ERROR_SETTING_INITIAL_POSE,
        WAYPOINT_SUCCESSFULLY_REACHED,
        MOVING_TO_WAYPOINT,
        ERROR_REACHING_WAYPOINT
    };

};

using namespace WayPointNav;

class WptNav
{
public:
    // Constructor
    WptNav();

    // Destructor
    ~WptNav();

    // Move to a WayPoint
    WPTStackResult moveToWpt(WayPoint wpt);

    // Move to a WayPoint
    WPTStackResult moveToWpt(double x, double y, double theta);

    // Set the initial pose
    WPTStackResult setInitialPose();

    // Get the current waypoint state
    WPTStackResult getWptState() {return wpt_state_;};


private:
    // Waypoints variables
    WPTStackResult wpt_state_;

    // Move base client and transform variables
    tf::TransformListener listener_;
    geometry_msgs::TransformStamped transform_in_map_;
    move_base_msgs::MoveBaseGoal goal_;

    // Marker locator object
    MarkerLocator marker_locator_obj_;

    // Publisher 
    ros::NodeHandle _n;
    ros::Publisher _pub;

};
#endif /* _WAYPOINT_NAVIGATOR_HPP */

