#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/String.h>
#include <string>

//States
#define IDLE 0
#define LINE_NAVIGATION 1
#define WAYPOINT_NAVIGATION 2
#define BOX_NAVIGATION 3
#define MANUAL_MODE 4

//Variables in which the incoming data is saved
bool stop_switch;
//bool stop_obstacle;		//Obstacle detection is currently disabled
bool stop_emergency;
int velocity_divisor;
int state;
bool manual_mode;
geometry_msgs::TwistStamped vel_from_hmi;
geometry_msgs::TwistStamped vel_from_line_navigation;
geometry_msgs::TwistStamped vel_from_waypoint_slam_navigation;

//Constants
const int PUBLISHING_FREQUENCY = 100;

//Function translating string state name to defined integer name
int rewriteStateToInt(std::string state_string)
{
    ROS_ERROR("State is: %s", state_string.c_str());
    if(state_string == "Idle")
	    return IDLE;
    else if(state_string == "Line_Navigation")
        return LINE_NAVIGATION;
    else if(state_string == "Waypoint_Navigation")
	    return WAYPOINT_NAVIGATION;
    else if(state_string == "BOX_NAVIGATION")
        return BOX_NAVIGATION;
    else if(state_string == "Manual_Mode")
	    return MANUAL_MODE;
    else
        return -1;
}

//Function translating defined integer name to the string name
std::string rewriteStateToString(int state_int)
{
    if(state_int == IDLE)
	    return "Idle";
    else if(state_int == LINE_NAVIGATION)
        return "Line_Navigation";
    else if(state_int == WAYPOINT_NAVIGATION)
	    return "Waypoint_Navigation";
    else if(state_int == BOX_NAVIGATION)
        return "BOX_NAVIGATION";
    else if(state_int == MANUAL_MODE)
	    return "Manual_Mode";
    else
        return "";
}



/*** CALLBACK FUNCTIONS ***/

//From the topic that decides which state is on
void mainStateCallback(const std_msgs::String::ConstPtr& msg)
{
    std::string state_string(msg->data.c_str());
    state = rewriteStateToInt(state_string);
}

//From the Arduino switch
void switchCallback(const std_msgs::String::ConstPtr& msg)
{
    if(std::strcmp(msg->data.c_str(), "navigation_mode") == 0)
    {
        ROS_INFO("navigation_mode");
        stop_switch = false;
    }
    //By default the robot is stopped
    else
    {
        stop_switch = true;
    }
} 

//From a software safety stop (emergency_stop_node)
void emergencyStopCallback(const std_msgs::String::ConstPtr& msg)
{
    if(std::strcmp(msg->data.c_str(), "stop") == 0)
    {
        ROS_INFO("Emergency stop!");
        stop_emergency = true;
    }
    else if(std::strcmp(msg->data.c_str(), "run") == 0)
    {
        ROS_INFO("Emergency stop disabled");
        stop_emergency = false;
    }
}

//Manual mode is started
void hmiModeCallback(const std_msgs::String::ConstPtr& msg)
{
    if(std::strcmp(msg->data.c_str(), "Manual_Mode_Start") == 0)
    {
        ROS_INFO("Manual mode started!");
        manual_mode = true;
    }
    else if(std::strcmp(msg->data.c_str(), "Manual_Mode_Stop") == 0)
    {
        ROS_INFO("Manual mode stopped");
        manual_mode = false;
    }
}

//From obstacle detector - DISABLED
//Possible messages: "no_obstacles" "slow" "stop"
/*void obstacleStopCallback(const std_msgs::String::ConstPtr& msg)
{
    if(std::strcmp(msg->data.c_str(), "stop") == 0)
    {
        ROS_INFO("Obstacle very close - stop!");
        stop_obstacle = true;
        velocity_divisor = 1;
    }
    else if(std::strcmp(msg->data.c_str(), "slow") == 0)
    {
        ROS_INFO("Obstacle nearby - slow down!");
        stop_obstacle = false;
        velocity_divisor = 2;
    }
    else
    {
        //ROS_INFO("No obstacles detected");
        stop_obstacle = false;
        velocity_divisor = 1;
    }
}*/

//Velocity from line navigation
void lineNavigationCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
    vel_from_line_navigation = *msg;
}

//Velocity from waypoint navigation
void waypointSlamNavigationCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
    vel_from_waypoint_slam_navigation = *msg;
}

//Velocity from SLAM navigation
void manualNavigationCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
    vel_from_hmi = *msg;
}

/***/


int main(int argc, char **argv)
{

    //Node initialisation
    ros::init(argc, argv, "mission_executor_node");
    ros::NodeHandle n;

    ROS_INFO("Mission executor started");

    //Publishers
    ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 10);

    //Subscribers
    ros::Subscriber switch_sub = n.subscribe<std_msgs::String>("/system_state", 1, switchCallback);
    //ros::Subscriber obstacle_detector_sub = n.subscribe<std_msgs::String>("/obstacle_detection_topic", 1, obstacleStopCallback);
    ros::Subscriber main_state_sub = n.subscribe<std_msgs::String>("/main_state_topic", 1, mainStateCallback);
	ros::Subscriber hmi_mode_sub = n.subscribe<std_msgs::String>("/manual_mode_topic", 1, hmiModeCallback);
    ros::Subscriber manual_sub = n.subscribe<geometry_msgs::TwistStamped>("/manual_mode_vel_topic", 5, manualNavigationCallback);
    ros::Subscriber line_navigation_sub = n.subscribe<geometry_msgs::TwistStamped>("/line_navigation_vel_topic", 5, lineNavigationCallback);
    ros::Subscriber waypoint_slam_sub = n.subscribe<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 5, waypointSlamNavigationCallback);
    ros::Subscriber emergency_stop_sub = n.subscribe<std_msgs::String>("/emergency_stop_topic", 1, emergencyStopCallback);

    //Variables - declaration
    int frequency; 
    bool fixed_state;
    int state_active;
    int prev_state;
    std::string fixed_state_name;
    geometry_msgs::TwistStamped vel_msg;
  
    /*** Variables - initialisation ***/
    state = IDLE;
    state_active = IDLE;
	prev_state = IDLE;
	manual_mode = false;
    stop_switch = true;			//The executor won't run without Arduino switch on
    //stop_obstacle = false;	//Obstacle detection is currently disabled
    stop_emergency = false;		//The software switch is not manadorily on
    velocity_divisor = 1;		//Obstacle detection is disabled, so slowing down feature is not used
    vel_from_line_navigation.twist.linear.x = 0;
    vel_from_line_navigation.twist.angular.z = 0;
    vel_from_hmi.twist.linear.x = 0;
    vel_from_hmi.twist.angular.z = 0;
    vel_from_waypoint_slam_navigation.twist.linear.x = 0;
    vel_from_waypoint_slam_navigation.twist.angular.z = 0;
  
    //Read parameters is started from launch file
    n.param("frequency", frequency, PUBLISHING_FREQUENCY);	//For default PUBLISHING_FREQUENCY see Constants above
    n.param("fixed_state", fixed_state, false);
    n.param<std::string>("fixed_state_name", fixed_state_name, "Waypoint_Navigation");
  
    //If there is a fixed initial state (not idle)
    if(fixed_state)
        state = rewriteStateToInt(fixed_state_name);
    else
        state = rewriteStateToInt("Idle");

    //Define loop frequency and collect initial callbacks
    ros::Rate loop_rate(frequency);
    ros::spinOnce();

    while (ros::ok())
    {

        //The state received from the main_state_topic is set as an active state
        state_active = state;
		
		//Manual mode overwrites state received from main_state_topic, but is overwritten by Arduino switch
		if(manual_mode)
			state_active = MANUAL_MODE;

        //Unless the Arduino swich is in off position or software emergency stop was activated
        if(stop_switch || stop_emergency)// || stop_obstacle - obstacle detection disabled
	{
            state_active = IDLE;

	} 
		/* note: existance of both "state" and "state_active" variables ensures, that under "state" variable 
        there is always the last state received from "main_state_topic" - it is never overwritten by IDLE, so 
		after the switch is back on it can continue the last received state */

		
		//State machine
		switch (state_active)
		{
		case IDLE:
            //ROS_ERROR("IDLE");
            vel_msg.twist.linear.x = 0;
            vel_msg.twist.angular.z = 0;
			break;
		case LINE_NAVIGATION:
			//ROS_ERROR("Active state: Line Navigation");
            vel_msg.twist.linear.x = vel_from_line_navigation.twist.linear.x / velocity_divisor;
            vel_msg.twist.angular.z = vel_from_line_navigation.twist.angular.z / velocity_divisor;
			break;
		case WAYPOINT_NAVIGATION:
        case BOX_NAVIGATION:
            //ROS_ERROR("Active state: Waypoint Slam Navigation");
            vel_msg.twist.linear.x = vel_from_waypoint_slam_navigation.twist.linear.x / velocity_divisor;
            vel_msg.twist.angular.z = vel_from_waypoint_slam_navigation.twist.angular.z / velocity_divisor;
			break;
		case MANUAL_MODE:
		    //ROS_INFO("Active state: Manual mode");
            vel_msg.twist.linear.x = vel_from_hmi.twist.linear.x / velocity_divisor;
            vel_msg.twist.angular.z = vel_from_hmi.twist.angular.z / velocity_divisor;
			break;
		default:
			ROS_ERROR("UNRECOGNISED STATE, MOTORS ARE OFF");
			vel_msg.twist.linear.x = 0;
            vel_msg.twist.angular.z = 0;
			break;
		}
		

        //Velocity is published to the robot with current time stamp
        vel_msg.header.stamp = ros::Time::now();
        velocity_pub.publish(vel_msg);


        if(state_active != prev_state) 
            ROS_INFO("New state is: [%s]", rewriteStateToString(state_active).c_str());
        prev_state = state_active;


        ros::spinOnce();
        loop_rate.sleep();
		
    }


  return 0;
}
