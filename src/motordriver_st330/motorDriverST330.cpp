#include "motorDriverST330.h"


	void serialSetup(){
		Serial.begin(9600);
		Serial.println("--- Start Serial Monitor SEND_RCVE ---");
		Serial.println();

	}

	void switchInit(){
		pinMode(A4,INPUT_PULLUP); // Analog input
		pinMode(A5,INPUT_PULLUP); // Analog input

	}

	void controlInit(){
		pinMode(10, OUTPUT); // Direction
		pinMode(11, OUTPUT); // Step
		pinMode(12, OUTPUT); // Enable
		digitalWrite(10, LOW);
		digitalWrite(11, LOW);
		digitalWrite(12, ENABLE); // Activate Motor controller

	}

	void motorStep(boolean up_down){

		if (up_down == 1){
			digitalWrite(10, HIGH);
		}
		else{
			digitalWrite(10, LOW);
		}
		digitalWrite(11, LOW);
		delayMicroseconds(pulseWith);
		digitalWrite(11, HIGH);
		delayMicroseconds(stepperSpeed);
	}

void setup(){
	serialSetup();
	switchInit();
	controlInit();
}

	void tipUp(){
		Serial.println("--- TIP UP START ---");
		Serial.println();
		if (!(TIP_UP_SWITCH == LOW))
		{
			boolean Up = 1;
			while (TIP_UP_SWITCH != LOW) // Test external switch tipUp
			{
			  motorStep(Up);
			  TIP_UP_SWITCH = digitalRead(A4);
				Serial.print("--- TIP UP ---");
				Serial.print(TIP_UP_SWITCH);
			}
		Serial.println("The Tipper upper position!");
		}
		else // If tip is up and a request of tipup
		{
			Serial.println("The Tipper is already in up position!");
			delay(100);
		}

	}

	void tipDown(){
		if (!(TIP_DOWN_SWITCH == LOW))
		{
			boolean Down = 0;
			while (TIP_DOWN_SWITCH != LOW)
			{
			  motorStep(Down);
			  TIP_DOWN_SWITCH = digitalRead(A5);
				Serial.print("--- TIP DOWN ---");
				Serial.print(TIP_DOWN_SWITCH);
			}

			Serial.println("The Tipper down position!");
			delay(100);
		}
		else // If tip is down and a request of tipdown
		{
			Serial.println("The Tipper is already in down position!");
			delay(100);
		}

	}

	void stopTip(){

		digitalWrite(12, LOW);
		boolean ENABLE_READ = digitalRead(12);
		Serial.print("ENABLE: ");
		Serial.print(ENABLE_READ);
		Serial.println("  --- TIP STOP DONE ---");

	}

	void serialReadState(){

		  if (Serial.available() > 0)
		  {
			tip_state =  Serial.read();
//			Serial.print(tip_state);
//		    Serial.print("        ");
//		    Serial.print(tip_state , HEX);
//		    Serial.print("       ");
//		    Serial.print(char(tip_state));
		  }
	}

void loop(){
  TIP_UP_SWITCH = digitalRead(A4);
  TIP_DOWN_SWITCH = digitalRead(A5);

  serialReadState();

  switch(tip_state){
  Serial.println(tip_state);
  case tipup:
		  tipUp();
		  delay(500);
	  break;
  case tipdown:
		  tipDown();
		  delay(500);
	  break;
  case stop:
  	  	  stopTip();
  	  	  delay(500);
  	  break;

  default :
	  Serial.println("Tip State out of scope! ");
		delay(500);
  }

}

