/*****************************************************************************
* University of Southern Denmark
*
* Robot System Design
*
* MODULENAME.: motorDriverST330.h
*
* PROJECT....: RSD
*
* DESCRIPTION: Motor driver for motor controller ST330v3
*
* Interface Serial 9600: States: tipup "1", tipdown "2", tipstop "3"
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYYY-MM-DD
* --------------------
* 2015-10-09  SAM    Module created.
*
*****************************************************************************/

#ifndef _motorDriverv330_H_
#define _motorDriverv330_H_

/***************************** Include files *******************************/
#include "Arduino.h"

/*****************************    Defines    *******************************/
	// enum in ascii values 49 > 1, 50 > 2, 51 > 3 where 1,2 and 3 are chars
    // send by terminal
	enum tip_states
	{
		tipup = (int)49,
		tipdown = (int)50,
		stop = (int)51
	};

	int tip_state = 0;
	// unsigned long time;
	boolean TIP_UP_SWITCH; // pin A4 read tip position at upper end
	                       // ( LOW define the TIP is in  upper position)
	boolean TIP_DOWN_SWITCH; // pin A5 read tip position at lower end
	                         // ( LOW define the TIP is in down position)
	boolean ENABLE = HIGH; // Activate-Deactivate motor controller

	int stepperSpeed = 1000; // scale microseconds
	int pulseWith = 30; // scale microseconds

/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

// Initiation
void serialSetup();
void switchInit();
void controlInit();
void setup();

// Motor control
void tipUp();
void tipDown();
void motorStep(boolean up_down);
void stopTip();

// Serial communication
void serialReadState();

void loop();



#endif /* _motorDriverv330_H_ */
