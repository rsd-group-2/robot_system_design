package com.github.ros_java.hmi.app;

import android.os.Bundle;
import org.ros.android.MessageCallable;
import org.ros.android.RosActivity;
import org.ros.android.view.RosTextView;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.io.IOException;
import java.lang.String;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.google.common.base.Objects;

public class App extends RosActivity
{
    private Spinner state;
    private RosTextView<std_msgs.String> android_sub;
    private RosTextView<std_msgs.String> tipper_sub;
    private RosTextView<std_msgs.String> status_sub;
    private com.github.rosjava.android_package.android_pubsub.Talker android_pub = new com.github.rosjava.android_package.android_pubsub.Talker("/interaction_to_pc");
    TextView position_text;
    TextView tipper_text;
    TextView status_text;
    private Button Nav_up;
    private Button Nav_down;
    private Button Nav_left;
    private Button Nav_right;
    ToggleButton toggleButton1;
    ToggleButton toggleButton2;

    public App() {
        // The RosActivity constructor configures the notification title and ticker
        // messages.
        super("App", "App");
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        addListenerOnSpinnerItemSelection();
        addListenerOnButton();
    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {

        // Initial textviews for text from pc
        position_text = (TextView) findViewById(R.id.position2);
        tipper_text = (TextView) findViewById(R.id.tipper2);
        status_text = (TextView) findViewById(R.id.status2);

        // Initial subscriber
        android_sub = (RosTextView<std_msgs.String>) findViewById(R.id.position);
        android_sub.setTopicName("/interaction_to_android");
        android_sub.setMessageType(std_msgs.String._TYPE);
        android_sub.setMessageToStringCallable(new MessageCallable<String, std_msgs.String>() {
            @Override
            public java.lang.String call(std_msgs.String message) {

                String h = message.getData();
                String[] array = h.split(",");

                position_text.setText(array[0]);
                tipper_text.setText(array[1]);
                status_text.setText(array[2]);

                return null;
            }
        });

        try {
            Thread.sleep(1000);

            // Ensure that the right network interface is used to connect with the ros master
            java.net.Socket socket = new java.net.Socket(getMasterUri().getHost(), getMasterUri().getPort());
            java.net.InetAddress local_network_address = socket.getLocalAddress();
            socket.close();
            NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(local_network_address.getHostAddress(), getMasterUri());
            Log.e("App", "master uri [" + getMasterUri() + "]");

            // Execute publisher
            nodeMainExecutor.execute(android_pub, nodeConfiguration);

            // Execute subscriber
            nodeMainExecutor.execute(android_sub, nodeConfiguration);

        } catch(InterruptedException e) {
            // Thread interruption
            Log.e("App", "sleep interrupted");

        } catch (IOException e) {
            // Socket problem
            Log.e("App", "socket error trying to get networking information from the master uri");
        }
    }

    //This is where we determine what happens when the Nav buttons are clicked. There are four buttons.
    public void addListenerOnButton() {

        Nav_up = (Button) findViewById(R.id.up);
        Nav_up.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("up");
            }

        });
        Nav_down = (Button) findViewById(R.id.down);
        Nav_down.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("down");
            }

        });
        Nav_left = (Button) findViewById(R.id.left);
        Nav_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("left");
            }

        });
        Nav_right = (Button) findViewById(R.id.right);
        Nav_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("right");
            }

        });

        toggleButton1 = (ToggleButton) findViewById(R.id.control2);
        toggleButton1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleButton1.isChecked()) {

                    android_pub.set_Control_mode("auto");
                } else {

                    android_pub.set_Control_mode("manual");
                }
            }
        });

        toggleButton2 = (ToggleButton) findViewById(R.id.tipper4);
        toggleButton2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleButton2.isChecked()) {

                    android_pub.set_tipper("up");
                } else {

                    android_pub.set_tipper("down");
                }
            }
        });

    }

    //These next two functions are for the drop-down menu - you can ignore them for now
    public class CustomOnItemSelectedListener implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
            Toast.makeText(parent.getContext(),
                    "OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }

    public void addListenerOnSpinnerItemSelection() {
        state = (Spinner) findViewById(R.id.state);
        state.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
