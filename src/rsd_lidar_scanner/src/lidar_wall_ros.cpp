/*
 * lidar_wall_ros.cpp
 *
 *  Created on: Sep 25, 2015
 *      Author: student
 */

#include "lidar_wall_ros.hpp"


LidarWallRos::LidarWallRos(std::string sub, std::string topic)
{
	initialized_ = false;

	// Topic to be published
	pub_ = n_.advertise<geometry_msgs::TwistStamped>(topic.c_str(), 10);

	// Topic to be subscribed
	sub_ = n_.subscribe<sensor_msgs::LaserScan>(sub.c_str(), 1000, &LidarWallRos::lidarParamReadingCallback, this);

	ROS_INFO("Publisher and Subcriber for Wall Follower setup completed");
}

double LidarWallRos::PIDController(double current_angle, double distance)
{
	const double epsilon = 0.01;
	const double kd = 0.001;
	const double ki = 0.0001;

	double output;

	static double set_point = PI/2;
	double kp = 1/distance;

	// Get the current time
	double current_time = ros::Time::now().toSec();
	double dt  = current_time - last_time_;

	std::cout << "current angle is: " << current_angle << std::endl;

	// Calculate error. Radians
	double error = set_point - abs(current_angle);
	std::cout << "Error is: " << error << std::endl;
	err_sum_ += (error * dt);
	double d_err = (error - last_err_) / dt;
	if(current_angle < 0) {
		// Compute the output
		output = -kp*error + kd*d_err; //ki*err_sum + kd*d_err;
	} else {
		output = kp*error + kd*d_err;
	}

	// Update variables
	last_err_ = error;
	last_time_ = current_time;

	std::cout << "dt: " << dt << std::endl;

	return output;
}


void LidarWallRos::lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	if(!initialized_) {
		ROS_INFO("Number of readings : \t [%i]", scan->ranges.size());
		ROS_INFO("Minimum angle: \t [%f]", scan->angle_min);
		ROS_INFO("Maximum angle: \t [%f]", scan->angle_max);
		ROS_INFO("Angle increment: \t [%f]", scan->angle_increment);
		initialized_ = true;
	}

	LineExtract lineExtract;
	std::vector<Point<double> > points;

	// Image for visualization
	cv::Mat laser_img = cv::Mat::zeros(IMAGE_SIZE_PX, IMAGE_SIZE_PX, CV_32F);
	cv::circle(laser_img, cv::Point(IMAGE_OFFSET_PX, IMAGE_OFFSET_PX), 5, cv::Scalar(122,122,122), 5, 8, 0);

	for(size_t i = 0; i < scan->ranges.size(); ++i) {
		// Get the current scan angle
		if(scan->ranges[i] > MIN_DISTANCE_TO_OBSTACLE) {
			double scan_angle = scan->angle_min + i*scan->angle_increment;

			// Draw the points
			Point<double> p = lineExtract.convertPolarToCartesian(scan_angle, scan->ranges[i]);
			p.x = round( p.x*SCALE_M_TO_CM) + IMAGE_OFFSET_PX;
			p.y = round( p.y*SCALE_M_TO_CM) + IMAGE_OFFSET_PX;
			drawPoint(&laser_img, p);

			// Store the points
			points.push_back(p);
		}
	}

	// Remove outliers:
	//lineExtract.removeStatistcalOutliers(points);

	// Check if any lines are colinear
	std::vector<Line<double> > lines = lineExtract.split_and_merge(points);
	// Draw lines
	for(size_t i = 0; i < lines.size(); i++) {
		if(lineExtract.Magnitude(lines[i].start, lines[i].end) < MIN_LINE_LENGTH_CM) {
			lines.erase(lines.begin() + i);
			//std::cout << "ERASING" << std::endl;
		} else {
			drawLine(&laser_img, lines[i]);
		}
	}

	//std::cout << "Number of lines detected: "<< lines.size() << std::endl;

	// Find the shortest distance to the nearest line
	double shortest_dist = 9999;
	int index = 0;
	for(size_t i = 0; i < lines.size(); ++i) {
		double distance = lineExtract.shortestDist2(Point<double>(IMAGE_OFFSET_PX,IMAGE_OFFSET_PX), lines[i]);
		if(distance < shortest_dist) {
			shortest_dist = distance;
			index = i;
		}
	}
	std::cout << "Shortest distance to a line is: " << shortest_dist << std::endl;

	// Apply the PID control
	cv::Point2d line_mid_point;
	double angle = lineExtract.shortestAngle(Point<double>(IMAGE_OFFSET_PX, IMAGE_OFFSET_PX), points[points.size()/2], lines[index], &line_mid_point);
	cv::circle(laser_img, line_mid_point, 10, cv::Scalar::all(255), 8, 5, 0);
	std::cout << "The angle is: " << angle << std::endl;
	double corr_angle = PIDController(angle, shortest_dist);
	std::cout << "Corrected angle: " << corr_angle << "\n\n";

	geometry_msgs::TwistStamped twistedStamp;
	twistedStamp.header.stamp = ros::Time::now();
	twistedStamp.twist.linear.x = 1.0;
	twistedStamp.twist.angular.z = corr_angle;
	pub_.publish(twistedStamp);

	cv::imshow(WINDOW_HEADER, laser_img);
	cv::waitKey(1);

	ros::spinOnce();
}

/**
 * Draw point on an opencv image Mat
 */
void LidarWallRos::drawPoint(cv::Mat *img, Point<double> p)
{
	if(p.x >= 0 && p.x < IMAGE_SIZE_PX && p.y >= 0 && p.y < IMAGE_SIZE_PX) {
		cv::Point point(p.x, p.y);
		img->at<float>(point) = 255;
		//cv::circle(laser_img, cv::Point(p.x, p.y), 5, cv::Scalar(255,255,255), 1, 1, 0);
	}
}

/**
 * Draw a line on an opencv image MAt
 */
void LidarWallRos::drawLine(cv::Mat *img, Line<double> line)
{
	cv::Point p1(line.start.x , line.start.y); //round(lines[i].start.x*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX, round(lines[i].start.y*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX/2);
	cv::Point p2(line.end.x, line.end.y);//round(lines[i].end.x*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX, round(lines[i].end.y*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX/2);
	cv::line(*img, p1, p2, cv::Scalar(255,0,0), 4, 1, 0);

	//cv::Point2d line_mid_point;
	//double angle = lineExtract.shortestAngle(Point<double>(IMAGE_OFFSET_PX, IMAGE_OFFSET_PX), points[points.size()/2], lines[i], &line_mid_point);
	//cv::circle(laser_img, line_mid_point, 10, cv::Scalar::all(255), 8, 5, 0);
	//std::cout << "Angle is: " << angle << std::endl;
}

