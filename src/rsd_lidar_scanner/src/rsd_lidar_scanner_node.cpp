#include "lidar_wall_ros.hpp"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#include <geometry_msgs/TwistStamped.h>

void testFunction()
{
	//ros::Publisher velocity_pub = node_handler.advertise<geometry_msgs::TwistStamped>("/wall_follower_topic", 10);

	LineExtract lineExtract;
	std::vector<Point<double> > points;

	// Generator random float number ranging from 0.1 to 4f
	float LO(0.1), HI(1.5);
	std::vector<float> random_num_vec;
	for(int i = 0; i < 270; i++) {
		srand(time(NULL));
		random_num_vec.push_back(LO + static_cast <float> (rand()) / ( static_cast <float> (RAND_MAX/(HI-LO))));
	}
	float angle_increment = 0.017453;
	float angle_min = -2.35619449;

	cv::Mat laser_img = cv::Mat::zeros(IMAGE_SIZE_PX, IMAGE_SIZE_PX, CV_32F);
	cv::circle(laser_img, cv::Point(IMAGE_OFFSET_PX,IMAGE_OFFSET_PX), 5, cv::Scalar(122,122,122), 5, 8, 0);

	std::cout << "Image size: " << laser_img.size() << std::endl;

	for(int i = 0; i < random_num_vec.size(); i++) {

		// Get the current scan angle
		double scan_angle = angle_min + i*angle_increment;
		if(i == 0)
			std::cout << "Minimum angle: " << scan_angle << std::endl;
		else if(i == random_num_vec.size()-2)
			std::cout << "Maximum angle: " << scan_angle << std::endl;
		Point<double> p = lineExtract.convertPolarToCartesian(scan_angle, random_num_vec[i]);
		p.x = round( p.x*SCALE_M_TO_CM) + IMAGE_OFFSET_PX;
		p.y = round( p.y*SCALE_M_TO_CM) + IMAGE_OFFSET_PX;
	//	std::cout << "Point: " << p << std::endl;
		if(p.x >= 0 && p.x < IMAGE_SIZE_PX && p.y >= 0 && p.y < IMAGE_SIZE_PX) {
			//cv::circle(laser_img, cv::Point(p.x, p.y), 5, cv::Scalar(255,255,255), 5, 8, 0);
			cv::Point point(p.x, p.y);
			laser_img.at<float>(point) = 255;
		}
		points.push_back(p);
	}

	// Check if any lines are colinear
	std::vector<Line<double> > lines = lineExtract.split_and_merge(points);
	// Draw lines
	for(size_t i = 0; i < lines.size(); i++) {
		if(lineExtract.Magnitude(lines[i].start, lines[i].end) < MIN_LINE_LENGTH_CM) {
			lines.erase(lines.begin() + i);
			//std::cout << "ERASING" << std::endl;
		} else {
			cv::Point p1(lines[i].start.x, lines[i].start.y); //round(lines[i].start.x*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX, round(lines[i].start.y*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX/2);
			cv::Point p2(lines[i].end.x, lines[i].end.y);//round(lines[i].end.x*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX, round(lines[i].end.y*SCALE_MULTIPLIER) + IMAGE_OFFSET_PX/2);
			cv::line(laser_img, p1, p2, cv::Scalar(255,0,0), 4, 1, 0);
			//cv::Point2d line_mid_point;
			//double angle = lineExtract.shortestAngle(Point<double>(IMAGE_OFFSET_PX, IMAGE_OFFSET_PX), points[points.size()/2], lines[i], &line_mid_point);
			//cv::circle(laser_img, line_mid_point, 10, cv::Scalar::all(255), 8, 5, 0);
			//std::cout << "Angle is: " << angle << std::endl;
		}
	}

	std::cout << "Number of lines detected: "<< lines.size() << std::endl;

	// Find the shortest distance to the nearest line
	double shortest_dist = 9999;
	int index = 0;
	for(size_t i = 0; i < lines.size(); ++i) {
		double distance = lineExtract.shortestDist2(Point<double>(IMAGE_OFFSET_PX,IMAGE_OFFSET_PX), lines[i]);
		std::cout << "distance of line " << i << " is: " << distance << std::endl;
		if(distance < shortest_dist) {
			shortest_dist = distance;
			index = i;
		}
	}
	std::cout << "Shortest distance to a line is: " << shortest_dist << std::endl;

	// Apply the PID control
	cv::Point2d line_mid_point;
	double angle = lineExtract.shortestAngle(Point<double>(IMAGE_OFFSET_PX, IMAGE_OFFSET_PX), points[points.size()/2], lines[index], &line_mid_point);
	cv::circle(laser_img, line_mid_point, 10, cv::Scalar::all(255), 8, 5, 0);
	std::cout << "The angle is: " << angle << std::endl;
	//double corr_angle = PIDController(angle, shortest_dist);
	//std::cout << "Corrected angle: " << corr_angle << std::endl;

	cv::imshow(WINDOW_HEADER, laser_img);
	cv::waitKey(1);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "rsd_lidar_scanner_node");

	// Window for visualization
	cv::namedWindow(WINDOW_HEADER, CV_WINDOW_AUTOSIZE);
	cv::startWindowThread();

	LidarWallRos liderWallRosObj("/scan", "/wall_follower_topic");

	//testFunction();

	ROS_INFO("Initiaization completed...");

	ros::spin();
}
