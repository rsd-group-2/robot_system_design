#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"

#include <sstream>
#include <math.h>
#include <stdlib.h>

const int DEFAULT_POINTS_THRESHOLD = 5;

const int center_angle = 135;
const int start_1m = center_angle - 10;
const int end_1m = center_angle + 10;
const int start_03m = center_angle - 25;//37;
const int end_03m = center_angle + 25;//37;

int points_threshold;

ros::Subscriber sub;
ros::Publisher pub;

/**
 * Callback function to handle incoming messages from the subscribed topic
 */

void Obstacle_Detection_Callback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	std::stringstream ss;
	std_msgs::String msg;

	bool obstacle_03m = false;
	bool obstacle_1m = false;

	float distance_03m = 0.3;
	float distance_1m = 1.0;

	int obstacle_point_stop_cnt = 0;
	int obstacle_point_slow_cnt = 0;

	for (int i = start_03m; i <= end_03m; i++)
	{
		/*if (i == center_angle) 
		{
			distance_03m = 0.3;
			distance_1m = 1;
		}
		else
		{		
			distance_03m = 0.3 / cos( abs( center_angle - i ) * (M_PI / 180) );
			distance_1m = 1 / cos( abs( center_angle - i ) * (M_PI / 180) ); 
		}	*/	

		if (scan->ranges[i] <= distance_03m && scan->ranges[i] > 0) 
		{
			obstacle_point_stop_cnt++;
			if(obstacle_point_stop_cnt > points_threshold) {
				ROS_ERROR("OBSTACLE");
				obstacle_03m = true;
				break;
			}	
			//std::cout << scan->ranges[i] << ", ";
			/*for (int j = i+1; j <= i+5; j++)
			{
				if (j == center_angle) 
				{
					distance_03m = 0.3;
				}
				else
				{		
					distance_03m = 0.3 / cos( abs( center_angle - j ) * (M_PI / 180) ); 
				}

				if (scan->ranges[j] <= distance_03m) 
				{
					obstacle_point_stop_cnt++;
					if(obstacle_point_stop_cnt > POINTS_THRESH) {
						ROS_ERROR("OBSTACLE");
						obstacle_03m = true;
						break;
					}
				}			
			}*/
		}

		else if (scan->ranges[i] <= distance_1m && i >= start_1m && i <= end_1m && scan->ranges[i] > 0) 
		{
			obstacle_point_slow_cnt++;
			if(obstacle_point_slow_cnt > points_threshold) {
				ROS_ERROR("SLOW OBSTACLE");
				obstacle_1m = true;
				break;
			}	
			/*for (int j = i+1; j <= i+5; j++)
			{
				if (j == center_angle) 
				{
					distance_1m = 1;
				}
				else
				{		
					distance_1m = 1 / cos( abs( center_angle - j ) * (M_PI / 180) ); 
				}				

				if (scan->ranges[j] <= distance_1m) 
				{	
					obstacle_point_slow_cnt++;
					if(obstacle_point_slow_cnt > POINTS_THRESH) {
						obstacle_1m = true;
						break;
					}
				}			
			}*/
		}
	}

	if (obstacle_03m == true)
	{
		ss << "stop";
	}
	else if (obstacle_1m == true)
	{
		ss << "slow";
	} 
	else 
	{
		ss << "no_obstacles";
	}

	// Clear counters
	obstacle_point_stop_cnt = 0;
	obstacle_point_slow_cnt = 0;

	msg.data = ss.str();

	pub.publish(msg);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "obstacle_detection_node");

	// Handler to retrieve published nodes
	ros::NodeHandle n;

	n.param("threshold", points_threshold, DEFAULT_POINTS_THRESHOLD);


	sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 10, Obstacle_Detection_Callback);

	pub = n.advertise<std_msgs::String>("/obstacle_detection_topic", 1);

	ros::spin();
}
