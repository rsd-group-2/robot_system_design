#include <ros/ros.h>
#include <ros/console.h>
#include <std_msgs/String.h>
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <limits>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_mes_srv");

	ros::NodeHandle n;

	printf("Test MES server started");
	
	ros::Publisher order_pub = n.advertise<std_msgs::String>("/order_topic", 10);

	ros::Rate loop_rate(10);
	
	std_msgs::String order_msg;

	int workcell_num;
	int bricks_num;
	int red_num;
	int green_num;
	int blue_num;
	char yes_no;
	std::stringstream order;

	while (ros::ok())
	{
		//"frobit:_X,_workcell:_X,_bricks:_XX,_red:_XX,_green:_XX,_blue:_XX"
		printf("New test order will be constructed\n");
		printf("It works only for Frobit robot from group 2 for now\n");
		order.str(std::string());
		while (true)
		{
			printf("Type the workcell number (1/2/3)\n");
			while (!(std::cin >> workcell_num))
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
				printf("Try again, now carefully\n");
			}
			
			if( workcell_num == 1 || workcell_num == 2 || workcell_num == 3 )
			{
				printf("Workcell %d chosen\n", workcell_num);
				break;
			}
			else
				printf("We have only 3 workcells...\n");
		}
			
		while (true)
		{
			printf("Type how many bricks overally you want\n");
			while (!(std::cin >> bricks_num))
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
				printf("Try again, now carefully\n");
			}
			
			if( bricks_num <= 30 )
			{
				printf("%d bricks ordered\n", bricks_num);
				break;
			}
			else
				printf("Haha, very funny, carry it yourself... 30 max\n");
		}
		
		while (true)
		{
			printf("Type how many red bricks you want\n");
			while (!(std::cin >> red_num))
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
				printf("Try again, now carefully\n");
			}
			
			if( red_num <= bricks_num )
			{
				printf("%d red bricks ordered\n", red_num);
				break;
			}
			else
				printf("Do you even math? You have %d bricks, from which %d are red, good job...\n", bricks_num, red_num);
		}
		
		while (true)
		{
			printf("Type how many green bricks you want\n");
			while (!(std::cin >> green_num))
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
				printf("Try again, now carefully\n");
			}
			
			if( green_num + red_num <= bricks_num )
			{
				printf("%d green bricks ordered\n", green_num);
				break;
			}
			else
				printf("Do you even math? You have %d bricks, from which %d are red and %d green, good job...\n", bricks_num, red_num, green_num);
		}
		
		while (true)
		{
			printf("This is your order:\n");
			
			if( bricks_num < 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_0" 
					<< bricks_num << ",_red:_0" << red_num << ",_green:_0" << green_num << ",_blue:_0" << bricks_num - red_num - green_num;
			else if( red_num < 10 && green_num < 10 && (bricks_num - red_num - green_num) < 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_0" << red_num << ",_green:_0" << green_num << ",_blue:_0" << bricks_num - red_num - green_num;
			else if( red_num >= 10 && green_num < 10 && (bricks_num - red_num - green_num) < 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_" << red_num << ",_green:_0" << green_num << ",_blue:_0" << bricks_num - red_num - green_num;
			else if( red_num < 10 && green_num >= 10 && (bricks_num - red_num - green_num) < 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_0" << red_num << ",_green:_" << green_num << ",_blue:_0" << bricks_num - red_num - green_num;
			else if( red_num < 10 && green_num < 10 && (bricks_num - red_num - green_num) >= 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_0" << red_num << ",_green:_0" << green_num << ",_blue:_" << bricks_num - red_num - green_num;
			else if( red_num >= 10 && green_num >= 10 && (bricks_num - red_num - green_num) < 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_" << red_num << ",_green:_" << green_num << ",_blue:_0" << bricks_num - red_num - green_num;
			else if( red_num >= 10 && green_num < 10 && (bricks_num - red_num - green_num) >= 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_" << red_num << ",_green:_0" << green_num << ",_blue:_" << bricks_num - red_num - green_num;
			else if( red_num < 10 && green_num >= 10 && (bricks_num - red_num - green_num) >= 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_0" << red_num << ",_green:_" << green_num << ",_blue:_" << bricks_num - red_num - green_num;
			else if( red_num >= 10 && green_num >= 10 && (bricks_num - red_num - green_num) >= 10 )
				order << "frobit:_2,_workcell:_" << workcell_num << ",_bricks:_" 
					<< bricks_num << ",_red:_" << red_num << ",_green:_" << green_num << ",_blue:_" << bricks_num - red_num - green_num;

			std::cout << order.str() << std::endl;
			
			printf("Is it fine? (y/n)\n");
			
			while (!(std::cin >> yes_no))
			{
				std::cin.clear();
				printf("Type 'y' or 'n', try again\n");
			}
			
			if( yes_no == 'y' )
			{
				order_msg.data = order.str();
				order_pub.publish(order_msg);
				printf("Your order has been published!\n");
				break;
			}
			else if( yes_no == 'n' )
			{
				printf("Publishing aborted, start over\n");
				break;
			}
			else
			{
				printf("Type 'y' or 'n', try again");
			}
			
		}
		
		ros::spinOnce();
		loop_rate.sleep();
		
	}



    return 0;
}