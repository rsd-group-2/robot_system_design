// OpenCV 
#include <opencv2/opencv.hpp>

// RobWork
#include <rw/rw.hpp>
#include <rwlibs/calibration.hpp>

// STL
#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <fstream>

int main() {
	// Define device and frame names
	const std::string device_name = "KukaKr6";
	const std::string sensor_frame_name = "Camera";
	const std::string marker_frame_name = "Marker";

	// Load workcell
	std::cout << "Loading work cell..." << std::endl;
	rw::models::WorkCell::Ptr wc = rw::loaders::WorkCellLoader::Factory::load("../../Workcell/Scene.wc.xml");
	rw::kinematics::State state = wc->getDefaultState();
	std::cout << "Done loading work cell" << std::endl << std::endl;

	std::cout << "Getting devices..." << std::endl;
	auto devices = wc->getDevices();

	for(auto device : devices) {
		std::cout << device->getName() << std::endl;
	}
	rw::models::Device::Ptr device = wc->findDevice(device_name);
	rw::models::SerialDevice::Ptr serial_device = device.cast<rw::models::SerialDevice>();
	std::cout << "Done getting devices" << std::endl << std::endl;

	std::cout << "Getting frames..." << std::endl;
	rw::kinematics::Frame::Ptr camera_frame = wc->findFrame(sensor_frame_name);
	rw::kinematics::Frame::Ptr marker_frame = wc->findFrame(marker_frame_name);
	std::cout << "Done getting frames" << std::endl << std::endl;
	
	// Initialize Calibrator
	std::cout << "Initializing work cell calibrator" << std::endl;

	std::vector<rw::kinematics::Frame*> sensor_frames;
	sensor_frames.push_back(camera_frame.get());

	std::vector<rwlibs::calibration::WorkCellCalibration::DeviceMarkerPair> device_marker_pairs;
	device_marker_pairs.push_back(std::make_pair(serial_device, marker_frame.get()));
	
	rwlibs::calibration::WorkCellCalibration::Ptr calibration(rw::common::ownedPtr(new rwlibs::calibration::WorkCellCalibration(device_marker_pairs, sensor_frames)));
	rwlibs::calibration::WorkCellJacobian::Ptr jacobian(rw::common::ownedPtr(new rwlibs::calibration::WorkCellJacobian(calibration)));
	rwlibs::calibration::WorkCellCalibrator calibrator(wc, calibration, jacobian);
	std::cout << "Done initializing work cell calibrator" << std::endl << std::endl;
	
	std::cout << "Minimum number of measurements: " << calibrator.getMinimumMeasurementCount() << std::endl << std::endl;

	// Load images and configurations
	std::cout << "Loading images and configurations..." << std::endl;
	std::vector< std::pair<rw::math::Q, cv::Mat> > data_pairs;
	std::string configuration_file_name = "configs.csv";
	std::string image_prefix = "frame000";
	std::string image_postfix = ".jpg";
	const unsigned int n_measurements = 10;

	std::ifstream q_file;
	q_file.open(configuration_file_name);
	
	for(unsigned int i = 0; i < n_measurements; ++i) {
		// Load image
		std::stringstream ss;
		ss << image_prefix;
		ss << i;
		ss << image_postfix;
		cv::Mat img;
		std::cout << "Image path: " << ss.str() << std::endl;
        img = cv::imread(ss.str(), CV_LOAD_IMAGE_COLOR);

		// Load configuration
		rw::math::Q q(6);
		for(unsigned int j = 0; j < 6; ++j) {
			std::string val;
			std::getline(q_file, val, ';');
			q[j] = std::stod(val);
		}

		data_pairs.push_back(std::make_pair(q, img));
	}
	q_file.close();
	std::cout << "Loaded " << data_pairs.size() << " configuration and image pairs" << std::endl << std::endl;
	
	// Find chessboard
	std::cout << "Finding valid measurements..." << std::endl;
	std::vector<rwlibs::calibration::CalibrationMeasurement::Ptr> calib_measurements;
	cv::Size chessboard_dim(8, 6);
	const double square_size = 1.0;
	std::vector< cv::Point3f > marker_points; // NEED TO FILL
	for(int i = 0; i < chessboard_dim.x; ++i) {
		for(int j = 0; j < chessboard_dim.y; ++i) {
			cv::Point3f p(i*square_size, j*square_size, 0);
			marker_points.push_back(p);
		}
	}

	
	cv::FileStorage fs("../../usb_cam/calibrations/workcell_calibration.yml", cv::FileStorage::READ);
	cv::Mat cam_matrix, dist_coeffs;
	fs["cameraMatrix"] >> cam_matrix;
	fs["distCoeffs"] >> dist_coeffs;
	std::cout << "Camera matrix: \n" << cam_matrix << std::endl;
	std::cout << "Distortion coefficients: \n" << dist_coeffs << std::endl;
	fs.release();
 
	for(auto it = data_pairs.begin(); it != data_pairs.end(); ++it) {
		rw::math::Q q = it->first;
		cv::Mat image = it->second;
		std::vector<cv::Point2f> chessboard_corners;
		
        // Look for chessboard
		try {
			if(cv::findChessboardCorners(image, chessboard_dim, chessboard_corners)) {
				// Solve pnp
				cv::Mat rvec, tvec;
				if(cv::solvePnP(marker_points, chessboard_corners, cam_matrix, dist_coeffs, rvec, tvec)) {
					// Yay got configuration
					// Convert rotation vector to rotation matrix
					cv::Mat rmat;
					cv::Rodrigues(rvec, rmat);

					// Create Transform3D from rmat and tvec
					rw::math::Transform3D<> camTmarker;
					camTmarker(0, 0) = rmat.at<double>(0, 0);
					camTmarker(0, 1) = rmat.at<double>(0, 1);
					camTmarker(0, 2) = rmat.at<double>(0, 2);
					camTmarker(1, 0) = rmat.at<double>(0, 0);
					camTmarker(1, 1) = rmat.at<double>(1, 1);
					camTmarker(1, 2) = rmat.at<double>(1, 2);
					camTmarker(2, 0) = rmat.at<double>(2, 0);
					camTmarker(2, 1) = rmat.at<double>(2, 1);
					camTmarker(2, 2) = rmat.at<double>(2, 2);

					camTmarker(0, 3) = tvec.at<double>(0, 0);
					camTmarker(1, 3) = tvec.at<double>(0, 1);
					camTmarker(2, 3) = tvec.at<double>(0, 2);
					
					calib_measurements.push_back(rw::common::ownedPtr(new rwlibs::calibration::CalibrationMeasurement(q, camTmarker, device_name, sensor_frame_name, marker_frame_name)));
				}
			}
		} catch( cv::Exception& e ) {
			std::cout << "Measurement " << std::distance(data_pairs.begin(), it) << " is invalid" << std::endl;
			std::cout << "Exception: " << e.what() << std::endl;
		}
	}
	std::cout << "Finished finding valid measurements" << std::endl << std::endl;

	
	// Check if enough measurements
	if(calib_measurements.size() < calibrator.getMinimumMeasurementCount()) {
		std::cout << "Not enough valid measurements" << std::endl;
		std::cout << "Valid measurements: " << calib_measurements.size() << std::endl;
		std::cout << "Required number of measurements: " << calibrator.getMinimumMeasurementCount() << std::endl;
		exit(1);
	}

	calibrator.setMeasurements(calib_measurements);

	// Set up calibration
	std::cout << "Setting up calibration..." << std::endl;
	std::cout << "Calibration count: " << calibration->getFixedFrameCalibrations()->getCalibrationCount() << std::endl;
	for(int i = 0; i < calibration->getFixedFrameCalibrations()->getCalibrationCount(); ++i) {
		std::cout << "Enabling calibration for frame: " << calibration->getFixedFrameCalibrations()->getCalibration(i)->getFrame()->getName() << std::endl;;
		calibration->getFixedFrameCalibrations()->getCalibration(i)->setEnabled(true);
	}
	std::cout << "Enabling composite link calibration" << std::endl;
	calibration->getCompositeLinkCalibration()->setEnabled(true);
	std::cout << "Enabling composite joint encoder calibration" << std::endl;
	calibration->getCompositeJointEncoderCalibration()->setEnabled(true);
	std::cout << "Calibration set up." << std::endl << std::endl;

	// Perform calibration
	std::cout << "Performing calibration..." << std::endl; 
	try {
		calibrator.calibrate(state);
	} catch (rw::common::Exception& ex) {
		std::cout << "Calibration failed: " << ex.getMessage() << std::endl;
		exit(1);
	}
	std::cout << "Calibration successful" << std::endl;
	
	// Get calibration parameters
	std::cout << "Transform:\n " << calibration->getFixedFrameCalibrationForSensor(sensor_frame_name)->getFrame()->getFixedTransform() << std::endl;
	std::cout << "Correction Transform:\n" << calibration->getFixedFrameCalibrationForSensor(sensor_frame_name)->getCorrectionTransform() << std::endl;
	return 0;
}
